package com.emts.gtp;

import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.PixelFormat;
import android.os.Build;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.emts.gtp.helper.Logger;

/**
 * Created on 2/20/2016.
 */
public class LockScreenWidget extends Service {

    private BroadcastReceiver mReceiver;
//    private boolean isShowing = false;

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

//    private WindowManager windowManager;
//    private RelativeLayout notLiveLayout;
//    WindowManager.LayoutParams params;

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public void onCreate() {
        super.onCreate();

        if (Build.VERSION.SDK_INT > 25) {
            String CHANNEL_ID = "my_channel_01";
            NotificationChannel channel = new NotificationChannel(CHANNEL_ID,
                    "GTP Live Price Service Running",
                    NotificationManager.IMPORTANCE_DEFAULT);

            ((NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE)).createNotificationChannel(channel);

            Notification notification = new NotificationCompat.Builder(this, CHANNEL_ID)
                    .setContentTitle("GTP Live Pricing")
                    .setContentText("GTP Live Pricing service running...").build();

            startForeground(1, notification);
        }
//        windowManager = (WindowManager) getSystemService(WINDOW_SERVICE);
//
//        //add live layout
//        notLiveLayout = (RelativeLayout) LayoutInflater.from(this).inflate(R.layout.notification_live_feed,
//                null, false);

//        RelativeLayout contentOverlay = notLiveLayout.findViewById(R.id.lock_overlay);
//        contentOverlay.setOnTouchListener(new View.OnTouchListener() {
//            @Override
//            public boolean onTouch(View view, MotionEvent motionEvent) {
//                Logger.e("onTouch contentOverlay", "view : touch" + view + " : " + motionEvent);
//                stopSelf();
//                return false;
//            }
//        });
//        LinearLayout notiLock = notLiveLayout.findViewById(R.id.noti_lock);
//        notiLock.setOnTouchListener(new View.OnTouchListener() {
//            @Override
//            public boolean onTouch(View view, MotionEvent motionEvent) {
//                Logger.e("onTouch notiLock", "view : touch" + view + " : " + motionEvent);
//                stopSelf();
//                return false;
//            }
//        });
//
//        TextView touchToDismiss = notLiveLayout.findViewById(R.id.touchToDismiss);
//        touchToDismiss.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Logger.e("onTouch touchToDismiss", "view :" + view);
//                stopSelf();
//            }
//        });
//
//        //set parameters for the view
//        params = new WindowManager.LayoutParams(
//                WindowManager.LayoutParams.MATCH_PARENT,
//                WindowManager.LayoutParams.MATCH_PARENT,
//                WindowManager.LayoutParams.TYPE_SYSTEM_OVERLAY,
//                WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE
//                        | WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL
//                        | WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED
//                        | WindowManager.LayoutParams.FLAG_WATCH_OUTSIDE_TOUCH,
//                PixelFormat.TRANSLUCENT);
//        params.gravity = Gravity.CENTER;

        //Register receiver for determining screen off and if user is present
        mReceiver = new LockScreenStateReceiver();
        IntentFilter filter = new IntentFilter(Intent.ACTION_SCREEN_OFF);
        filter.addAction(Intent.ACTION_USER_PRESENT);

        registerReceiver(mReceiver, filter);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return START_STICKY;
    }

    public class LockScreenStateReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction() == null) return;
            if (intent.getAction().equals(Intent.ACTION_SCREEN_OFF)) {
                //if screen is turn off show the view
//                if (!isShowing) {
////                    windowManager.addView(notLiveLayout, params);
                    showActivityOverLockScreen();
//                    isShowing = true;
//                }
            } else if (intent.getAction().equals(Intent.ACTION_USER_PRESENT)) {
                //Handle resuming events if user is present/screen is unlocked remove the view immediately
//                if (isShowing) {
//                    windowManager.removeViewImmediate(notLiveLayout);
//                    isShowing = false;
//                }
            }
        }
    }

    private void showActivityOverLockScreen() {
        Intent alarmIntent = new Intent("android.intent.action.MAIN");
        alarmIntent.setClass(LockScreenWidget.this, LockScreenActivity.class);
        alarmIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//        alarmIntent.addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED |
//                WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD |
//                WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON |
//                WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);
//        alarmIntent.putExtra("isLockIntended", true);
        startActivity(alarmIntent);
    }

    @Override
    public void onDestroy() {
        //unregister receiver when the service is destroy
        if (mReceiver != null) {
            unregisterReceiver(mReceiver);
        }

        //remove view if it is showing and the service is destroy
//        if (isShowing) {
//            windowManager.removeViewImmediate(notLiveLayout);
//            isShowing = false;
//        }
        super.onDestroy();
    }

}