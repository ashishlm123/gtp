package com.emts.gtp.fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.NestedScrollView;
import android.text.Editable;
import android.text.Html;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.emts.gtp.Api;
import com.emts.gtp.DialogSessionPinCode;
import com.emts.gtp.MainActivity;
import com.emts.gtp.R;
import com.emts.gtp.helper.AlertUtils;
import com.emts.gtp.helper.InputHelper;
import com.emts.gtp.helper.Logger;
import com.emts.gtp.helper.NetworkUtils;
import com.emts.gtp.helper.VolleyHelper;
import com.emts.gtp.model.Product;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Objects;

public class FutureOrder extends Fragment {
    EditText etValueRm, etTotalXauWtGm, etAceBuyPrice, etAceSellPrice;
    TextView tvErrValueRm, tvErrTotalXauWtGm, tvErrAceBuyPrice, tvErrAceSellPrice;
    Spinner spProduct;
    NestedScrollView mContent;
    ProgressBar progressBar;
    TextView errorText;
    ArrayList<Product> productLists = new ArrayList<>();

//    //save data to retrieve while pop up from backstack
//    int selectedProductPos = 0;

    //activity prefs
    SharedPreferences actPrefs;
    public static final String PREF_PORDUCT_POS = "product_pos";
    public static final String PREF_VALUE_RM = "value_rm";
    public static final String PREF_XAU_WEIGHT = "xau_weight";
    public static final String PREF_ACE_BUY = "ace_buy";
    public static final String PREF_ACE_SELL = "ace_sell";

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        actPrefs = ((MainActivity) getActivity()).getPreferences(Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = actPrefs.edit();
        editor.putInt(PREF_PORDUCT_POS, 0);
        editor.putString(PREF_VALUE_RM, "");
        editor.putString(PREF_XAU_WEIGHT, "");
        editor.putString(PREF_ACE_BUY, "");
        editor.putString(PREF_ACE_SELL, "");
        editor.apply();

    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_future_order, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ((MainActivity) Objects.requireNonNull(getActivity())).setToolbarTitle(false, "Future Order");

        mContent = view.findViewById(R.id.content);
        progressBar = view.findViewById(R.id.progress);
        errorText = view.findViewById(R.id.error_text);

        Button btnQueueOrder = view.findViewById(R.id.btn_queue);

        spProduct = view.findViewById(R.id.sp_product);

        etValueRm = view.findViewById(R.id.et_product_values);
        etValueRm.setText(actPrefs.getString(PREF_VALUE_RM, ""));
        etTotalXauWtGm = view.findViewById(R.id.et_total_xau_weight);
        etTotalXauWtGm.setText(actPrefs.getString(PREF_XAU_WEIGHT, ""));
        etAceBuyPrice = view.findViewById(R.id.et_ace_buy_price_rm_g);
        etAceBuyPrice.setText(actPrefs.getString(PREF_ACE_BUY, ""));
        etAceSellPrice = view.findViewById(R.id.et_ace_sell_price_rm_g);
        etAceSellPrice.setText(actPrefs.getString(PREF_ACE_SELL, ""));
        InputHelper.limitAfterDecimal(etAceSellPrice, 2);
        InputHelper.limitAfterDecimal(etAceBuyPrice, 2);

        tvErrValueRm = view.findViewById(R.id.tv_err_value);
        tvErrTotalXauWtGm = view.findViewById(R.id.tv_err_total_xau_wt);
        tvErrAceBuyPrice = view.findViewById(R.id.tv_err_ace_buy_price);
        tvErrAceSellPrice = view.findViewById(R.id.tv_err_ace_sell_price);

        etValueRm.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.length() > 0) {
                    etTotalXauWtGm.setBackgroundResource(R.drawable.bg_et_style_half_disable);
                } else {
                    etTotalXauWtGm.setBackgroundResource(R.drawable.bg_et_style_half_color_primary);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }

        });
        etValueRm.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    if (spProduct.getSelectedItemPosition() == 0) {
                        AlertUtils.showToast(getActivity(), "Select product");
                        return true;
                    }
                    if (etTotalXauWtGm.getText().length() > 0) {
                        etTotalXauWtGm.setText("");
                        etTotalXauWtGm.setBackgroundResource(R.drawable.bg_et_style_half_disable);
                    }
//                    if (!etValueRm.isEnabled()) {
//                        etValueRm.setBackgroundResource(R.drawable.bg_et_style_half_color_primary);
//                    }
                }
                return false;
            }
        });

        etTotalXauWtGm.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.length() > 0) {
                    etValueRm.setBackgroundResource(R.drawable.bg_et_style_half_disable);
                } else {
                    etValueRm.setBackgroundResource(R.drawable.bg_et_style_half_color_primary);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });
        etTotalXauWtGm.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    if (spProduct.getSelectedItemPosition() == 0) {
                        AlertUtils.showToast(getActivity(), "Select product");
                        return true;
                    }
                    if (etValueRm.getText().length() > 0) {
                        etValueRm.setText("");
                        etValueRm.setBackgroundResource(R.drawable.bg_et_style_half_disable);
                    }
                }
                return false;
            }
        });

        etAceBuyPrice.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.length() > 0) {
                    etAceSellPrice.setBackgroundResource(R.drawable.bg_et_style_half_disable);
                } else {
                    etAceSellPrice.setBackgroundResource(R.drawable.bg_et_style_half_color_primary);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });
        etAceBuyPrice.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    if (spProduct.getSelectedItemPosition() == 0) {
                        AlertUtils.showToast(getActivity(), "Select product");
                        return true;
                    }
                    if (etAceSellPrice.getText().length() > 0) {
                        etAceSellPrice.setText("");
                        etAceSellPrice.setBackgroundResource(R.drawable.bg_et_style_half_disable);
                    }
                }
                return false;
            }
        });

        etAceSellPrice.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.length() > 0) {
                    etAceBuyPrice.setBackgroundResource(R.drawable.bg_et_style_half_disable);
                } else {
                    etAceBuyPrice.setBackgroundResource(R.drawable.bg_et_style_half_color_primary);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });
        etAceSellPrice.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    if (spProduct.getSelectedItemPosition() == 0) {
                        AlertUtils.showToast(getActivity(), "Select product");
                        return true;
                    }
                    if (etAceBuyPrice.getText().length() > 0) {
                        etAceBuyPrice.setText("");
                        etAceBuyPrice.setBackgroundResource(R.drawable.bg_et_style_half_disable);
                    }
                }
                return false;
            }
        });

        spProduct.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (i > 0) {
                    etValueRm.setBackgroundResource(R.drawable.bg_et_style_half_color_primary);
                    etTotalXauWtGm.setBackgroundResource(R.drawable.bg_et_style_half_color_primary);
                    etAceBuyPrice.setBackgroundResource(R.drawable.bg_et_style_half_color_primary);
                    etAceSellPrice.setBackgroundResource(R.drawable.bg_et_style_half_color_primary);
                } else {
                    etValueRm.setBackgroundResource(R.drawable.bg_et_style_half_disable);
                    etTotalXauWtGm.setBackgroundResource(R.drawable.bg_et_style_half_disable);
                    etAceBuyPrice.setBackgroundResource(R.drawable.bg_et_style_half_disable);
                    etAceSellPrice.setBackgroundResource(R.drawable.bg_et_style_half_disable);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });

        btnQueueOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                if (validation()) {
                    DialogSessionPinCode dialogSessionPinCode = DialogSessionPinCode.getInstance();
                    dialogSessionPinCode.show(getActivity().getSupportFragmentManager(), DialogSessionPinCode.TAG);
                    dialogSessionPinCode.setDialogInterface(new DialogSessionPinCode.OnDialogSessionPinCodeInterface() {
                        @Override
                        public void onDialogButtonClicked(boolean isPositive) {
                            AlertUtils.hideInputMethod(getActivity(), view);
                            if (isPositive) {
                                if (NetworkUtils.isInNetwork(getActivity())) {
                                    //save for back
                                    SharedPreferences.Editor editor = actPrefs.edit();
                                    editor.putInt(PREF_PORDUCT_POS, spProduct.getSelectedItemPosition());
                                    editor.putString(PREF_VALUE_RM, etValueRm.getText().toString().trim());
                                    editor.putString(PREF_XAU_WEIGHT, etTotalXauWtGm.getText().toString().trim());
                                    editor.putString(PREF_ACE_BUY, etAceBuyPrice.getText().toString().trim());
                                    editor.putString(PREF_ACE_SELL, etAceSellPrice.getText().toString().trim());
                                    editor.apply();

                                    //if ACE SELL PRICE --> bid/verify
                                    //if ACE Buy PRice --> ask to sell
                                    verifyBuySalesTask(TextUtils.isEmpty(etAceBuyPrice.getText().toString().trim()),
                                            productLists.get(spProduct.getSelectedItemPosition()),
                                            etValueRm.getText().toString().trim(), etTotalXauWtGm.getText().toString().trim(),
                                            (TextUtils.isEmpty(etAceSellPrice.getText().toString().trim()) ? etAceBuyPrice.getText().toString().trim()
                                                    : etAceSellPrice.getText().toString().trim()));
                                } else {
                                    AlertUtils.showSnack(getActivity(), view, getString(R.string.error_no_internet));
                                }
                            }
                        }
                    });
                }
            }
        });

        if (NetworkUtils.isInNetwork(getActivity())) {
            productsListingTask();
        } else {
            errorText.setText(getString(R.string.error_no_internet));
            errorText.setVisibility(View.VISIBLE);
        }
    }

    private void setProductSpinner() {
        ArrayAdapter<Product> spinnerAdapter = new ArrayAdapter<>(Objects.requireNonNull(getActivity()),
                R.layout.layout_profile_sp_textview, productLists);
        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spProduct.setAdapter(spinnerAdapter);
        spProduct.setSelection(actPrefs.getInt(PREF_PORDUCT_POS, 0));
    }

    private void verifyBuySalesTask(final boolean isBid, final Product product, final String byAmount,
                                    final String byWeight, String price) {
        final ProgressDialog pDialog = AlertUtils.showProgressDialog(getActivity(), "Verifying "
                + (isBid ? "bid" : "ask to sell"));

        VolleyHelper vHelper = VolleyHelper.getInstance(getActivity());
        HashMap<String, String> postMap = vHelper.getPostParams();
        postMap.put("prod_id", product.getpId());
        postMap.put("uom", "g");
        if (!TextUtils.isEmpty(byWeight)) {
            postMap.put("total_weight", byWeight);
            postMap.put("total_amount", "0");
        } else {
            postMap.put("total_amount", byAmount);
            postMap.put("total_weight", "0");
        }
        postMap.put("bookmethod", (TextUtils.isEmpty(byWeight) ? Product.P_BY_AMOUNT : Product.P_BY_WEIGHT));

        String url = "";
        if (isBid) {
            url = Api.getInstance().bidVerify;
            postMap.put("bid_price", price);
        } else {
            url = Api.getInstance().askVerify;
            postMap.put("ask_price", price);
        }

        vHelper.addVolleyRequestListeners(url, Request.Method.POST,
                postMap, new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(final String response) {
                        pDialog.dismiss();
                        try {
                            final JSONObject resObj = new JSONObject(response);
                            JSONObject resData = resObj.getJSONObject("ResponseData");

                            Fragment fragment = new FutureOrderConfirmation();
                            Bundle bundle = new Bundle();
                            bundle.putBoolean("isBid", isBid);
                            bundle.putSerializable("product", product);
                            bundle.putString("aceBuyPrice", etAceBuyPrice.getText().toString().trim());
                            bundle.putString("aceSellPrice", etAceSellPrice.getText().toString().trim());
                            bundle.putString(Product.P_BY_AMOUNT, byAmount);
                            bundle.putString(Product.P_BY_WEIGHT, byWeight);
                            bundle.putString("refine_fee", resData.optString("refine_fee"));
                            bundle.putString("premium_fee", resData.optString("premium_fee"));
                            bundle.putString("total_est_value", resData.getString("total_est_value"));
                            bundle.putString("xau_weight", resData.optString("xau_weight"));
                            bundle.putString("matching_bid_price", resData.optString("matching_bid_price"));
                            bundle.putString("final_bid_price", resData.optString("final_bid_price"));
                            bundle.putString("matching_ask_price", resData.optString("matching_ask_price"));
                            bundle.putString("final_ask_price", resData.optString("final_ask_price"));
                            fragment.setArguments(bundle);

                            ((MainActivity) getActivity()).openFragment(fragment, true);
                        } catch (JSONException e) {
                            Logger.e("verifyBuySalesTask res ex", e.getMessage() + " ");
                            try {
                                JSONObject errorObj = new JSONObject(response);
                                AlertUtils.showAppAlert(getActivity(), R.drawable.icon_not_accept,
                                        errorObj.getJSONObject("Details").getString("appCodeMessage"),
                                        "OK", null);
                            } catch (Exception ex) {
                                Logger.e("verifyBuySalesTask ex in ex", ex.getMessage() + " ");
                            }
                        }
                    }

                    @Override
                    public void onError(String errorResponse, VolleyError volleyError) {
                        pDialog.dismiss();
                        String errorMsg = "Unexpected error !!! Please try again with internet access.";
                        try {
                            JSONObject errorObj = new JSONObject(errorResponse);
                            errorMsg = errorObj.getJSONObject("Details").getString("appCodeMessage");
                        } catch (Exception e) {
                            Logger.e("verifyBuySalesTask error res", e.getMessage() + " ");
                        }
                        AlertUtils.showAppAlert(getActivity(), R.drawable.icon_not_accept, errorMsg, "OK", null);
                    }
                }, "verifyBuySalesTask");
    }

    private void productsListingTask() {
        progressBar.setVisibility(View.VISIBLE);
        mContent.setVisibility(View.GONE);

        VolleyHelper vHelper = VolleyHelper.getInstance(getActivity());
        HashMap<String, String> postMap = vHelper.getPostParams();

        vHelper.addVolleyRequestListeners(Api.getInstance().productList, Request.Method.GET,
                postMap, new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(final String response) {
                        progressBar.setVisibility(View.GONE);
                        try {
                            final JSONObject resObj = new JSONObject(response);
                            JSONArray resArray = resObj.getJSONArray("ResponseData");
                            if (resArray.length() >= 2) {
                                JSONObject jsonObject = resArray.getJSONObject(1);
                                JSONArray productArray = jsonObject.getJSONArray("Products");
                                productLists.clear();
                                Product defPro = new Product();
                                defPro.setpName("Select Product");
                                defPro.setpId("-1");
                                productLists.add(defPro);
                                for (int i = 0; i < productArray.length(); i++) {
                                    JSONObject eachProduct = productArray.getJSONObject(i);
                                    Product product = new Product();
                                    product.setpName(eachProduct.getString("product_name"));
                                    product.setpId(eachProduct.getString("product_id"));
                                    product.setRefineFee(eachProduct.getString("refine_fee"));
                                    product.setPremiumFee(eachProduct.getString("premium_fee"));

                                    productLists.add(product);
                                }
                                setProductSpinner();
                                mContent.setVisibility(View.VISIBLE);
                            }
                        } catch (JSONException e) {
                            Logger.e("productsListingTask res ex", e.getMessage() + " ");
                        }
                    }

                    @Override
                    public void onError(String errorResponse, VolleyError volleyError) {
                        progressBar.setVisibility(View.GONE);
                        String errorMsg = "Unexpected error !!! Please try again with internet access.";
                        try {
                            JSONObject errorObj = new JSONObject(errorResponse);
                            errorMsg = errorObj.getJSONObject("Details").getString("appCodeMessage");
                        } catch (Exception e) {
                            Logger.e("productsListingTask error res", e.getMessage() + " ");
                        }
                        errorText.setText(Html.fromHtml(errorMsg));
                        errorText.setVisibility(View.VISIBLE);
                    }
                }, "productsListingTask");
    }

    @Override
    public void onDetach() {
        VolleyHelper.getInstance(getActivity()).cancelRequest("productsListingTask");
        VolleyHelper.getInstance(getActivity()).cancelRequest("verifyBuySalesTask");
        super.onDetach();
    }

    private boolean validation() {
        boolean isValid = true;

        if (spProduct.getSelectedItemPosition() < 1) {
            AlertUtils.showSnack(getActivity(), spProduct, "Please select a product");
            return false;
        }
        if (TextUtils.isEmpty(etValueRm.getText().toString().trim()) && TextUtils.isEmpty(etTotalXauWtGm.getText().toString().trim())) {
            AlertUtils.showSnack(getActivity(), spProduct, "Please enter either value or weight to place order");
            return false;
        }
        if (TextUtils.isEmpty(etAceBuyPrice.getText().toString().trim()) && TextUtils.isEmpty(etAceSellPrice.getText().toString().trim())) {
            AlertUtils.showSnack(getActivity(), spProduct, "Please enter either ACE buy price or ACE sell price");
            return false;
        }

//        if (etValueRm.isEnabled()) {
//            if (etValueRm.getText().toString().trim().equals("")) {
//                isValid = false;
//                tvErrValueRm.setVisibility(View.VISIBLE);
//            } else {
//                tvErrValueRm.setVisibility(View.GONE);
//            }
//        } else {
//            tvErrValueRm.setVisibility(View.GONE);
//        }
//
//        if (etTotalXauWtGm.isEnabled()) {
//            if (etTotalXauWtGm.getText().toString().trim().equals("")) {
//                isValid = false;
//                tvErrTotalXauWtGm.setVisibility(View.VISIBLE);
//            } else {
//                tvErrTotalXauWtGm.setVisibility(View.GONE);
//            }
//        } else {
//            tvErrTotalXauWtGm.setVisibility(View.GONE);
//        }
//
//        if (etAceBuyPrice.isEnabled()) {
//            if (etAceBuyPrice.getText().toString().trim().equals("")) {
//                isValid = false;
//                tvErrAceBuyPrice.setVisibility(View.VISIBLE);
//            } else {
//                tvErrAceBuyPrice.setVisibility(View.GONE);
//            }
//        } else {
//            tvErrAceBuyPrice.setVisibility(View.GONE);
//        }
//
//        if (etAceSellPrice.isEnabled()) {
//            if (etAceSellPrice.getText().toString().trim().equals("")) {
//                isValid = false;
//                tvErrAceSellPrice.setVisibility(View.VISIBLE);
//            } else {
//                tvErrAceSellPrice.setVisibility(View.GONE);
//            }
//        } else {
//            tvErrAceSellPrice.setVisibility(View.GONE);
//        }

        return isValid;
    }
}
