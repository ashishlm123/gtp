package com.emts.gtp.fragment;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.bumptech.glide.Glide;
import com.emts.gtp.Api;
import com.emts.gtp.R;
import com.emts.gtp.helper.AlertUtils;
import com.emts.gtp.helper.Logger;
import com.emts.gtp.helper.NetworkUtils;
import com.emts.gtp.helper.VolleyHelper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;


public class AboutUs extends Fragment {
    ProgressBar progressBar;
    TextView errorText, tvCompanyDesc;
    LinearLayout l1;
    ImageView staticMap;
    String latAceCapital = "3.048536";
    String lngAceCapital = "101.584253";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_about_us, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        progressBar = view.findViewById(R.id.progress_bar);
        l1 = view.findViewById(R.id.l1);
        errorText = view.findViewById(R.id.error_text);


        tvCompanyDesc = view.findViewById(R.id.tv_company_desc);
        staticMap = view.findViewById(R.id.static_map);

        Uri gmmIntentUri = Uri.parse("geo:" + latAceCapital + "," + lngAceCapital + "?q=Ace Capital Growth Sdn Bhd");
        final Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);

        TextView viewLargerMap = view.findViewById(R.id.tv_view_larger_map);
        viewLargerMap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mapIntent.setPackage("com.google.android.apps.maps");
                startActivity(mapIntent);
            }
        });


        if (NetworkUtils.isInNetwork(getActivity())) {
            aboutUsTask();
        } else {
            AlertUtils.showSnack(getActivity(), progressBar, getResources().getString(R.string.error_no_internet));
        }

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    private void aboutUsTask() {
        progressBar.setVisibility(View.VISIBLE);
        l1.setVisibility(View.GONE);
        errorText.setVisibility(View.GONE);
        VolleyHelper vHelper = VolleyHelper.getInstance(getActivity());
        HashMap<String, String> postParams = vHelper.getPostParams();
        vHelper.addVolleyRequestListeners(Api.getInstance().aboutUs, Request.Method.GET, postParams, new VolleyHelper.VolleyHelperInterface() {
            @Override
            public void onSuccess(String response) {
                try {
                    progressBar.setVisibility(View.GONE);
                    l1.setVisibility(View.VISIBLE);
                    JSONObject res = new JSONObject(response);
                    JSONArray responseData = res.getJSONArray("ResponseData");

                    if (responseData.length() >= 1) {
                        JSONObject det = responseData.getJSONObject(0);
                        JSONObject details = det.getJSONObject("Details");
                        if (details.getString("appCode").equals(Api.APP_STATUS_OK)) {
                            JSONObject abt = responseData.getJSONObject(1);
                            JSONObject about = abt.getJSONObject("article");
                            tvCompanyDesc.setText(Html.fromHtml(about.getString("article_body")));

                            staticMap.post(new Runnable() {
                                @Override
                                public void run() {
                                    String marker = "&markers=icon:https://openclipart.org/image/800px/svg_to_png/169839/map_pin.png|"
                                            + latAceCapital + "," + lngAceCapital + "color:0x0000FF80|weight:5|" + latAceCapital + "," +
                                            lngAceCapital + "&size=175x175";
                                    String url = "http://maps.google.com/maps/api/staticmap?center=" + latAceCapital
                                            + "," + lngAceCapital + "&zoom=18&size=" + staticMap.getWidth() + "x" +
                                            staticMap.getHeight() + "&sensor=true" + marker;
                                    Logger.e("url ", url);
                                    Glide.with(getActivity()).load(url).into(staticMap);
                                }
                            });

                        } else {
                            l1.setVisibility(View.GONE);
                            progressBar.setVisibility(View.GONE);
                            errorText.setText(details.getString("appCodeMessage"));
                            errorText.setVisibility(View.VISIBLE);
                        }
                    }

                } catch (JSONException ex) {
                    Logger.e("aboutUsTask json ex: ", ex.getMessage());
                }
            }

            @Override
            public void onError(String errorResponse, VolleyError volleyError) {
                progressBar.setVisibility(View.GONE);
                l1.setVisibility(View.GONE);
                String errorMsg = "Unexpected error !!! Please try again with internet access.";
                try {
                    JSONObject errorObj = new JSONObject(errorResponse);
                    errorMsg = errorObj.getJSONObject("Details").getString("appCodeMessage");
                } catch (Exception e) {
                    Logger.e("aboutUsTask error res", e.getMessage() + " ");
                }
                Logger.e("error message", errorMsg);
                errorText.setText(errorMsg);
                errorText.setVisibility(View.VISIBLE);
            }
        }, "aboutUsTask");
    }
}
