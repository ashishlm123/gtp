package com.emts.gtp.fragment;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.emts.gtp.MainActivity;
import com.emts.gtp.R;
import com.emts.gtp.helper.AlertUtils;
import com.emts.gtp.model.Product;

import java.util.Objects;

/**
 * A simple {@link Fragment} subclass.
 */
public class FutureOrderComplete extends Fragment {
    boolean isBid;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_future_order_complete, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Bundle bundle = getArguments();
        if (bundle == null) {
            return;
        }
        ((MainActivity) Objects.requireNonNull(getActivity())).setToolbarTitle(false, "Future Order Complete");

        //isBid -> Sell
        //but sell to ACE means user BUY confirmation
        // Hence, isBid --> Buy
        isBid = bundle.getBoolean("isBid", false);
        final Product product = (Product) bundle.getSerializable("product");
        if (product == null) {
            AlertUtils.showToast(getActivity(), "Product cannot be null");
            return;
        }

        TextView tvProductType = view.findViewById(R.id.tv_product_type);
        TextView tvXauWeight = view.findViewById(R.id.tv_xau_weight);
        TextView tvAceBuyMatchingPrice = view.findViewById(R.id.tv_ace_buy_matching_price);
        TextView tvFinalBuyPrice = view.findViewById(R.id.tv_final_buy_price);
        TextView tvTotalEstVal = view.findViewById(R.id.tv_total_est_val);
        TextView tvLvlMatchingPrice = view.findViewById(R.id.tvLvlMatchingPrice);
        TextView tvLvlFinalPrice = view.findViewById(R.id.tvLvlFinalPrice);

        tvProductType.setText(product.getpName());
        tvXauWeight.setText(bundle.getString("xau_weight"));
        tvTotalEstVal.setText(getString(R.string.currenty_rm) + " " + bundle.getString("total_est_value"));

        if (isBid) {
            tvLvlMatchingPrice.setText("Ace Sell Matching Price(RM/g):");
            tvLvlFinalPrice.setText("Final Sell Price:");
//            tvAceBuyMatchingPrice.setText(String.format("%,.2f", Double.parseDouble(bundle.getString("matching_bid_price"))));
//            tvFinalBuyPrice.setText(getString(R.string.currenty_rm) + " " + String.format("%,.2f", Double.parseDouble(bundle.getString("final_bid_price"))));
            tvAceBuyMatchingPrice.setText(bundle.getString("matching_bid_price"));
            tvFinalBuyPrice.setText(getString(R.string.currenty_rm) + " " + bundle.getString("final_bid_price"));
        } else {
            tvLvlMatchingPrice.setText("Ace Buy Matching Price(RM/g):");
            tvLvlFinalPrice.setText("Final Buy Price:");
//            tvAceBuyMatchingPrice.setText(String.format("%,.2f", Double.parseDouble(bundle.getString("matching_ask_price"))));
//            tvFinalBuyPrice.setText(getString(R.string.currenty_rm) + " " + String.format("%,.2f", Double.parseDouble(bundle.getString("final_ask_price"))));
            tvAceBuyMatchingPrice.setText(bundle.getString("matching_ask_price"));
            tvFinalBuyPrice.setText(getString(R.string.currenty_rm) + " " + bundle.getString("final_ask_price"));
        }

        Button btnOk = view.findViewById(R.id.btn_ok);
        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().onBackPressed();
            }
        });

    }
}
