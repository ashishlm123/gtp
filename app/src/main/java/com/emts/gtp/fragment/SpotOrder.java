package com.emts.gtp.fragment;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.NestedScrollView;
import android.text.Editable;
import android.text.Html;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.emts.gtp.Api;
import com.emts.gtp.DialogSessionPinCode;
import com.emts.gtp.LivePriceSocketService;
import com.emts.gtp.MainActivity;
import com.emts.gtp.R;
import com.emts.gtp.helper.AlertUtils;
import com.emts.gtp.helper.Logger;
import com.emts.gtp.helper.NetworkUtils;
import com.emts.gtp.helper.VolleyHelper;
import com.emts.gtp.model.Product;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Objects;

public class SpotOrder extends Fragment {
    NestedScrollView mContent;
    ProgressBar progressBar;
    TextView errorText, tvStatusOnline;
    Spinner spProduct;
    ArrayList<Product> productLists = new ArrayList<>();
    static int previousProductSelection = 0;
//    Socket mSocket;

    LinearLayout itemAceBuy, itemAceSell;
    ImageView buyPriceBehaveIcon, sellPriceBehaveIcon;
    TextView tvBuyProductPrice, tvSellProductPrice;

    //for frequent change
    Bitmap iconStable, iconIncreasing, iconDecreasing;
    int colorStable, colorIncreasing, colorDecreasing;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        previousProductSelection = 0;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_order_dashboard, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mContent = view.findViewById(R.id.content);
        progressBar = view.findViewById(R.id.progress);
        errorText = view.findViewById(R.id.error_text);

        tvStatusOnline = view.findViewById(R.id.tv_status);
        spProduct = view.findViewById(R.id.sp_product);
        final EditText etProductValues = view.findViewById(R.id.et_product_values);
//        etProductValues.setText("");
        final EditText etXauWeight = view.findViewById(R.id.et_xau_weight);
//        etXauWeight.setText("");

        etProductValues.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.length() > 0) {
                    etXauWeight.setBackgroundResource(R.drawable.bg_et_style_half_disable);
                } else {
                    etXauWeight.setBackgroundResource(R.drawable.bg_et_style_half_color_primary);
                }
                if (etXauWeight.getText().length() > 0) {
                    etXauWeight.setText("");
                }
                etProductValues.setBackgroundResource(R.drawable.bg_et_style_half_color_primary);
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });
        etProductValues.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    if (spProduct.getSelectedItemPosition() < 1) {
                        AlertUtils.showToast(getActivity(), "Select product");
                        return true;
                    }
                    if (etXauWeight.getText().length() > 0) {
                        etXauWeight.setText("");
                    }
                    etXauWeight.setBackgroundResource(R.drawable.bg_et_style_half_disable);
                    etProductValues.setBackgroundResource(R.drawable.bg_et_style_half_color_primary);
                }
                return false;
            }
        });

        etXauWeight.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.length() > 0) {
                    etProductValues.setBackgroundResource(R.drawable.bg_et_style_half_disable);
                } else {
                    etProductValues.setBackgroundResource(R.drawable.bg_et_style_half_color_primary);
                }
                if (etProductValues.getText().length() > 0) {
                    etProductValues.setText("");
                }
                etXauWeight.setBackgroundResource(R.drawable.bg_et_style_half_color_primary);
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });
        etXauWeight.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    if (spProduct.getSelectedItemPosition() < 1) {
                        AlertUtils.showToast(getActivity(), "Select product");
                        return true;
                    }
                    if (etProductValues.getText().length() > 0) {
                        etProductValues.setText("");
                    }
                    etProductValues.setBackgroundResource(R.drawable.bg_et_style_half_disable);
                    etXauWeight.setBackgroundResource(R.drawable.bg_et_style_half_color_primary);
                }
                return false;
            }
        });

        spProduct.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                previousProductSelection = i;
                if (i > 0) {
                    etProductValues.setBackgroundResource(R.drawable.bg_et_style_half_color_primary);
                    etXauWeight.setBackgroundResource(R.drawable.bg_et_style_half_color_primary);
                } else {
                    etProductValues.setBackgroundResource(R.drawable.bg_et_style_half_disable);
                    etXauWeight.setBackgroundResource(R.drawable.bg_et_style_half_disable);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        itemAceBuy = view.findViewById(R.id.item_buy);
        TextView tvAceBuy = itemAceBuy.findViewById(R.id.tv_ace_buy_sell);
        tvAceBuy.setText("Ace Buy");
        buyPriceBehaveIcon = itemAceBuy.findViewById(R.id.price_behaviour_icon);
        tvBuyProductPrice = itemAceBuy.findViewById(R.id.product_price);

        itemAceBuy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (spProduct.getSelectedItemPosition() < 1) {
                    AlertUtils.showSnack(getActivity(), view, "Please select a product");
                    return;
                }
                if (TextUtils.isEmpty(etProductValues.getText().toString().trim()) && TextUtils.isEmpty(etXauWeight.getText().toString().trim())) {
                    AlertUtils.showSnack(getActivity(), view, "Please enter either value or weight to buy the product");
                    return;
                }
                DialogSessionPinCode mDialog = DialogSessionPinCode.getInstance();
                mDialog.setDialogInterface(new DialogSessionPinCode.OnDialogSessionPinCodeInterface() {
                    @Override
                    public void onDialogButtonClicked(boolean isPositive) {
                        if (isPositive) {
//                            Fragment fragment = new SpotOrderConfirmation();
//                            Bundle bundle = new Bundle();
//                            bundle.putBoolean("isSell", false);
//                            bundle.putSerializable("product", pCodeList.get(spProduct.getSelectedItemPosition()));
//                            bundle.putString(Product.P_BY_AMOUNT, etProductValues.getText().toString().trim());
//                            bundle.putString(Product.P_BY_WEIGHT, etXauWeight.getText().toString().trim());
//                            fragment.setArguments(bundle);
//
//                            ((MainActivity) getActivity()).openFragment(fragment);

                            verifyBuySalesTask(true, productLists.get(spProduct.getSelectedItemPosition()),
                                    etProductValues.getText().toString().trim(), etXauWeight.getText().toString().trim(),
                                    latestBuyPrice, latestSellPrice);
                        }
                    }
                });
                mDialog.show(getActivity().getSupportFragmentManager(), DialogSessionPinCode.TAG);
            }
        });

        itemAceSell = view.findViewById(R.id.item_sell);
        TextView tvAceSell = itemAceSell.findViewById(R.id.tv_ace_buy_sell);
        tvAceSell.setText("Ace Sell");
        sellPriceBehaveIcon = itemAceSell.findViewById(R.id.price_behaviour_icon);
        tvSellProductPrice = itemAceSell.findViewById(R.id.product_price);

        itemAceSell.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (spProduct.getSelectedItemPosition() < 1) {
                    AlertUtils.showSnack(getActivity(), view, "Please select a product");
                    return;
                }
                if (TextUtils.isEmpty(etProductValues.getText().toString().trim()) && TextUtils.isEmpty(etXauWeight.getText().toString().trim())) {
                    AlertUtils.showSnack(getActivity(), view, "Please enter either value or weight to sell the product");
                    return;
                }
                DialogSessionPinCode mDialog = DialogSessionPinCode.getInstance();
                mDialog.setDialogInterface(new DialogSessionPinCode.OnDialogSessionPinCodeInterface() {
                    @Override
                    public void onDialogButtonClicked(boolean isPositive) {
                        if (isPositive) {
//                            Fragment fragment = new SpotOrderConfirmation();
//                            Bundle bundle = new Bundle();
//                            bundle.putBoolean("isSell", true);
//                            fragment.setArguments(bundle);
//
//                            ((MainActivity) getActivity()).openFragment(fragment);

                            verifyBuySalesTask(false, productLists.get(spProduct.getSelectedItemPosition()),
                                    etProductValues.getText().toString().trim(), etXauWeight.getText().toString().trim(),
                                    latestBuyPrice, latestSellPrice);
                        }
                    }
                });
                mDialog.show(getActivity().getSupportFragmentManager(), DialogSessionPinCode.TAG);
            }
        });

        if (NetworkUtils.isInNetwork(getActivity())) {
            productsListingTask();
        } else {
            errorText.setText(getString(R.string.error_no_internet));
            errorText.setVisibility(View.VISIBLE);
        }

        //ready the icons and color
        Resources res = getResources();

        iconStable = BitmapFactory.decodeResource(res, R.drawable.icon_stable);
        iconIncreasing = BitmapFactory.decodeResource(res, R.drawable.icon_increase);
        iconDecreasing = BitmapFactory.decodeResource(res, R.drawable.icon_decrease);

        colorStable = res.getColor(R.color.priceStable);
        colorIncreasing = res.getColor(R.color.priceIncrease);
        colorDecreasing = res.getColor(R.color.priceDecline);

//        connectToServerSocket();
    }

    private void setProductSpinner() {
        ArrayAdapter<Product> spinnerAdapter = new ArrayAdapter<>(Objects.requireNonNull(getActivity()),
                R.layout.layout_profile_sp_textview, productLists);
        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spProduct.setAdapter(spinnerAdapter);
        spProduct.setSelection(previousProductSelection);
    }

    private void verifyBuySalesTask(final boolean isBuy, final Product product, final String byAmount,
                                    final String byWeight, double latestBuyPrice, double latestSellPrice) {
        final ProgressDialog pDialog = AlertUtils.showProgressDialog(getActivity(), "Verifying "
                + (isBuy ? "purchase" : "sales"));

        String url = "";
        if (isBuy) {
            url = Api.getInstance().purchaseVerify;
        } else {
            url = Api.getInstance().salesVerify;
        }

        VolleyHelper vHelper = VolleyHelper.getInstance(getActivity());
        HashMap<String, String> postMap = vHelper.getPostParams();
        postMap.put("prod_id", product.getpId());
        postMap.put("uom", "g");
        if (!TextUtils.isEmpty(byWeight)) {
            postMap.put("total_weight", byWeight);
            postMap.put("total_amount", "0");
        } else {
            double amount = Double.parseDouble(byAmount);
            double weight = 0.0;
            if (isBuy) {
                weight = amount / latestBuyPrice;
            } else {
                weight = amount / latestSellPrice;
            }
            postMap.put("total_weight", String.valueOf(weight));
            postMap.put("total_amount", byAmount);
        }
        postMap.put("bookmethod", (TextUtils.isEmpty(byWeight) ? Product.P_BY_AMOUNT : Product.P_BY_WEIGHT));

        vHelper.addVolleyRequestListeners(url, Request.Method.POST,
                postMap, new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(final String response) {
                        pDialog.dismiss();
                        try {
                            final JSONObject resObj = new JSONObject(response);
                            JSONObject resArray = resObj.getJSONObject("ResponseData");
                            if (resArray.getJSONObject("Details").getInt("appCode") == 1000) {
                                Fragment fragment = new SpotOrderConfirmation();
                                Bundle bundle = new Bundle();
                                bundle.putBoolean("isSell", !isBuy);
                                bundle.putSerializable("product", product);
                                bundle.putString(Product.P_BY_AMOUNT, byAmount);
                                bundle.putString(Product.P_BY_WEIGHT, byWeight);
                                fragment.setArguments(bundle);

                                ((MainActivity) getActivity()).openFragment(fragment, true);
                            } else {
                                AlertUtils.showAppAlert(getActivity(), R.drawable.icon_not_accept,
                                        resArray.getJSONObject("Details").getString("appCodeMessage"),
                                        "OK", null);
                            }
                        } catch (JSONException e) {
                            Logger.e("verifyBuySalesTask res ex", e.getMessage() + " ");
                            try {
                                JSONObject errorObj = new JSONObject(response);
                                AlertUtils.showAppAlert(getActivity(), R.drawable.icon_not_accept,
                                        errorObj.getJSONObject("Details").getString("appCodeMessage"),
                                        "OK", null);
                            } catch (Exception ex) {
                                Logger.e("verifyBuySalesTask ex in ex", ex.getMessage() + " ");
                            }
                        }
                    }

                    @Override
                    public void onError(String errorResponse, VolleyError volleyError) {
                        pDialog.dismiss();
                        String errorMsg = "Unexpected error !!! Please try again with internet access.";
                        try {
                            JSONObject errorObj = new JSONObject(errorResponse);
                            errorMsg = errorObj.getJSONObject("Details").getString("appCodeMessage");
                        } catch (Exception e) {
                            Logger.e("verifyBuySalesTask error res", e.getMessage() + " ");
                        }
                        AlertUtils.showAppAlert(getActivity(), R.drawable.icon_not_accept, errorMsg, "OK", null);
                    }
                }, "verifyBuySalesTask");
    }

    private void productsListingTask() {
        progressBar.setVisibility(View.VISIBLE);

        VolleyHelper vHelper = VolleyHelper.getInstance(getActivity());
        HashMap<String, String> postMap = vHelper.getPostParams();

        vHelper.addVolleyRequestListeners(Api.getInstance().productList, Request.Method.GET,
                postMap, new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(final String response) {
                        progressBar.setVisibility(View.GONE);
                        try {
                            final JSONObject resObj = new JSONObject(response);
                            JSONArray resArray = resObj.getJSONArray("ResponseData");
                            productLists.clear();
                            if (resArray.length() >= 2) {
                                JSONObject jsonObject = resArray.getJSONObject(1);
                                JSONArray productArray = jsonObject.getJSONArray("Products");
                                Product defPro = new Product();
                                defPro.setpName("Select Product");
                                defPro.setpId("-1");
                                productLists.add(defPro);
                                for (int i = 0; i < productArray.length(); i++) {
                                    JSONObject eachProduct = productArray.getJSONObject(i);
                                    Product product = new Product();
                                    product.setpName(eachProduct.getString("product_name"));
                                    product.setpId(eachProduct.getString("product_id"));
                                    product.setRefineFee(eachProduct.getString("refine_fee"));
                                    product.setPremiumFee(eachProduct.getString("premium_fee"));

                                    productLists.add(product);
                                }
                                setProductSpinner();
                                mContent.setVisibility(View.VISIBLE);
                            }
                        } catch (JSONException e) {
                            Logger.e("productsListingTask res ex", e.getMessage() + " ");
                        }
                    }

                    @Override
                    public void onError(String errorResponse, VolleyError volleyError) {
                        progressBar.setVisibility(View.GONE);
                        String errorMsg = "Unexpected error !!! Please try again with internet access.";
                        try {
                            JSONObject errorObj = new JSONObject(errorResponse);
                            errorMsg = errorObj.getJSONObject("Details").getString("appCodeMessage");
                        } catch (Exception e) {
                            Logger.e("productsListingTask error res", e.getMessage() + " ");
                        }
                        errorText.setText(Html.fromHtml(errorMsg));
                        errorText.setVisibility(View.VISIBLE);
                    }
                }, "productsListingTask");
    }

    @Override
    public void onDetach() {
        super.onDetach();

        VolleyHelper.getInstance(getActivity()).cancelRequest("productsListingTask");
//        mSocket.disconnect();
//        mSocket.off();
    }

    //    private void connectToServerSocket() {
//        try {
//            if (mSocket != null) {
//                if (mSocket.connected()) {
//                    mSocket.off();
//                    mSocket.disconnect();
//                    mSocket.off();
//                }
//            }
//
////            IO.Options opts = new IO.Options();
//////            opts.forceNew = true;
//////            opts.reconnection = true;
//////        opts.query = "auth_token=" + authToken;
////            opts.transports = new String[]{WebSocket.NAME};
//
//            String nodeServerHostUrl = Api.getInstance().socketUrl;
//            mSocket = IO.socket(nodeServerHostUrl);
//            mSocket.on(Socket.EVENT_CONNECT, new Emitter.Listener() {
//                @Override
//                public void call(Object... args) {
//                    Logger.e("nodeJs def connect call", "Object : ");
//                    if (mSocket.connected()) {
//                        Logger.e("nodeJs connected", mSocket.toString() + "");
//                    }
//                }
//            }).on(Socket.EVENT_DISCONNECT, new Emitter.Listener() {
//                @Override
//                public void call(Object... args) {
//                    Logger.e("nodeJs def. disconnect", "Object " + args[0]);
//                }
//            });
//            //listen to node before connect
//            listenToHeartBeat();
//            listenToIntIX();
//            listenToTradeOpen();
//
//            mSocket.connect();
//            Logger.e("connectToServerSocket", ">>>>>>>>>>CONNECTING>>>>>>>>>>\n" + nodeServerHostUrl);
//        } catch (URISyntaxException e) {
//            Logger.e("connectToServerSocket ex", e.getMessage() + " ");
//        }
//    }
//
//    public void listenToHeartBeat() {
//        Logger.e("listenToHeartBeat EVENT:", "goodfeed");
//        mSocket.off("goodfeed");
//        mSocket.on("goodfeed", new Emitter.Listener() {
//            @Override
//            public void call(Object... args) {
//                Logger.e("listenToHeartBeat call", args[0].toString() + "");
//            }
//        });
//    }
//
    double latestBuyPrice, latestSellPrice, prevBuyPrice, prevSellPrice;
//
//    public void listenToIntIX() {
////        Logger.e("listenToIntIX EVENT:", "intlX");
//        mSocket.off("intlX");
//        mSocket.on("intlX", new Emitter.Listener() {
//            @Override
//            public void call(final Object... args) {
////                Logger.e("listenToIntIX call", args[0].toString() + "");
//                if (getActivity() != null)
//                    getActivity().runOnUiThread(new Runnable() {
//                        @Override
//                        public void run() {
//                            try {
//                                final JSONObject eachChat = new JSONObject(args[0].toString());
//                                Double liveBuyPrice = Double.parseDouble(eachChat.getString("gp_livebuyprice_gm"));
//                                Double liveSellPrice = Double.parseDouble(eachChat.getString("gp_livesellprice_gm"));
//
//            tvBuyProductPrice.setText("RM " + String.format("%.3f", liveBuyPrice));
//            tvSellProductPrice.setText("RM " + String.format("%.3f", liveSellPrice));
//                                prevBuyPrice = latestBuyPrice;
//                                prevSellPrice = latestSellPrice;
//                                latestBuyPrice = liveBuyPrice;
//                                latestSellPrice = liveSellPrice;
//
//                                if (prevSellPrice == latestSellPrice) {
//                                    itemAceSell.setBackgroundColor(colorStable);
//                                    sellPriceBehaveIcon.setImageBitmap(iconStable);
//                                } else if (prevSellPrice < latestSellPrice) {
//                                    itemAceSell.setBackgroundColor(colorIncreasing);
//                                    sellPriceBehaveIcon.setImageBitmap(iconIncreasing);
//                                } else {
//                                    itemAceSell.setBackgroundColor(colorDecreasing);
//                                    sellPriceBehaveIcon.setImageBitmap(iconDecreasing);
//                                }
//                                if (prevBuyPrice == latestBuyPrice) {
//                                    itemAceBuy.setBackgroundColor(colorStable);
//                                    buyPriceBehaveIcon.setImageBitmap(iconStable);
//                                } else if (prevBuyPrice < latestBuyPrice) {
//                                    itemAceBuy.setBackgroundColor(colorIncreasing);
//                                    buyPriceBehaveIcon.setImageBitmap(iconIncreasing);
//                                } else {
//                                    itemAceBuy.setBackgroundColor(colorDecreasing);
//                                    buyPriceBehaveIcon.setImageBitmap(iconDecreasing);
//                                }
//                            } catch (Exception e) {
//                                Logger.e("listenToIntIX onUI thread ex", e.getMessage() + " ");
//                            }
//                        }
//                    });
//            }
//        });
//    }
//
//    public void listenToTradeOpen() {
//        Logger.e("listenToTradeOpen EVENT:", "tradeopen");
//        mSocket.off("tradeopen");
//        mSocket.on("tradeopen", new Emitter.Listener() {
//            @Override
//            public void call(final Object... args) {
//                Logger.e("listenToTradeOpen call", args[0].toString() + "");
//                getActivity().runOnUiThread(new Runnable() {
//                    @Override
//                    public void run() {
//                        try {
//                            if (args[0].toString().equals("true")) {
//                                tvStatusOnline.setText("Online");
//                            } else {
//                                tvStatusOnline.setText("Offline");
//                            }
//                        } catch (Exception e) {
//                            Logger.e("listenToTradeOpen onUI thread ex", e.getMessage() + " ");
//                        }
//                    }
//                });
//            }
//        });
//    }

    @Override
    public void onStart() {
        super.onStart();
        Objects.requireNonNull(getActivity()).startService(new Intent(getActivity(), LivePriceSocketService.class));
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
//        Objects.requireNonNull(getActivity()).stopService(new Intent(getActivity(), LivePriceSocketService.class));
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(LivePriceSocketService.TradeOpenMesObj event) {
        try {
            if (event.msg.equals("true")) {
                tvStatusOnline.setText("Online");
            } else {
                tvStatusOnline.setText("Offline");
            }
        } catch (Exception e) {
            Logger.e("listenToTradeOpen onUI thread ex", e.getMessage() + " ");
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(LivePriceSocketService.IntIXMessageObj event) {
//        Logger.e("onMessageEvent()", "initIX :" + event.msg);
        try {
            final JSONObject eachChat = new JSONObject(event.msg);
            Double liveBuyPrice = Double.parseDouble(eachChat.getString("gp_livebuyprice_gm"));
            Double liveSellPrice = Double.parseDouble(eachChat.getString("gp_livesellprice_gm"));

            tvBuyProductPrice.setText("RM " + String.format("%.3f", liveBuyPrice));
            tvSellProductPrice.setText("RM " + String.format("%.3f", liveSellPrice));
            prevBuyPrice = latestBuyPrice;
            prevSellPrice = latestSellPrice;
            latestBuyPrice = liveBuyPrice;
            latestSellPrice = liveSellPrice;

            if (prevSellPrice == latestSellPrice) {
                itemAceSell.setBackgroundColor(colorStable);
                sellPriceBehaveIcon.setImageBitmap(iconStable);
            } else if (prevSellPrice < latestSellPrice) {
                itemAceSell.setBackgroundColor(colorIncreasing);
                sellPriceBehaveIcon.setImageBitmap(iconIncreasing);
            } else {
                itemAceSell.setBackgroundColor(colorDecreasing);
                sellPriceBehaveIcon.setImageBitmap(iconDecreasing);
            }
            if (prevBuyPrice == latestBuyPrice) {
                itemAceBuy.setBackgroundColor(colorStable);
                buyPriceBehaveIcon.setImageBitmap(iconStable);
            } else if (prevBuyPrice < latestBuyPrice) {
                itemAceBuy.setBackgroundColor(colorIncreasing);
                buyPriceBehaveIcon.setImageBitmap(iconIncreasing);
            } else {
                itemAceBuy.setBackgroundColor(colorDecreasing);
                buyPriceBehaveIcon.setImageBitmap(iconDecreasing);
            }
        } catch (Exception e) {
            Logger.e("listenToIntIX onUI thread ex", e.getMessage() + " ");
        }
    }
}
