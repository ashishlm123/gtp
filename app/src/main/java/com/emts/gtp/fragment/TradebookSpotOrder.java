package com.emts.gtp.fragment;


import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.emts.gtp.Api;
import com.emts.gtp.R;
import com.emts.gtp.activity.TradebookSpotOrderFilterActivity;
import com.emts.gtp.adapter.SpotOrderAdapter;
import com.emts.gtp.helper.Logger;
import com.emts.gtp.helper.NetworkUtils;
import com.emts.gtp.helper.VolleyHelper;
import com.emts.gtp.model.OrderModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

public class TradebookSpotOrder extends Fragment {
    int REQUEST_CODE = 111;
    String status = "";
    String startDate = "";
    String endDate = "";
    RecyclerView rvSpotOrderListings;
    ProgressBar progressBar, infiniteProgressBar;
    TextView tvErrorText;
    ArrayList<OrderModel> spotOrderLists, filterLists;
    SpotOrderAdapter spotOrderAdapter;
    int limit = 10;
    int offset = 0;
    ImageView filterIcon;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_tradebook_spot_order, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        progressBar = view.findViewById(R.id.progress_bar);
        infiniteProgressBar = view.findViewById(R.id.infinite_progress_bar);
        tvErrorText = view.findViewById(R.id.error_text);

        rvSpotOrderListings = view.findViewById(R.id.recycler_view);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        rvSpotOrderListings.setLayoutManager(linearLayoutManager);

        spotOrderLists = new ArrayList<>();
        filterLists = new ArrayList<>();
        spotOrderAdapter = new SpotOrderAdapter(getActivity(), filterLists);
        rvSpotOrderListings.setAdapter(spotOrderAdapter);

        if (NetworkUtils.isInNetwork(getActivity())) {
            spotOrderListingsTask();
        } else {
            progressBar.setVisibility(View.GONE);
            rvSpotOrderListings.setVisibility(View.GONE);
            tvErrorText.setText(getResources().getString(R.string.error_no_internet));
            tvErrorText.setVisibility(View.VISIBLE);
        }

        filterIcon = view.findViewById(R.id.ico_filter);
        filterIcon.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), TradebookSpotOrderFilterActivity.class);
                intent.putExtra("status", status);
                intent.putExtra("startDate", startDate);
                intent.putExtra("endDate", endDate);
                startActivityForResult(intent, REQUEST_CODE);
            }
        });
    }

    private void spotOrderListingsTask() {
        progressBar.setVisibility(View.VISIBLE);
        tvErrorText.setVisibility(View.GONE);
        rvSpotOrderListings.setVisibility(View.GONE);

        VolleyHelper vHelper = VolleyHelper.getInstance(getActivity());
        HashMap<String, String> postParams = vHelper.getPostParams();

        vHelper.addVolleyRequestListeners(Api.getInstance().tradeBookSpotOrder + "200", Request.Method.GET, postParams,
                new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(String response) {
                        progressBar.setVisibility(View.GONE);
                        try {
                            JSONObject res = new JSONObject(response);
                            JSONArray responseArray = res.getJSONArray("ResponseData");
                            filterLists.clear();
                            if (responseArray.length() >= 1) {
                                JSONObject response1 = responseArray.getJSONObject(0);
                                JSONObject details = response1.getJSONObject("Details");
                                if (details.getString("appCode").equals(Api.APP_STATUS_OK)) {
                                    JSONObject tfoObject = responseArray.getJSONObject(1);
                                    JSONArray tradeBookFutureOrderArray = tfoObject.getJSONArray("TradeBook");
                                    for (int i = 0; i < tradeBookFutureOrderArray.length(); i++) {
                                        JSONObject eachFutureOrder = tradeBookFutureOrderArray.getJSONObject(i);
                                        OrderModel orderModel = new OrderModel();
                                        long miliseconds = Long.parseLong(eachFutureOrder.getString("createDate"));

                                        Date date = new Date(miliseconds);
//                                        SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy hh:mm:ss ");
                                        SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy");
                                        String formattedDate = sdf.format(date);

                                        orderModel.setOrderDate(formattedDate);
                                        orderModel.setOrderId(eachFutureOrder.getString("orderId"));
                                        orderModel.setOrderNo(eachFutureOrder.getString("orderNumber"));
                                        orderModel.setPrice(eachFutureOrder.getString("price"));
                                        orderModel.setBookBy(eachFutureOrder.getString("bookByLabel"));
                                        orderModel.setXauWeight(eachFutureOrder.getString("weight"));
                                        orderModel.setProductType(eachFutureOrder.getString("productName"));
                                        orderModel.setCustomerOrderType(eachFutureOrder.getString("customerOrderType"));
                                        orderModel.setAceBuySell(eachFutureOrder.getString("orderType"));
                                        orderModel.setOrderStatus(eachFutureOrder.getString("statusLabel"));
                                        orderModel.setGrossTotal(eachFutureOrder.getString("grossTotal"));
                                        spotOrderLists.add(orderModel);
                                    }
                                    filterLists.addAll(spotOrderLists);
                                    spotOrderAdapter.notifyDataSetChanged();
                                    rvSpotOrderListings.setVisibility(View.VISIBLE);
                                    tvErrorText.setVisibility(View.GONE);
                                    progressBar.setVisibility(View.GONE);
                                } else {
                                    rvSpotOrderListings.setVisibility(View.GONE);
                                    tvErrorText.setText(details.getString("appCodeMessage"));
                                    tvErrorText.setVisibility(View.VISIBLE);
                                }
                            }
                        } catch (JSONException e) {
                            Logger.e("spotOrderListingsTask json ex", e.getMessage());
                            rvSpotOrderListings.setVisibility(View.GONE);
                            tvErrorText.setText("No Spot Orders");
                            tvErrorText.setVisibility(View.VISIBLE);
                        }
                    }

                    @Override
                    public void onError(String errorResponse, VolleyError volleyError) {
                        String errorMsg = "Unexpected error !!! Please try again with internet access.";
                        try {
                            JSONObject errorObj = new JSONObject(errorResponse);
                            errorMsg = errorObj.getJSONObject("Details").getString("appCodeMessage");
                        } catch (Exception e) {
                            Logger.e("spotOrderListingsTask error res", e.getMessage() + " ");
                        }
                        Logger.e("error message", errorMsg);
                        tvErrorText.setText(errorMsg);
                        tvErrorText.setVisibility(View.VISIBLE);
                        progressBar.setVisibility(View.GONE);
                    }
                }, "spotOrderListingsTask");
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    public void filterMethod() {
        filterLists.clear();
        Date dateStart = null;
        Date dateEnd = null;
        SimpleDateFormat sD = new SimpleDateFormat("dd MMM yyyy");
        SimpleDateFormat sD1 = new SimpleDateFormat("yyyy-MM-dd");

        try {
            Calendar cal = Calendar.getInstance();
            if (!TextUtils.isEmpty(startDate)) {
                dateStart = sD1.parse(startDate);
                // to include the start date also check start date minus a day
                cal.setTime(dateStart);
                cal.add(Calendar.DATE, -1);
                cal.set(Calendar.HOUR_OF_DAY, cal.getActualMaximum(Calendar.HOUR_OF_DAY));
                cal.set(Calendar.MINUTE, cal.getActualMaximum(Calendar.MINUTE));
                cal.set(Calendar.SECOND, cal.getActualMaximum(Calendar.SECOND));
                dateStart = cal.getTime();
            }
            dateEnd = sD1.parse(endDate);
            // to include the start date also check start date minus a day
            cal.setTime(dateEnd);
            cal.add(Calendar.DATE, +1);
            cal.set(Calendar.HOUR_OF_DAY, cal.getActualMinimum(Calendar.HOUR_OF_DAY));
            cal.set(Calendar.MINUTE, cal.getActualMinimum(Calendar.MINUTE));
            cal.set(Calendar.SECOND, cal.getActualMinimum(Calendar.SECOND));
            dateEnd = cal.getTime();
        } catch (ParseException e) {
            Logger.e("inputDate Parse Exception: ", e.getMessage());
        }

        Logger.e("filter status", status);
        Logger.e("filter date", dateStart + " " + dateEnd);

        if (TextUtils.isEmpty(status) && dateStart == null && dateEnd == null) {
            filterLists.addAll(spotOrderLists);
            spotOrderAdapter.notifyDataSetChanged();
            filterIcon.setImageResource(R.drawable.btn_filter);
            return;
        }

        for (int i = 0; i < spotOrderLists.size(); i++) {
            Date dateToCheck = null;
            try {
                dateToCheck = sD.parse(spotOrderLists.get(i).getOrderDate());
            } catch (ParseException e) {
                Logger.e("dateToCheck Parse Exception: ", e.getMessage());
            }

            if (!TextUtils.isEmpty(status)) {
                if (!status.toLowerCase().contains(spotOrderLists.get(i).getOrderStatus().toLowerCase())) {
                    continue;
                }
            }
            if (dateToCheck != null) {
                Logger.e("Dates start: (" + i + ")", spotOrderLists.get(i).getOrderDate()
                        + " (" + dateToCheck + ")" + " :::after:::" + dateStart);

                if (dateStart != null && !dateToCheck.after(dateStart)) {
                    continue;
                }

                Logger.e("Dates end: (" + i + ")", spotOrderLists.get(i).getOrderDate()
                        + " (" + dateToCheck + ")" + " :::before:::" + dateEnd);
                if (dateEnd != null && !dateToCheck.before(dateEnd)) {
                    continue;
                }
            }
            filterLists.add(spotOrderLists.get(i));
        }
        spotOrderAdapter.notifyDataSetChanged();
        filterIcon.setImageResource(R.drawable.btn_filter_notification);


    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == REQUEST_CODE) {
                status = data.getStringExtra("status");
                endDate = data.getStringExtra("endDate");
                startDate = data.getStringExtra("startDate");
                filterMethod();
            }
        } else if (resultCode == Activity.RESULT_CANCELED) {
            if (requestCode == REQUEST_CODE) {
                status = "";
                endDate = "";
                startDate = "";
                filterIcon.setImageResource(R.drawable.btn_filter);
            }
        }
    }
}
