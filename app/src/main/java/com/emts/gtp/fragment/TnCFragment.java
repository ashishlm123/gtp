package com.emts.gtp.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.emts.gtp.Api;
import com.emts.gtp.R;
import com.emts.gtp.helper.Logger;
import com.emts.gtp.helper.NetworkUtils;
import com.emts.gtp.helper.VolleyHelper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class TnCFragment extends Fragment {
    TextView tvErrorView;
    ProgressBar progress;
    WebView webView;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_tnc, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        progress = view.findViewById(R.id.progress);
        tvErrorView = view.findViewById(R.id.tvError);
        webView = view.findViewById(R.id.wv);
        webView.setWebViewClient(new WebViewClient());

        if (NetworkUtils.isInNetwork(getActivity())) {
            getTnCTask();
        }
    }

    private void getTnCTask() {
        progress.setVisibility(View.VISIBLE);

        VolleyHelper vHelper = VolleyHelper.getInstance(getActivity());
        HashMap<String, String> postMap = vHelper.getPostParams();

        vHelper.addVolleyRequestListeners(Api.getInstance().tncApi, Request.Method.GET,
                postMap, new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(final String response) {
                        progress.setVisibility(View.GONE);
                        try {
                            final JSONObject resObj = new JSONObject(response);
                            JSONArray resArray = resObj.getJSONArray("ResponseData");
                            if (resArray.length() >= 2) {
                                JSONObject jsonObject = resArray.getJSONObject(1);
//                                tvTnC.setText(Html.fromHtml("<![CDATA[" +
//                                        jsonObject.getJSONObject("article").getString("article_body"))
//                                        + "]]>");
                                webView.loadData("<html><body>" +
                                        jsonObject.getJSONObject("article").getString("article_body") +
                                        "</body></html>", "text/html", "utf-8");
                            }

                            tvErrorView.setVisibility(View.GONE);
                        } catch (JSONException e) {
                            Logger.e("getTnCTask res ex", e.getMessage() + " ");
                        }
                    }

                    @Override
                    public void onError(String errorResponse, VolleyError volleyError) {
                        progress.setVisibility(View.GONE);
                        String errorMsg = "Unexpected error !!! Please try again with internet access.";
                        try {
                            JSONObject errorObj = new JSONObject(errorResponse);
                            errorMsg = errorObj.getJSONObject("Details").getString("appCodeMessage");
                        } catch (Exception e) {
                            Logger.e("getTnCTask error res", e.getMessage() + " ");
                        }
                        tvErrorView.setText(Html.fromHtml(errorMsg));
                        tvErrorView.setVisibility(View.VISIBLE);
                    }
                }, "getTnCTask");
    }
}
