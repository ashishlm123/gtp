package com.emts.gtp.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.emts.gtp.R;
import com.emts.gtp.adapter.FragmentPageAdapter;

public class Tradebook extends Fragment {
    TabLayout tabLayout;
    ViewPager viewPager;
    FragmentPageAdapter fragmentPageAdapter;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_tradebook, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        tabLayout = view.findViewById(R.id.tab_layout);
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        viewPager = view.findViewById(R.id.view_pager);
        tabLayout.setupWithViewPager(viewPager);

        addTabs(viewPager);
    }

    public void addTabs(ViewPager viewPager) {
        fragmentPageAdapter = new FragmentPageAdapter(getChildFragmentManager());

        Fragment spotOrderFrag = new TradebookSpotOrder();
        fragmentPageAdapter.addFrag(spotOrderFrag, "Spot Order");

        Fragment futureOrderFrag = new TradebookFutureOrder();
        fragmentPageAdapter.addFrag(futureOrderFrag, "Future Order");

        viewPager.setOffscreenPageLimit(1);
        viewPager.setAdapter(fragmentPageAdapter);


    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

}
