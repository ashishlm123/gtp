package com.emts.gtp.fragment;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.emts.gtp.Api;
import com.emts.gtp.LivePriceSocketService;
import com.emts.gtp.MainActivity;
import com.emts.gtp.R;
import com.emts.gtp.helper.AlertUtils;
import com.emts.gtp.helper.Logger;
import com.emts.gtp.helper.VolleyHelper;
import com.emts.gtp.model.Product;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Objects;


public class SpotOrderConfirmation extends Fragment {
    boolean isSell;
//    Socket mSocket;

    LinearLayout itemBuySell;
    TextView tvBuySell, tvProductPrice;
    ImageView priceBehaveIcon;

    Product product;

    //for frequent change
    Bitmap iconStable, iconIncreasing, iconDecreasing;
    int colorStable, colorIncreasing, colorDecreasing;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //ready the icons and color
        Resources res = getResources();

        iconStable = BitmapFactory.decodeResource(res, R.drawable.icon_stable);
        iconIncreasing = BitmapFactory.decodeResource(res, R.drawable.icon_increase);
        iconDecreasing = BitmapFactory.decodeResource(res, R.drawable.icon_decrease);

        colorStable = res.getColor(R.color.priceStable);
        colorIncreasing = res.getColor(R.color.priceIncrease);
        colorDecreasing = res.getColor(R.color.priceDecline);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_spot_order_confirmation, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ((MainActivity) Objects.requireNonNull(getActivity())).setToolbarTitle(false, "");
        Bundle bundle = getArguments();

        if (bundle == null) {
            return;
        }
        isSell = bundle.getBoolean("isSell", false);
        product = (Product) bundle.getSerializable("product");
        if (product == null) return;

        final String byAmount = bundle.getString(Product.P_BY_AMOUNT);
        final String byWeight = bundle.getString(Product.P_BY_WEIGHT);

        TextView tvBuySellConfirmation = view.findViewById(R.id.tv_buy_sell_confirmation);
        TextView tvProduct = view.findViewById(R.id.tv_product_type);
        tvProduct.setText(product.getpName());
        TextView titleAmountOrWeight = view.findViewById(R.id.tvTitleAmt_Wt);
        TextView tvProductValue = view.findViewById(R.id.tv_product_value);
        if (TextUtils.isEmpty(byWeight)) {
            titleAmountOrWeight.setText("Value(RM)");
            tvProductValue.setText(getString(R.string.currenty_rm) + " " + String.format("%,.3f", Double.parseDouble(byAmount)));
        } else {
            tvProductValue.setText(String.format("%,.3f", Double.parseDouble(byWeight)) + getString(R.string.gram));
            titleAmountOrWeight.setText("Xau Weight (gram)");
        }

        itemBuySell = view.findViewById(R.id.item_buy_sell);
        tvBuySell = itemBuySell.findViewById(R.id.tv_ace_buy_sell);
        priceBehaveIcon = itemBuySell.findViewById(R.id.price_behaviour_icon);
        tvProductPrice = itemBuySell.findViewById(R.id.product_price);

        TextView tvFeeType = view.findViewById(R.id.tv_text);
        TextView tvFee = view.findViewById(R.id.tv_refining_fee);

        if (isSell) {
            tvBuySellConfirmation.setText(getResources().getString(R.string.spot_order_sell_confirmation));
            tvBuySell.setText("Ace Sell");
            tvFeeType.setText("Premium Fee");
            tvFee.setText(product.getPremiumFee());
        } else {
            tvBuySellConfirmation.setText(getResources().getString(R.string.spot_order_buy_confirmation));
            tvBuySell.setText("Ace Buy");
            tvFeeType.setText("Refining Fee");
            tvFee.setText(product.getRefineFee());
        }

        Button btnConfirm, btnCancel;
        btnConfirm = view.findViewById(R.id.btn_confirm);
        btnCancel = view.findViewById(R.id.btn_cancel);
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((MainActivity) getActivity()).onBackPressed();
            }
        });
        btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (latestPriceId.equals("null")) {
                    AlertUtils.showToast(getActivity(), "Fetching information...");
                    return;
                }
                confirmBuySellTask(!isSell, product, byAmount, byWeight, latestPrice, latestPriceId);
            }
        });

//        connectToServerSocket();
    }


    private void confirmBuySellTask(final boolean isBuy, final Product product, final String byAmount,
                                    final String byWeight, final double latestPrice, String latestPriceId) {
        final ProgressDialog pDialog = AlertUtils.showProgressDialog(getActivity(), "Confirm "
                + (isBuy ? "purchase" : "sales"));

        String url = "";
        if (isBuy) {
            url = Api.getInstance().purchaseConfirm;
        } else {
            url = Api.getInstance().salesConfirm;
        }

        VolleyHelper vHelper = VolleyHelper.getInstance(getActivity());
        HashMap<String, String> postMap = vHelper.getPostParams();
        postMap.put("prod_id", product.getpId());
        postMap.put("uom", "g");
        if (!TextUtils.isEmpty(byWeight)) {
            postMap.put("total_weight", byWeight);
            postMap.put("total_amount", "0");
        } else {
//            double amount = Double.parseDouble(byAmount);
//            double weight = 0.0;
//            weight = amount / latestPrice;
//            postMap.put("total_weight", String.valueOf(weight));
            postMap.put("total_weight", "0");
            postMap.put("total_amount", byAmount);
        }
        postMap.put("bookmethod", (TextUtils.isEmpty(byWeight) ? Product.P_BY_AMOUNT : Product.P_BY_WEIGHT));
        postMap.put("price_id", latestPriceId);

        vHelper.addVolleyRequestListeners(url, Request.Method.POST,
                postMap, new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(final String response) {
                        pDialog.dismiss();
                        try {
                            final JSONObject resObj = new JSONObject(response);
                            JSONObject responseData = resObj.getJSONObject("ResponseData");
                            JSONObject details = responseData.getJSONObject("Details");
                            if (details.getString("appCode").equals(Api.APP_STATUS_OK)) {
                                Fragment fragment = new SpotOrderComplete();
                                Bundle bundle = new Bundle();
                                bundle.putBoolean("isSell", isSell);
                                bundle.putSerializable("product", product);
                                bundle.putString("total", details.getString("final_amount"));
                                bundle.putString("xau_weight", details.getString("final_weight"));
                                bundle.putDouble("latestPrice", Double.parseDouble(details.getString("final_ace_price")));
                                fragment.setArguments(bundle);
                                ((MainActivity) getActivity()).openFragment(fragment, true);
                            }
                        } catch (JSONException e) {
                            Logger.e("confirmBuySellTask res ex", e.getMessage() + " ");
                            try {
                                JSONObject errorObj = new JSONObject(response);
                                AlertUtils.showAppAlert(getActivity(), R.drawable.icon_not_accept,
                                        errorObj.getJSONObject("Details").getString("appCodeMessage"),
                                        "OK", null);
                            } catch (Exception ex) {
                                Logger.e("confirmBuySellTask ex in ex", ex.getMessage() + " ");
                            }
                        }
                    }

                    @Override
                    public void onError(String errorResponse, VolleyError volleyError) {
                        pDialog.dismiss();
                        String errorMsg = "Unexpected error !!! Please try again with internet access.";
                        try {
                            JSONObject errorObj = new JSONObject(errorResponse);
                            errorMsg = errorObj.getJSONObject("Details").getString("appCodeMessage");
                        } catch (Exception e) {
                            Logger.e("confirmBuySellTask error res", e.getMessage() + " ");
                        }
                        AlertUtils.showAppAlert(getActivity(), R.drawable.icon_not_accept, errorMsg, "OK", null);
                    }
                }, "confirmBuySellTask");
    }


//    private void connectToServerSocket() {
//        try {
//            if (mSocket != null) {
//                if (mSocket.connected()) {
//                    mSocket.off();
//                    mSocket.disconnect();
//                }
//            }
//
////            IO.Options opts = new IO.Options();
//////            opts.forceNew = true;
//////            opts.reconnection = true;
//////        opts.query = "auth_token=" + authToken;
////            opts.transports = new String[]{WebSocket.NAME};
//
//            String nodeServerHostUrl = Api.getInstance().socketUrl;
//            mSocket = IO.socket(nodeServerHostUrl);
//            mSocket.on(Socket.EVENT_CONNECT, new Emitter.Listener() {
//                @Override
//                public void call(Object... args) {
//                    Logger.e("nodeJs def connect call", "Object : ");
//
//                    if (mSocket.connected()) {
//                        Logger.e("nodeJs connected", mSocket.toString() + "");
//                    }
//                }
//            }).on(Socket.EVENT_DISCONNECT, new Emitter.Listener() {
//                @Override
//                public void call(Object... args) {
//                    Logger.e("nodeJs def. disconnect", "Object " + args[0]);
//                }
//            });
//            //listen to node before connect
//            listenToHeartBeat();
//            listenToIntIX();
//            listenToTradeOpen();
//
//            mSocket.connect();
//            Logger.e("connectToServerSocket", ">>>>>>>>>>CONNECTING>>>>>>>>>>\n" + nodeServerHostUrl);
//        } catch (URISyntaxException e) {
//            Logger.e("connectToServerSocket ex", e.getMessage() + " ");
//        }
//    }
//
//    public void listenToHeartBeat() {
//        Logger.e("listenToHeartBeat EVENT:", "goodfeed");
//        mSocket.off("goodfeed");
//        mSocket.on("goodfeed", new Emitter.Listener() {
//            @Override
//            public void call(Object... args) {
//                Logger.e("listenToHeartBeat call", args[0].toString() + "");
//            }
//        });
//    }

    double latestPrice, prevPrice;
    String latestPriceId = "null";

//    public void listenToIntIX() {
//        Logger.e("listenToIntIX EVENT:", "intlX");
//        mSocket.off("intlX");
//        mSocket.on("intlX", new Emitter.Listener() {
//            @Override
//            public void call(final Object... args) {
//                Logger.e("listenToIntIX call", args[0].toString() + "");
//                if (getActivity() != null)
//                    getActivity().runOnUiThread(new Runnable() {
//                        @Override
//                        public void run() {
//                            try {
//                                final JSONObject eachChat = new JSONObject(args[0].toString());
//                                Double liveBuyPrice = Double.parseDouble(eachChat.getString("gp_livebuyprice_gm"));
//                                Double liveSellPrice = Double.parseDouble(eachChat.getString("gp_livesellprice_gm"));
//                                latestPriceId = eachChat.getString("gp_uuid");
//
//                                if (!isSell) {
//                                    tvProductPrice.setText(getString(R.string.currency_rm) + " " + String.format("%.3f", liveBuyPrice));
//                                    prevPrice = latestPrice;
//                                    latestPrice = liveBuyPrice;
//                                } else {
//                                    tvProductPrice.setText(getString(R.string.currency_rm) + " " + String.format("%.3f", liveSellPrice));
//                                    prevPrice = latestPrice;
//                                    latestPrice = liveSellPrice;
//                                }
//
//                                if (prevPrice == latestPrice) {
//                                    itemBuySell.setBackgroundColor(colorStable);
//                                    priceBehaveIcon.setImageBitmap(iconStable);
//                                } else if (prevPrice < latestPrice) {
//                                    itemBuySell.setBackgroundColor(colorIncreasing);
//                                    priceBehaveIcon.setImageBitmap(iconIncreasing);
//                                } else {
//                                    itemBuySell.setBackgroundColor(colorDecreasing);
//                                    priceBehaveIcon.setImageBitmap(iconDecreasing);
//                                }
//                            } catch (Exception e) {
//                                Logger.e("listenToIntIX onUI thread ex", e.getMessage() + " ");
//                            }
//                        }
//                    });
//            }
//        });
//    }

//    public void listenToTradeOpen() {
//        Logger.e("listenToTradeOpen EVENT:", "tradeopen");
//        mSocket.off("tradeopen");
//        mSocket.on("tradeopen", new Emitter.Listener() {
//            @Override
//            public void call(Object... args) {
//                Logger.e("listenToTradeOpen call", args[0].toString() + "");
//            }
//        });
//    }


    @Override
    public void onDetach() {
        super.onDetach();

        VolleyHelper.getInstance(getActivity()).cancelRequest("productsListingTask");
//        mSocket.disconnect();
//        mSocket.off();
    }


    @Override
    public void onStart() {
        super.onStart();
        Objects.requireNonNull(getActivity()).startService(new Intent(getActivity(), LivePriceSocketService.class));
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
//        Objects.requireNonNull(getActivity()).stopService(new Intent(getActivity(), LivePriceSocketService.class));
        EventBus.getDefault().unregister(this);
    }

//    @Subscribe(threadMode = ThreadMode.MAIN)
//    public void onMessageEvent(LivePriceSocketService.TradeOpenMesObj event) {
//        try {
//            if (event.msg.equals("true")) {
//                tvStatusOnline.setText("Online");
//            } else {
//                tvStatusOnline.setText("Offline");
//            }
//        } catch (Exception e) {
//            Logger.e("listenToTradeOpen onUI thread ex", e.getMessage() + " ");
//        }
//    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(LivePriceSocketService.IntIXMessageObj event) {
//        Logger.e("onMessageEvent()", "initIX :" + event.msg);
        try {
            final JSONObject eachChat = new JSONObject(event.msg);
            Double liveBuyPrice = Double.parseDouble(eachChat.getString("gp_livebuyprice_gm"));
            Double liveSellPrice = Double.parseDouble(eachChat.getString("gp_livesellprice_gm"));
            latestPriceId = eachChat.getString("gp_uuid");

            if (!isSell) {
                tvProductPrice.setText(getString(R.string.currenty_rm) + " " + String.format("%.3f", liveBuyPrice));
                prevPrice = latestPrice;
                latestPrice = liveBuyPrice;
            } else {
                tvProductPrice.setText(getString(R.string.currenty_rm) + " " + String.format("%.3f", liveSellPrice));
                prevPrice = latestPrice;
                latestPrice = liveSellPrice;
            }

            if (prevPrice == latestPrice) {
                itemBuySell.setBackgroundColor(colorStable);
                priceBehaveIcon.setImageBitmap(iconStable);
            } else if (prevPrice < latestPrice) {
                itemBuySell.setBackgroundColor(colorIncreasing);
                priceBehaveIcon.setImageBitmap(iconIncreasing);
            } else {
                itemBuySell.setBackgroundColor(colorDecreasing);
                priceBehaveIcon.setImageBitmap(iconDecreasing);
            }
        } catch (Exception e) {
            Logger.e("listenToIntIX onUI thread ex", e.getMessage() + " ");
        }
    }
}
