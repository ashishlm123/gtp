package com.emts.gtp.fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.emts.gtp.Api;
import com.emts.gtp.MainActivity;
import com.emts.gtp.R;
import com.emts.gtp.helper.AlertUtils;
import com.emts.gtp.helper.Logger;
import com.emts.gtp.helper.NetworkUtils;
import com.emts.gtp.helper.VolleyHelper;
import com.emts.gtp.model.Product;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Objects;


public class FutureOrderConfirmation extends Fragment {
    boolean isBid;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_future_order_confirmation, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ((MainActivity) Objects.requireNonNull(getActivity())).setToolbarTitle(false, "");

        Bundle bundle = getArguments();
        if (bundle == null) {
            return;
        }

        //isBid -> Sell
        //but sell to ACE means user BUY confirmation
        // Hence, isBid --> Buy
        isBid = bundle.getBoolean("isBid", false);
        final Product product = (Product) bundle.getSerializable("product");
        if (product == null) {
            AlertUtils.showToast(getActivity(), "Product cannot be null");
            return;
        }
        final String byWeight = bundle.getString(Product.P_BY_WEIGHT);
        final String byAmount = bundle.getString(Product.P_BY_AMOUNT);
        final String price;
        if (isBid) {
            price = bundle.getString("aceSellPrice");
        } else {
            price = bundle.getString("aceBuyPrice");
        }

        TextView tvFinalPrice = view.findViewById(R.id.tv_final_price);
        TextView tvMatchingPriceType = view.findViewById(R.id.tv_matching_price_type);
        TextView tvProductType = view.findViewById(R.id.tv_product_type);
        tvProductType.setText(product.getpName());
        TextView tvXauWeight = view.findViewById(R.id.tv_xau_weight);
        tvXauWeight.setText(bundle.getString("xau_weight"));
//        tvXauWeight.setText(String.format("%,.3f", Double.parseDouble(bundle.getString("xau_weight"))) + "g");

        TextView tvFeeType = view.findViewById(R.id.tv_fee_type);
        TextView tvFee = view.findViewById(R.id.tv_fee);
        TextView tvTotalEstVal = view.findViewById(R.id.tv_total_est_val);
        tvTotalEstVal.setText(getString(R.string.currenty_rm) + " " + bundle.getString("total_est_value"));
//        tvTotalEstVal.setText(getString(R.string.currenty_rm) + " "
//                + String.format("%,.2f", Double.parseDouble(bundle.getString("total_est_value"))));

        TextView tvAceBuyMatchingPrice = view.findViewById(R.id.tv_ace_buy_matching_price);
        TextView tvFinalBuyPrice = view.findViewById(R.id.tv_final_buy_price);

        if (isBid) {
            tvFeeType.setText("Premium Fee(RM/g):");
            tvFinalPrice.setText("Final Sell Price(RM/g):");
            tvMatchingPriceType.setText("Ace Sell Matching Price(RM/g):");
            tvFee.setText(bundle.getString("premium_fee"));
//            tvAceBuyMatchingPrice.setText(String.format("%.2f", Double.parseDouble(bundle.getString("matching_buy_price"))));
//            tvFinalBuyPrice.setText(String.format("%.2f", Double.parseDouble(bundle.getString("final_buy_price"))));
            tvAceBuyMatchingPrice.setText(bundle.getString("matching_bid_price"));
            tvFinalBuyPrice.setText(bundle.getString("final_bid_price"));
        } else {
            tvFeeType.setText("Refining Fee(RM/g):");
            tvFinalPrice.setText("Final Buy Price(RM/g):");
            tvMatchingPriceType.setText("Ace Buy Matching Price(RM/g):");
            tvFee.setText(bundle.getString("refine_fee"));
//            tvAceBuyMatchingPrice.setText(String.format("%.2f", Double.parseDouble(bundle.getString("matching_ask_price"))));
//            tvFinalBuyPrice.setText(String.format("%.2f", Double.parseDouble(bundle.getString("final_ask_price"))));
            tvAceBuyMatchingPrice.setText(bundle.getString("matching_ask_price"));
            tvFinalBuyPrice.setText(bundle.getString("final_ask_price"));
        }

        Button btnConfirm, btnCancel;
        btnConfirm = view.findViewById(R.id.btn_confirm);
        btnCancel = view.findViewById(R.id.btn_cancel);
        btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (NetworkUtils.isInNetwork(getActivity())) {
                    confirmFutureOrder(isBid, product, byAmount, byWeight, price);
                } else {
                    AlertUtils.showSnack(getActivity(), view, getString(R.string.error_no_internet));
                }
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((MainActivity)getActivity()).onBackPressed();
            }
        });
    }

    private void confirmFutureOrder(final boolean isBid, final Product product, final String byAmount,
                                    final String byWeight, final String price) {
        final ProgressDialog pDialog = AlertUtils.showProgressDialog(getActivity(), "Confirm order...");

        VolleyHelper vHelper = VolleyHelper.getInstance(getActivity());
        HashMap<String, String> postMap = vHelper.getPostParams();
        postMap.put("prod_id", product.getpId());
        postMap.put("uom", "g");
        if (!TextUtils.isEmpty(byWeight)) {
            postMap.put("total_weight", byWeight);
            postMap.put("total_amount", "0");
        } else {
            postMap.put("total_amount", byAmount);
            postMap.put("total_weight", "0");
        }
        postMap.put("bookmethod", (TextUtils.isEmpty(byWeight) ? Product.P_BY_AMOUNT : Product.P_BY_WEIGHT));

        String url = "";
        if (isBid) {
            url = Api.getInstance().bidConfirm;
            postMap.put("bid_price", price);
        } else {
            url = Api.getInstance().askConfirm;
            postMap.put("ask_price", price);
        }

        vHelper.addVolleyRequestListeners(url, Request.Method.POST,
                postMap, new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(final String response) {
                        pDialog.dismiss();
                        try {
                            final JSONObject resObj = new JSONObject(response);
                            JSONObject resDetail = resObj.getJSONObject("ResponseData").getJSONObject("Details");
                            if (resDetail.getInt("appCode") == 1000) {
                                Fragment fragment = new FutureOrderComplete();
                                Bundle bundle = getArguments();
                                fragment.setArguments(bundle);
                                ((MainActivity) getActivity()).openFragment(fragment, false);

                                SharedPreferences actPrefs = ((MainActivity) getActivity()).getPreferences(Context.MODE_PRIVATE);
                                SharedPreferences.Editor editor = actPrefs.edit();
                                editor.putInt(FutureOrder.PREF_PORDUCT_POS, 0);
                                editor.putString(FutureOrder.PREF_VALUE_RM, "");
                                editor.putString(FutureOrder.PREF_XAU_WEIGHT, "");
                                editor.putString(FutureOrder.PREF_ACE_BUY, "");
                                editor.putString(FutureOrder.PREF_ACE_SELL, "");
                                editor.commit();
                            }
                        } catch (JSONException e) {
                            Logger.e("confirmFutureOrder res ex", e.getMessage() + " ");
                            try {
                                JSONObject errorObj = new JSONObject(response);
                                AlertUtils.showAppAlert(getActivity(), R.drawable.icon_not_accept,
                                        errorObj.getJSONObject("Details").getString("appCodeMessage"),
                                        "OK", null);
                            } catch (Exception ex) {
                                Logger.e("confirmFutureOrder ex in ex", ex.getMessage() + " ");
                            }
                        }
                    }

                    @Override
                    public void onError(String errorResponse, VolleyError volleyError) {
                        pDialog.dismiss();
                        String errorMsg = "Unexpected error !!! Please try again with internet access.";
                        try {
                            JSONObject errorObj = new JSONObject(errorResponse);
                            errorMsg = errorObj.getJSONObject("Details").getString("appCodeMessage");
                        } catch (Exception e) {
                            Logger.e("confirmFutureOrder error res", e.getMessage() + " ");
                        }
                        AlertUtils.showAppAlert(getActivity(), R.drawable.icon_not_accept, errorMsg, "OK", null);
                    }
                }, "confirmFutureOrder");
    }
}