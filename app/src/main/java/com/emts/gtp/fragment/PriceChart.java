package com.emts.gtp.fragment;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.emts.gtp.Api;
import com.emts.gtp.R;
import com.emts.gtp.helper.Logger;
import com.emts.gtp.helper.NetworkUtils;
import com.emts.gtp.helper.VolleyHelper;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.Description;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;
//import java.util.TimeZone;

public class PriceChart extends Fragment {
//    public static final String TIME_ZONE = "UTC";
    ////    TimeZone timeZone = TimeZone.getDefault();
//    TimeZone timeZone = TimeZone.getTimeZone(TIME_ZONE);

    TextView tvProductPrice, tvInrDcrRange;
    ImageView iconBehaviour;

    LineChart lineChart;
    List<Entry> entries = new ArrayList<>();
    //for time store
    ArrayList<Long> times = new ArrayList<>();
    //work around for time display
    ArrayList<String> showTime = new ArrayList<>();

    Bitmap iconStable, iconIncreasing, iconDecreasing;
    int colorGraphFill;

    View holderAvgPrice;
    Spinner spHour, spDay, spWeek, spMonth;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_price_chart, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        holderAvgPrice = view.findViewById(R.id.holderAvgPrice);
        holderAvgPrice.setVisibility(View.INVISIBLE);
        tvProductPrice = view.findViewById(R.id.product_price);
        tvInrDcrRange = view.findViewById(R.id.tv_product_increase_decrease_range);
        iconBehaviour = view.findViewById(R.id.product_icon_behaviour);

        lineChart = view.findViewById(R.id.chart);
        lineChart.setNoDataText("Loading Live Price ...");
        lineChart.setNoDataTextColor(getResources().getColor(R.color.graphColor));
        lineChart.setNoDataTextTypeface(Typeface.DEFAULT_BOLD);
        lineChart.setDrawGridBackground(true);
        lineChart.setDrawBorders(true);
//        lineChart.setDrawValues();
        lineChart.setMaxVisibleValueCount(6);
        lineChart.setVisibleXRangeMaximum(6);
        lineChart.setAutoScaleMinMaxEnabled(true);
        lineChart.setKeepPositionOnRotation(true);
        //chart description
        lineChart.setContentDescription("");
        Description description = new Description();
        description.setText("");
        lineChart.setDescription(description);
        //axis
        XAxis xAxis = lineChart.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setLabelCount(6);
        xAxis.setValueFormatter(new IAxisValueFormatter() {
            @Override
            public String getFormattedValue(float value, AxisBase axis) {
                if (value == -1) return "";
                return String.format(java.util.Locale.US, "%.2f", value);
            }
        });
        xAxis.setGranularityEnabled(true);

        YAxis leftAxis = lineChart.getAxisLeft();
        leftAxis.setPosition(YAxis.YAxisLabelPosition.INSIDE_CHART);
        YAxis rightAxis = lineChart.getAxisRight();
        rightAxis.setDrawLabels(false);

        //legend
        Legend legend = lineChart.getLegend();
        legend.setEnabled(false);
        lineChart.invalidate();

        ArrayList<String> hourLists, dayLists, weekLists, monthLists;

        spHour = view.findViewById(R.id.sp_hour);
        hourLists = new ArrayList<>();
        hourLists.add("xx Hour");
        hourLists.add("1 Hour");
        hourLists.add("2 Hour");
        hourLists.add("5 Hour");
        hourLists.add("10 Hour");
        ArrayAdapter<String> hourAdapter = new ArrayAdapter<>(Objects.requireNonNull(getActivity()),
                R.layout.item_chart_spinner_textview, hourLists);
        hourAdapter.setDropDownViewResource(R.layout.layout_textview);
        spHour.setAdapter(hourAdapter);
        spHour.post(new Runnable() {
            @Override
            public void run() {
                spHour.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                        selectFilter(adapterView, i);
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> adapterView) {

                    }
                });
            }
        });

        spDay = view.findViewById(R.id.sp_day);
        dayLists = new ArrayList<>();
        dayLists.add("xx Day");
        dayLists.add("1 Day");
        dayLists.add("2 Day");
        dayLists.add("5 Day");
        dayLists.add("10 Day");
        ArrayAdapter<String> dayAdapter = new ArrayAdapter<>(Objects.requireNonNull(getActivity()),
                R.layout.item_chart_spinner_textview, dayLists);
        dayAdapter.setDropDownViewResource(R.layout.layout_textview);
        spDay.setAdapter(dayAdapter);
        spDay.post(new Runnable() {
            @Override
            public void run() {
                spDay.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                        selectFilter(adapterView, i);
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> adapterView) {

                    }
                });
            }
        });

        spWeek = view.findViewById(R.id.sp_week);
        weekLists = new ArrayList<>();
        weekLists.add("xx Week");
        weekLists.add("1 Week");
        weekLists.add("2 Week");
        weekLists.add("5 Week");
        weekLists.add("10 Week");
        ArrayAdapter<String> weekAdapter = new ArrayAdapter<>(Objects.requireNonNull(getActivity()),
                R.layout.item_chart_spinner_textview, weekLists);
        weekAdapter.setDropDownViewResource(R.layout.layout_textview);
        spWeek.setAdapter(weekAdapter);
        spWeek.post(new Runnable() {
            @Override
            public void run() {
                spWeek.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                        selectFilter(adapterView, i);
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> adapterView) {

                    }
                });
            }
        });

        spMonth = view.findViewById(R.id.sp_month);
        monthLists = new ArrayList<>();
        monthLists.add("xx Month");
        monthLists.add("1 Month");
        monthLists.add("2 Month");
        monthLists.add("5 Month");
        monthLists.add("10 Month");
        ArrayAdapter<String> monthAdapter = new ArrayAdapter<>(Objects.requireNonNull(getActivity()),
                R.layout.item_chart_spinner_textview, monthLists);
        monthAdapter.setDropDownViewResource(R.layout.layout_textview);
        spMonth.setAdapter(monthAdapter);
        spMonth.post(new Runnable() {
            @Override
            public void run() {
                spMonth.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                        selectFilter(adapterView, i);
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> adapterView) {

                    }
                });
            }
        });


        //ready the icons and color
        Resources res = getResources();

        iconStable = BitmapFactory.decodeResource(res, R.drawable.icon_stable);
        iconIncreasing = BitmapFactory.decodeResource(res, R.drawable.icon_increase);
        iconDecreasing = BitmapFactory.decodeResource(res, R.drawable.icon_decrease);

        colorGraphFill = res.getColor(R.color.graphColor);

        //load data of 1 day
        if (NetworkUtils.isInNetwork(getActivity())) {
            Spinner sp = new Spinner(getActivity());
            sp.setId(R.id.sp_day);
            priceChartFilterTask(sp, 1); //self made spinner to mock spDay and pos == 1 for 1 day data
        }

        //to show latest hour price average at the top
        if (NetworkUtils.isInNetwork(getActivity())) {
            Spinner sp = new Spinner(getActivity());
            sp.setId(R.id.sp_hour);
            priceChartFilterTask(sp, -1); //self made spinner to mock spHour and pos == -1 for latest hour average
        }
    }

    private void selectFilter(View view, int position) {
        if (position == 0) return;
        priceChartFilterTask(view, position);

        if (view.getId() == R.id.sp_hour) {
            spDay.setSelection(0);
            spWeek.setSelection(0);
            spMonth.setSelection(0);
        } else if (view.getId() == R.id.sp_day) {
            spHour.setSelection(0);
            spWeek.setSelection(0);
            spMonth.setSelection(0);
        } else if (view.getId() == R.id.sp_week) {
            spDay.setSelection(0);
            spHour.setSelection(0);
            spMonth.setSelection(0);
        } else if (view.getId() == R.id.sp_month) {
            spDay.setSelection(0);
            spWeek.setSelection(0);
            spHour.setSelection(0);
        }
    }

    private void priceChartFilterTask(final View spinner, int position) {
        final boolean isLatestHourlyPrice = position == -1;
        if (!isLatestHourlyPrice) {
            lineChart.invalidate();
            lineChart.clear();
            lineChart.setNoDataText("Loading past data");
            lineChart.setNoDataTextColor(getResources().getColor(R.color.graphColor));
        }

        //1,2,5,10
        int spValue = 0;
        if (position == 1 || isLatestHourlyPrice) {
            spValue = 1;
        } else if (position == 2) {
            spValue = 2;
        } else if (position == 3) {
            spValue = 5;
        } else if (position == 4) {
            spValue = 10;
        }

        String url = "";
        if (spinner.getId() == R.id.sp_hour) {
            url = Api.getInstance().chartByHour + String.valueOf(spValue);
        } else if (spinner.getId() == R.id.sp_day) {
            url = Api.getInstance().chartByDay + String.valueOf(spValue);
        } else if (spinner.getId() == R.id.sp_week) {
            url = Api.getInstance().chartByWeek + String.valueOf(spValue);
        } else if (spinner.getId() == R.id.sp_month) {
            url = Api.getInstance().chartByMonth + String.valueOf(spValue);
        }

        VolleyHelper vHelper = VolleyHelper.getInstance(getActivity());
        HashMap<String, String> postMap = vHelper.getPostParams();

        vHelper.addVolleyRequestListeners(url, Request.Method.GET,
                postMap, new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(final String response) {
                        try {
                            final JSONArray priceResponse = new JSONArray(response);

                            //if position == -1 i.e:
                            if (isLatestHourlyPrice) {
                                if (priceResponse.length() < 1) {
                                    return;
                                }
                                double latestPrice = Double.parseDouble(priceResponse.getJSONObject(0).getString("avgprice"));
                                tvProductPrice.setText(String.format("%.4f", latestPrice) + " MYR/g");

                                double prevSellPrice = latestPrice;
                                if (priceResponse.length() > 1) {
                                    prevSellPrice = Double.parseDouble(priceResponse.getJSONObject(1).getString("avgprice"));
                                }
                                double difference = Math.abs(prevSellPrice - latestPrice);
                                double changePercentage = (difference / prevSellPrice) * 100;
                                if (prevSellPrice == latestPrice) {
                                    iconBehaviour.setImageBitmap(iconStable);
                                    tvInrDcrRange.setText("0.0000-0.0000%");
                                } else if (prevSellPrice < latestPrice) {
                                    iconBehaviour.setImageBitmap(iconIncreasing);
                                    tvInrDcrRange.setText(String.format("%.4f", difference) + "-"
                                            + String.format("%.4f", changePercentage) + "%");
                                } else {
                                    iconBehaviour.setImageBitmap(iconDecreasing);
                                    tvInrDcrRange.setText("-" + String.format("%.4f", difference) + "-"
                                            + String.format("%.4f", changePercentage) + "%");
                                }
                                holderAvgPrice.setVisibility(View.VISIBLE);
                                return;
                            }

                            entries.clear();
                            //    "dtime": "2018-07-11T01:00:00.000Z"
//                            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
                            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//                            format.setTimeZone(timeZone);
                            final Calendar calendar = Calendar.getInstance();
//                            calendar.setTimeZone(timeZone);
                            showTime.clear();
                            times.clear();
                            for (int i = 0; i < priceResponse.length(); i++) {
                                try {
                                    JSONObject eachPrice = priceResponse.getJSONObject(i);
                                    Double liveBuyPrice = Double.parseDouble(eachPrice.getString("avgprice"));

//                                calendar.setTime(format.parse(eachPrice.getString("dtime")));
//                                Entry entry = new Entry(calendar.getTimeInMillis(), liveBuyPrice.floatValue());

                                    String dTime = eachPrice.getString("dtime");
                                    String timeOnly = dTime.substring(11, 16);
                                    showTime.add(timeOnly);
                                    Date date = format.parse(dTime);
                                    times.add(date.getTime());
                                    Entry entry = new Entry((float) ((entries.size() + 1)), liveBuyPrice.floatValue());

                                    entries.add(entry);
                                } catch (Exception e) {
                                    Logger.e("PRICE CHART EXXXX ##########", e.getMessage());
                                }
                            }

                            if (entries.size() <= 0) {
                                lineChart.invalidate();
                                lineChart.clear();
                                lineChart.setNoDataText("No Data Available");
                                lineChart.setNoDataTextColor(Color.RED);

                                return;
                            }

                            lineChart.removeAllViews();
                            List<Entry> entriesToShow = new ArrayList<>(entries);

                            LineDataSet dataSet = new LineDataSet(entriesToShow, "");
                            dataSet.setColor(Color.GRAY);
                            dataSet.setDrawFilled(true);
                            dataSet.setFillColor(colorGraphFill);
                            dataSet.setFillAlpha(120);
                            dataSet.setDrawCircles(false);
                            LineData lineData = new LineData(dataSet);
                            lineChart.setData(lineData);

                            if ((spinner.getId() == R.id.sp_day &&
                                    ((Spinner) spinner).getSelectedItemPosition() < 3)
                                    || spinner.getId() == R.id.sp_hour) {
                                lineChart.getXAxis().setValueFormatter(new IAxisValueFormatter() {
                                    @Override
                                    public String getFormattedValue(float value, AxisBase axis) {
//                                        Calendar calendar1 = Calendar.getInstance();
////                                        calendar1.setTimeZone(timeZone);
//                                        Date date = new Date();
////                                        date.setTime((long) value);
//                                        date.setTime(times.get((int) value - 1));
//                                        Logger.e("date set --> ", date.toString() + " AND TIME :" + date.getTime());
//                                        calendar1.setTime(date);
//
//                                        Logger.e("date get --> ", calendar1.getTime().toString());
//
//                                        Logger.e("F Time : " + value,
//                                                "saved :" + times.get((int) value - 1) + " :::: GOT :" + calendar1.getTime().getTime());
//                                        Logger.e("Calendar FUCK:", calendar1.toString());
//                                        Logger.e("hour", calendar1.get(Calendar.HOUR_OF_DAY) + " ");
//                                        Logger.e("minutes", calendar1.get(Calendar.MINUTE) + " ");
//                                        String toShow = String.format("%02d", calendar1.get(Calendar.HOUR_OF_DAY)) +
//                                                ":" + String.format("%02d", calendar1.get(Calendar.MINUTE)) +
//                                                (calendar1.get(Calendar.AM_PM) == Calendar.AM ? " AM" : " PM");
//                                        Logger.e("toShow: ", toShow);

                                        //workaround
                                        try {
                                            String time = showTime.get((int) value - 1);
                                            int hour = Integer.parseInt(time.substring(0, 2));
                                            String am_pm = "AM";
                                            if (hour >= 12) {
                                                am_pm = "PM";
                                                hour = hour - 12;
                                            }
                                            String toShow = String.format("%02d", hour) + time.substring(2) + " " + am_pm;

                                            return toShow;
                                        } catch (Exception e) {
                                            return "";
                                        }
                                    }
                                });
                            } else if ((spinner.getId() == R.id.sp_day &&
                                    ((Spinner) spinner).getSelectedItemPosition() > 2)
                                    || spinner.getId() == R.id.sp_week || spinner.getId() == R.id.sp_month) {
                                lineChart.getXAxis().setValueFormatter(new IAxisValueFormatter() {
                                    @Override
                                    public String getFormattedValue(float value, AxisBase axis) {
                                        try {
                                            Calendar calendar1 = Calendar.getInstance();
                                            Date date = new Date();
                                            date.setTime(times.get((int) value - 1));
                                            calendar1.setTime(date);
                                            SimpleDateFormat sdf32 = new SimpleDateFormat("MMM");
                                            return sdf32.format(calendar1.getTime())
                                                    + " " + calendar1.get(Calendar.DAY_OF_MONTH);
                                        } catch (Exception e) {
                                            return "";
                                        }
                                    }
                                });
                            } else {
                                lineChart.getXAxis().setValueFormatter(new IAxisValueFormatter() {
                                    @Override
                                    public String getFormattedValue(float value, AxisBase axis) {
//                                        Calendar calendar1 = Calendar.getInstance();
//                                        Date date = new Date();
//                                        date.setTime(times.get((int) value - 1));
//                                        calendar1.setTime(date);
//                                        return "00:00:" + String.format("%02d", calendar.get(Calendar.MINUTE));
                                        return "";
                                    }
                                });
                            }
                            lineChart.invalidate();
                        } catch (Exception e) {
                            Logger.e("priceChartFilterTask res ex", e.getMessage() + " ");
                        }
                    }

                    @Override
                    public void onError(String errorResponse, VolleyError volleyError) {
                        String errorMsg = "Unexpected error !!! Please try again with internet access.";
                        try {
                            JSONObject errorObj = new JSONObject(errorResponse);
                            errorMsg = errorObj.getJSONObject("Details").getString("appCodeMessage");
                        } catch (Exception e) {
                            Logger.e("priceChartFilterTask error res", e.getMessage() + " ");
                        }
                        lineChart.setNoDataText(errorMsg);
                        lineChart.setNoDataTextColor(getResources().getColor(R.color.red));
                    }
                }, "priceChartFilterTask");

    }

    @Override
    public void onDetach() {
        super.onDetach();

        VolleyHelper.getInstance(getActivity()).cancelRequest("productsListingTask");
    }
}
