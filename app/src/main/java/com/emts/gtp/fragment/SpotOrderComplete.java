package com.emts.gtp.fragment;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.emts.gtp.MainActivity;
import com.emts.gtp.R;
import com.emts.gtp.helper.AlertUtils;
import com.emts.gtp.model.Product;

import java.util.Objects;


public class SpotOrderComplete extends Fragment {
    boolean isSell;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_spot_order_complete, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ((MainActivity) Objects.requireNonNull(getActivity())).setToolbarTitle(false, "");
        Bundle bundle = getArguments();

        if (bundle == null) {
            return;
        }
        isSell = bundle.getBoolean("isSell", false);
        Product product = (Product) bundle.getSerializable("product");
        if (product == null) {
            AlertUtils.showToast(getActivity(), "Product cannot be null");
            return;
        }

        double latestPrice = bundle.getDouble("latestPrice");

        TextView tvConfirmation = view.findViewById(R.id.tv_spot_order_buy_sell_confirmation);
        TextView tvProductType = view.findViewById(R.id.tv_product_type);
        tvProductType.setText(product.getpName());
        TextView tvComplete = view.findViewById(R.id.tv_buy_sell_complete);

        TextView tvCompletePrice = view.findViewById(R.id.tv_buy_sell_complete_price);
        tvCompletePrice.setText(String.format("%,.3f", latestPrice));
        TextView tvXauWeight = view.findViewById(R.id.tv_xau_weight);
        tvXauWeight.setText(String.format("%,.3f", Double.parseDouble(bundle.getString("xau_weight"))));

        TextView tvTotalEstVal = view.findViewById(R.id.tv_total_est_val);
        tvTotalEstVal.setText(getString(R.string.currenty_rm) + " "
                + String.format("%,.2f", Double.parseDouble(bundle.getString("total"))));

        if (isSell) {
            tvConfirmation.setText(getResources().getString(R.string.spot_order_sell_confirmation));
            tvComplete.setText(getResources().getString(R.string.final_ace_sell_complete));
        } else {
            tvConfirmation.setText(getResources().getString(R.string.spot_order_buy_confirmation));
            tvComplete.setText(getResources().getString(R.string.final_ace_buy_complete));
        }

        Button btnOk = view.findViewById(R.id.btn_ok);
        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Return to wireframe 3.0
                Intent intent = new Intent(getActivity(), MainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        });


    }
}
