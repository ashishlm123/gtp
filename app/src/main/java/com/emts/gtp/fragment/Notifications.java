package com.emts.gtp.fragment;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.emts.gtp.Api;
import com.emts.gtp.R;
import com.emts.gtp.adapter.NotificationAdapter;
import com.emts.gtp.helper.Logger;
import com.emts.gtp.helper.NetworkUtils;
import com.emts.gtp.helper.VolleyHelper;
import com.emts.gtp.model.NotificationModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;


public class Notifications extends Fragment {
    RecyclerView rvNotificationListings;
    ProgressBar progressBar, infiniteProgressBar;
    TextView errorText;
    NotificationAdapter notificationAdapter;
    ArrayList<NotificationModel> notificationLists;
    SwipeRefreshLayout swipeRefreshLayout;

    int limit = 10;
    int offset = 0;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_notifications, container, false);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        rvNotificationListings = view.findViewById(R.id.recycler_view);
        progressBar = view.findViewById(R.id.progress_bar);
        infiniteProgressBar = view.findViewById(R.id.infinite_progress_bar);
        errorText = view.findViewById(R.id.error_text);
        swipeRefreshLayout = view.findViewById(R.id.swipeRefresh);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        rvNotificationListings.setLayoutManager(linearLayoutManager);

        notificationLists = new ArrayList<>();
        notificationAdapter = new NotificationAdapter(getActivity(), notificationLists);
        rvNotificationListings.setAdapter(notificationAdapter);

        if (NetworkUtils.isInNetwork(getActivity())) {
            notificationListingTask();
        } else {
            errorText.setText(getResources().getString(R.string.error_no_internet));
            errorText.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.GONE);
            rvNotificationListings.setVisibility(View.GONE);
        }

        swipeRefreshLayout.setEnabled(true);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (NetworkUtils.isInNetwork(getActivity())) {
                    notificationListingTask();
                } else {
                    swipeRefreshLayout.setRefreshing(false);
                    errorText.setText(getResources().getString(R.string.error_no_internet));
                    errorText.setVisibility(View.VISIBLE);
                    progressBar.setVisibility(View.GONE);
                    rvNotificationListings.setVisibility(View.GONE);
                }
            }
        });
    }

    private void notificationListingTask() {
        progressBar.setVisibility(View.VISIBLE);
        VolleyHelper volleyHelper = VolleyHelper.getInstance(getActivity());
        HashMap<String, String> postParams = volleyHelper.getPostParams();

        volleyHelper.addVolleyRequestListeners(Api.getInstance().getNotifications, Request.Method.GET, postParams,
                new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(String response) {
                        swipeRefreshLayout.setRefreshing(false);
                        try {
                            JSONObject res = new JSONObject(response);
                            JSONArray responseData = res.getJSONArray("ResponseData");
                            if (responseData.length() >= 1) {
                                JSONObject det = responseData.getJSONObject(0);
                                JSONObject details = det.getJSONObject("Details");
                                if (details.getString("appCode").equals(Api.APP_STATUS_OK)) {
                                    notificationLists.clear();
                                    JSONObject notiObj = responseData.getJSONObject(1);
                                    JSONArray notificationArray = notiObj.getJSONArray("announcements");
                                    if (notificationArray.length() > 0) {
                                        for (int i = 0; i < notificationArray.length(); i++) {
                                            JSONObject eachNotifications = notificationArray.getJSONObject(i);
                                            NotificationModel notificationModel = new NotificationModel();
                                            notificationModel.setNotiTitle(eachNotifications.getString("entry_title"));
                                            notificationModel.setNotiBody(eachNotifications.getString("entry_content"));
                                            notificationModel.setNotiDate(eachNotifications.getString("entry_date"));
                                            notificationLists.add(notificationModel);
                                        }
                                        notificationAdapter.notifyDataSetChanged();
                                        rvNotificationListings.setVisibility(View.VISIBLE);
                                        errorText.setVisibility(View.GONE);
                                        progressBar.setVisibility(View.GONE);
                                    } else {
                                        Logger.e("here", "here");
                                        progressBar.setVisibility(View.GONE);
                                        rvNotificationListings.setVisibility(View.GONE);
                                        errorText.setText("No Notifications..");
                                        errorText.setVisibility(View.VISIBLE);
                                    }

                                } else {
                                    errorText.setText(details.getString("appCodeMessage"));
                                    errorText.setVisibility(View.VISIBLE);
                                    progressBar.setVisibility(View.GONE);
                                    rvNotificationListings.setVisibility(View.GONE);
                                }
                            }

                        } catch (JSONException ex) {
                            Logger.e("notificationListingTask json ex", ex.getMessage());
                        }
                    }

                    @Override
                    public void onError(String errorResponse, VolleyError volleyError) {
                        String errorMsg = "Unexpected error !!! Please try again with internet access.";
                        try {
                            JSONObject errorObj = new JSONObject(errorResponse);
                            errorMsg = errorObj.getJSONObject("Details").getString("appCodeMessage");
                        } catch (Exception e) {
                            Logger.e("notificationListingTask error res", e.getMessage() + " ");
                        }
                        Logger.e("error message", errorMsg);
                        errorText.setText(errorMsg);
                        errorText.setVisibility(View.VISIBLE);
                    }
                }, "notificationListingTask");
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }
}
