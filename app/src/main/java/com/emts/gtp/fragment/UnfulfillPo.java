package com.emts.gtp.fragment;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.emts.gtp.Api;
import com.emts.gtp.R;
import com.emts.gtp.activity.UnfulfillPOFilterActivity;
import com.emts.gtp.adapter.UnfulfilledPoAdapter;
import com.emts.gtp.helper.AlertUtils;
import com.emts.gtp.helper.Logger;
import com.emts.gtp.helper.NetworkUtils;
import com.emts.gtp.helper.VolleyHelper;
import com.emts.gtp.model.Product;
import com.emts.gtp.model.UnfulfilledPOModel;
import com.google.common.collect.ComparisonChain;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;

public class UnfulfillPo extends Fragment {
    RecyclerView rvUnfulfilledPoListings;
    ProgressBar progressBar, infiniteProgressBar;
    TextView tvErrorText;
    ArrayList<UnfulfilledPOModel> unfulfilledPOLists, searchLists;
    UnfulfilledPoAdapter unfulfilledPoAdapter;
    ArrayList<String> pCodeList = new ArrayList<>();
    ArrayList<Product> productList = new ArrayList<>();
    ImageView filterIcon;

    private static final int REQUEST_FILTER_CODE = 32;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_unfulfill_po, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        progressBar = view.findViewById(R.id.progress_bar);
        infiniteProgressBar = view.findViewById(R.id.infinite_progress_bar);
        tvErrorText = view.findViewById(R.id.error_text);
        EditText etSearchPo = view.findViewById(R.id.et_search_unfulfilled_po);

        rvUnfulfilledPoListings = view.findViewById(R.id.recycler_view);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        rvUnfulfilledPoListings.setLayoutManager(linearLayoutManager);

        unfulfilledPOLists = new ArrayList<>();
        searchLists = new ArrayList<>();
        unfulfilledPoAdapter = new UnfulfilledPoAdapter(getActivity(), searchLists);
        rvUnfulfilledPoListings.setAdapter(unfulfilledPoAdapter);

        if (NetworkUtils.isInNetwork(getActivity())) {
            unfulfillPOListingsTask();
        } else {
            progressBar.setVisibility(View.GONE);
            rvUnfulfilledPoListings.setVisibility(View.GONE);
            tvErrorText.setText(getResources().getString(R.string.error_no_internet));
            tvErrorText.setVisibility(View.VISIBLE);
        }

        etSearchPo.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.length() > 0) {
                    searchLists.clear();
                    for (int j = 0; j < unfulfilledPOLists.size(); j++) {
                        if (unfulfilledPOLists.get(j).getGtpBookingNo().contains(charSequence)) {
                            searchLists.add(unfulfilledPOLists.get(j));
                        }
                    }
                    unfulfilledPoAdapter.notifyDataSetChanged();
                } else {
                    searchLists.clear();
                    searchLists.addAll(unfulfilledPOLists);
                    unfulfilledPoAdapter.notifyDataSetChanged();
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        filterIcon = view.findViewById(R.id.ico_filter);
        filterIcon.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onClick(View view) {
                if (unfulfilledPOLists.size() > 0) {
                    Intent intent = new Intent(getActivity(), UnfulfillPOFilterActivity.class);
                    intent.putExtra("productList", productList);
                    intent.putExtra("jList", jList);
                    intent.putExtra("bList", bList);
                    intent.putExtra("startDate", startDate);
                    intent.putExtra("endDate", endDate);
                    intent.putExtra("agingPos", agingPos);
                    intent.putExtra("shortPo", shortPo);
                    intent.putExtra("shortUnfulfill", shortUnfulfill);
                    intent.putExtra("short1_2", short1_2);

                    startActivityForResult(intent, REQUEST_FILTER_CODE);
                } else {
                    AlertUtils.showSnack(getActivity(), view, "Data not available to filter");
                }
            }
        });
    }

    private void unfulfillPOListingsTask() {
        progressBar.setVisibility(View.VISIBLE);
        tvErrorText.setVisibility(View.GONE);
        rvUnfulfilledPoListings.setVisibility(View.GONE);

        VolleyHelper vHelper = VolleyHelper.getInstance(getActivity());
        HashMap<String, String> postParams = vHelper.getPostParams();

        vHelper.addVolleyRequestListeners(Api.getInstance().unfulfillPO, Request.Method.GET, postParams,
                new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(String response) {
                        progressBar.setVisibility(View.GONE);
                        productList.clear();
                        Logger.printLongLog("wholeresponse", response);
                        try {
                            JSONObject res = new JSONObject(response);
                            JSONArray responseArray = res.getJSONArray("ResponseData");
                            if (responseArray.length() >= 1) {
                                JSONObject resObj1 = responseArray.getJSONObject(0);
                                JSONObject resMesg = resObj1.getJSONObject("Details");
                                if (resMesg.getString("appCode").equals(Api.APP_STATUS_OK)) {
                                    JSONObject poObject = responseArray.getJSONObject(1);
                                    JSONArray sapJs = poObject.getJSONArray("sapJs");
                                    for (int i = 0; i < sapJs.length(); i++) {
                                        JSONObject eachUnfulfilledObject = sapJs.getJSONObject(i);
                                        UnfulfilledPOModel unfulfilledPO = new UnfulfilledPOModel();
                                        unfulfilledPO.setPoNo(eachUnfulfilledObject.getString("docNum"));
                                        unfulfilledPO.setPoDate(eachUnfulfilledObject.getString("docDate"));
                                        unfulfilledPO.setPoVendorCode(eachUnfulfilledObject.getString("cardCode"));
                                        String productCode = eachUnfulfilledObject.getString("itemCode");
                                        unfulfilledPO.setPoProduct(productCode);
                                        if (!pCodeList.contains(productCode)) {
                                            pCodeList.add(productCode);
                                            Product product = new Product();
                                            product.setpCode(productCode);
                                            productList.add(product);
                                        }
                                        unfulfilledPO.setPoQty(eachUnfulfilledObject.getString("quantity"));
                                        unfulfilledPO.setPoUnfulfillQty_1(eachUnfulfilledObject.getString("openQty"));
                                        unfulfilledPO.setDraftGrnQty_2(eachUnfulfilledObject.getString("draftQty"));
                                        unfulfilledPO.setBalance_1_2(eachUnfulfilledObject.getString("opndraft"));
                                        unfulfilledPO.setPoUnitPrice(eachUnfulfilledObject.getString("price"));
                                        unfulfilledPO.setPoAmount(eachUnfulfilledObject.getString("docTotal"));
                                        unfulfilledPO.setPoGst(eachUnfulfilledObject.getString("vatSum"));
                                        unfulfilledPO.setPoTotal(eachUnfulfilledObject.getString("docTotalAmt"));
                                        unfulfilledPO.setGrnRef(eachUnfulfilledObject.getString("draftGRN"));
                                        unfulfilledPO.setGtpBookingNo(eachUnfulfilledObject.getString("u_GTPREFNO"));
                                        unfulfilledPO.setJList(true);
                                        unfulfilledPOLists.add(unfulfilledPO);
                                    }
                                    JSONArray sapBs = poObject.getJSONArray("sapBs");
                                    for (int i = 0; i < sapBs.length(); i++) {
                                        JSONObject eachUnfulfilledObject = sapBs.getJSONObject(i);
                                        UnfulfilledPOModel unfulfilledPO = new UnfulfilledPOModel();
                                        unfulfilledPO.setPoNo(eachUnfulfilledObject.getString("docNum"));
                                        unfulfilledPO.setPoDate(eachUnfulfilledObject.getString("docDate"));
                                        unfulfilledPO.setPoVendorCode(eachUnfulfilledObject.getString("cardCode"));
                                        String productCode = eachUnfulfilledObject.getString("itemCode");
                                        unfulfilledPO.setPoProduct(productCode);
                                        if (!pCodeList.contains(productCode)) {
                                            pCodeList.add(productCode);
                                            Product product = new Product();
                                            product.setpCode(productCode);
                                            productList.add(product);
                                        }
                                        unfulfilledPO.setPoQty(eachUnfulfilledObject.getString("quantity"));
                                        unfulfilledPO.setPoUnfulfillQty_1(eachUnfulfilledObject.getString("openQty"));
                                        unfulfilledPO.setDraftGrnQty_2(eachUnfulfilledObject.getString("draftQty"));
                                        unfulfilledPO.setBalance_1_2(eachUnfulfilledObject.getString("opndraft"));
                                        unfulfilledPO.setPoUnitPrice(eachUnfulfilledObject.getString("price"));
                                        unfulfilledPO.setPoAmount(eachUnfulfilledObject.getString("docTotal"));
                                        unfulfilledPO.setPoGst(eachUnfulfilledObject.getString("vatSum"));
                                        unfulfilledPO.setPoTotal(eachUnfulfilledObject.getString("docTotalAmt"));
                                        unfulfilledPO.setGrnRef(eachUnfulfilledObject.getString("draftGRN"));
                                        unfulfilledPO.setGtpBookingNo(eachUnfulfilledObject.getString("u_GTPREFNO"));
                                        unfulfilledPO.setJList(false);
                                        unfulfilledPOLists.add(unfulfilledPO);
                                    }
                                    searchLists.clear();
                                    searchLists.addAll(unfulfilledPOLists);

                                    unfulfilledPoAdapter.notifyDataSetChanged();
                                    rvUnfulfilledPoListings.setVisibility(View.VISIBLE);
                                    tvErrorText.setVisibility(View.GONE);
                                    progressBar.setVisibility(View.GONE);
                                } else {
                                    progressBar.setVisibility(View.GONE);
                                    rvUnfulfilledPoListings.setVisibility(View.GONE);
                                    tvErrorText.setText(resMesg.getString("appCodeMessage"));
                                    tvErrorText.setVisibility(View.VISIBLE);
                                }
                            }
                        } catch (JSONException e) {
                            Logger.e("unfulfillPOListingsTask json ex", e.getMessage());
                        }
                    }

                    @Override
                    public void onError(String errorResponse, VolleyError volleyError) {
                        progressBar.setVisibility(View.GONE);
                        String errorMsg = "Unexpected error !!! Please try again with internet access.";
                        try {
                            JSONObject errorObj = new JSONObject(errorResponse);
                            errorMsg = errorObj.getJSONObject("Details").getString("appCodeMessage");
                        } catch (Exception e) {
                            Logger.e("unfulfillPOListingsTask error res", e.getMessage() + " ");
                        }
                        Logger.e("error message", errorMsg);
                        tvErrorText.setText(errorMsg);
                        tvErrorText.setVisibility(View.VISIBLE);
                    }
                }, "unfulfillPOListingsTask");
    }


    boolean jList, bList;
    String startDate, endDate;
    int agingPos = 0;
    int shortPo, shortUnfulfill, short1_2;

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == REQUEST_FILTER_CODE) {
                Logger.e("filter data", data + " ");
                productList = (ArrayList<Product>) data.getSerializableExtra("productList");
                jList = data.getBooleanExtra("jList", false);
                bList = data.getBooleanExtra("bList", false);
                startDate = data.getStringExtra("startDate");
                endDate = data.getStringExtra("endDate");
                agingPos = data.getIntExtra("agingPos", 0);
                shortPo = data.getIntExtra("shortPo", UnfulfillPOFilterActivity.NONE);
                shortUnfulfill = data.getIntExtra("shortUnfulfill", UnfulfillPOFilterActivity.NONE);
                short1_2 = data.getIntExtra("short1_2", UnfulfillPOFilterActivity.NONE);

                filterMethod();
            }
        } else if (resultCode == Activity.RESULT_CANCELED) {
            if (requestCode == REQUEST_FILTER_CODE) {
                for (int i = 0; i < productList.size(); i++) {
                    productList.get(i).setSelected(false);
                }
                jList = false;
                bList = false;
                startDate = "";
                endDate = "";
                agingPos = 0;
                shortPo = UnfulfillPOFilterActivity.NONE;
                shortUnfulfill = UnfulfillPOFilterActivity.NONE;
                short1_2 = UnfulfillPOFilterActivity.NONE;

                filterIcon.setImageResource(R.drawable.btn_filter);
            }
        }
    }

    public void filterMethod() {
        searchLists.clear();

        //product filter
        String pCodeLinker = " <*>*<*> ";
        StringBuilder selectedPCodes = new StringBuilder(pCodeLinker);
        for (int i = 0; i < productList.size(); i++) {
            if (productList.get(i).isSelected()) {
                selectedPCodes.append(productList.get(i).getpCode()).append(pCodeLinker);
            }
        }
        Logger.e("log 1111111111111111111111", selectedPCodes.toString());

        Date dateStart = null;
        Date dateEnd = null;
        SimpleDateFormat sD = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        SimpleDateFormat sD1 = new SimpleDateFormat("yyyy-MM-dd");

        try {
            Calendar cal = Calendar.getInstance();
            if (!TextUtils.isEmpty(startDate)) {
                dateStart = sD1.parse(startDate);
                // to include the start date also check start date minus a day
                cal.setTime(dateStart);
                cal.add(Calendar.DATE, -1);
                cal.set(Calendar.HOUR_OF_DAY, cal.getActualMaximum(Calendar.HOUR_OF_DAY));
                cal.set(Calendar.MINUTE, cal.getActualMaximum(Calendar.MINUTE));
                cal.set(Calendar.SECOND, cal.getActualMaximum(Calendar.SECOND));
                dateStart = cal.getTime();
                Logger.e("log 2222222222222222222", dateStart + " ");
            }
            dateEnd = sD1.parse(endDate);
            // to include the start date also check start date minus a day
            cal.setTime(dateEnd);
            cal.add(Calendar.DATE, +1);
            cal.set(Calendar.HOUR_OF_DAY, cal.getActualMaximum(Calendar.HOUR_OF_DAY));
            cal.set(Calendar.MINUTE, cal.getActualMaximum(Calendar.MINUTE));
            cal.set(Calendar.SECOND, cal.getActualMaximum(Calendar.SECOND));
            dateEnd = cal.getTime();
        } catch (ParseException e) {
            Logger.e("inputDate Parse Exception: ", e.getMessage());
            Logger.e("log 333333333333333333333333", "3333333333333333333333333333333333333");
        }

        Logger.e("filter date", dateStart + " " + dateEnd);

        if (selectedPCodes.toString().equals(pCodeLinker) && dateStart == null && dateEnd == null
                && !jList && !bList && agingPos == 0) {
            Logger.e("log 4444444444444444444444444", "444444444444444444444444444444444444444444");
            searchLists.addAll(unfulfilledPOLists);
            shortList();
            unfulfilledPoAdapter.notifyDataSetChanged();
            filterIcon.setImageResource(R.drawable.btn_filter);
            return;
        }

        //get aging day
        int daysOld = 0;
        if (agingPos == 0) daysOld = 0;
        else if (agingPos == 1) daysOld = 10;
        else if (agingPos == 2) daysOld = 30;
        else if (agingPos == 3) daysOld = 60;
        else if (agingPos == 4) daysOld = 90;
        Logger.e("log 5555555555555555555555555", "aging days :" + daysOld);

        for (int i = 0; i < unfulfilledPOLists.size(); i++) {

            //first filter product code
            if (!selectedPCodes.toString().contains(pCodeLinker + unfulfilledPOLists.get(i).getPoProduct() + pCodeLinker)
                    && !selectedPCodes.toString().equals(pCodeLinker)) {
                Logger.e("log 666666666666666666666666666666", "666666666666666666666666666666666666");
                continue;
            }

            if (!((jList && bList) || (!jList && !bList))) {
                Logger.e("log 7777777777777777777777777777777", "77777777777777777777777777777777");
                if (jList && !unfulfilledPOLists.get(i).isJList()) {
                    Logger.e("log 888888888888888888888888888", "888888888888888888888888888888888");
                    continue;
                }
                if (bList && unfulfilledPOLists.get(i).isJList()) {
                    Logger.e("log 9999999999999999999999999999999", "9999999999999999999999999999999999999999");
                    continue;
                }
            }

            //2017-05-04T00:00:00 ==docDate
            Date dateToCheck = null;
            try {
                dateToCheck = sD.parse(unfulfilledPOLists.get(i).getPoDate());
                Logger.e("log aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa", "dateToCheck : " + dateToCheck);
            } catch (ParseException e) {
                Logger.e("dateToCheck Parse Exception: ", e.getMessage());
            }

            if (dateToCheck != null) {
                Logger.e("log bbbbbbbbbbbbbbbbbbbbbbbb", "bbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbb");
                Logger.e("date to check parse date ", unfulfilledPOLists.get(i).getPoDate() + " " + dateToCheck);
                if (dateStart != null && dateEnd != null) {
                    Logger.e("log cccccccccccccccccccccccccccccc", "ccccccccccccccccccccccccccccccc");
                    if (!(dateToCheck.after(dateStart) && dateToCheck.before(dateEnd))) {
                        Logger.e("log ddddddddddddddddddddddd", "dddddddddddddddddddddddddddddddd");
                        continue;
                    }
                } else if (dateStart == null && dateEnd != null) {
                    Logger.e("log eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee", "eeeeeeeeeeeeeeeeeeeeeeeeeee");
                    if (!dateToCheck.before(dateEnd)) {
                        Logger.e("log ffffffffffffffffffffffff", "ffffffffffffffffffffffffffffffffff");
                        continue;
                    }
                } else if (dateStart != null) {
                    Logger.e("log gggggggggggggggggggggggggggggg", "ggggggggggggggggggggggggggggg");
                    if (!dateToCheck.after(dateStart)) {
                        Logger.e("log hhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhh", "hhhhhhhhhhhhhhhhhhhhhhhhhhhhh");
                        continue;
                    }
                }
            }

            //check aging
            if (daysOld > 0) {
                Logger.e("log iiiiiiiiiiiiiiiiiiiiiiiiiiiiii", "iiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiii");
                if (dateToCheck != null) {
                    long diff = new Date().getTime() - dateToCheck.getTime();
                    int daysBefore = (int) TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
                    Logger.e("log jjjjjjjjjjjjjjjjjjjjjjjjjjjjjj", "credated Days ago :" + daysBefore);
                    if (daysBefore > daysOld) {
                        Logger.e("log kkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkk", "kkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkk");
                        continue;
                    }
                }
            }

            Logger.e("log llllllllllllllllllllllllllllllll", "lllllllllllllllllllllllllllll");
            searchLists.add(unfulfilledPOLists.get(i));
        }

        Logger.e("log mmmmmmmmmmmmmmmmmmmmmmmmmmm", "mmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmm");
        shortList();
        unfulfilledPoAdapter.notifyDataSetChanged();
        filterIcon.setImageResource(R.drawable.btn_filter_notification);
    }

    public void shortList() {
        Logger.e("shortList data", shortPo + " >>> " + shortUnfulfill + " >>> " + short1_2);
        if (shortPo == UnfulfillPOFilterActivity.LOW_TO_HIGH) {
            //ascending
            Collections.sort(searchLists, new Comparator<UnfulfilledPOModel>() {
                @Override
                public int compare(UnfulfilledPOModel o1, UnfulfilledPOModel o2) {
                    Double o1qty, o2qty;
                    o1qty = Double.parseDouble(o1.getPoQty());
                    o2qty = Double.parseDouble(o2.getPoQty());
                    return o1qty.compareTo(o2qty);
                }
            });
        } else if (shortPo == UnfulfillPOFilterActivity.HIGH_To_LOW) {
            //descending
            Collections.sort(searchLists, new Comparator<UnfulfilledPOModel>() {
                @Override
                public int compare(UnfulfilledPOModel o1, UnfulfilledPOModel o2) {
                    Double o1qty, o2qty;
                    o1qty = Double.parseDouble(o1.getPoQty());
                    o2qty = Double.parseDouble(o2.getPoQty());
                    return o2qty.compareTo(o1qty);
                }
            });
        }


        if (shortUnfulfill == UnfulfillPOFilterActivity.HIGH_To_LOW) {
            //descending
            Collections.sort(searchLists, new Comparator<UnfulfilledPOModel>() {
                @Override
                public int compare(UnfulfilledPOModel o1, UnfulfilledPOModel o2) {
                    Double o1qty, o2qty;
                    o1qty = Double.parseDouble(o1.getPoUnfulfillQty_1());
                    o2qty = Double.parseDouble(o2.getPoUnfulfillQty_1());
                    return o2qty.compareTo(o1qty);
                }
            });
        } else if (shortUnfulfill == UnfulfillPOFilterActivity.LOW_TO_HIGH) {
            //ascending
            Collections.sort(searchLists, new Comparator<UnfulfilledPOModel>() {
                @Override
                public int compare(UnfulfilledPOModel o1, UnfulfilledPOModel o2) {
                    Double o1qty, o2qty;
                    o1qty = Double.parseDouble(o1.getPoQty());
                    o2qty = Double.parseDouble(o2.getPoQty());
                    return o1qty.compareTo(o2qty);
                }
            });
        }

        if (short1_2 == UnfulfillPOFilterActivity.HIGH_To_LOW) {
            //descending
            Collections.sort(searchLists, new Comparator<UnfulfilledPOModel>() {
                @Override
                public int compare(UnfulfilledPOModel o1, UnfulfilledPOModel o2) {
                    Double o1qty, o2qty;
                    o1qty = Double.parseDouble(o1.getBalance_1_2());
                    o2qty = Double.parseDouble(o2.getBalance_1_2());
                    return o2qty.compareTo(o1qty);
                }
            });
        } else if (short1_2 == UnfulfillPOFilterActivity.LOW_TO_HIGH) {
            //ascending
            Collections.sort(searchLists, new Comparator<UnfulfilledPOModel>() {
                @Override
                public int compare(UnfulfilledPOModel o1, UnfulfilledPOModel o2) {
                    Double o1qty, o2qty;
                    o1qty = Double.parseDouble(o1.getBalance_1_2());
                    o2qty = Double.parseDouble(o2.getBalance_1_2());
                    return o1qty.compareTo(o2qty);
                }
            });
        }
    }

//    public class ReportComparator implements Comparator<UnfulfilledPOModel> {
//        public int compare(UnfulfilledPOModel r1, UnfulfilledPOModel r2) {
//            return ComparisonChain.start()
//                    .compare(r1.getReportKey(), r2.getReportKey())
//                    .compare(r1.getStudentNumber(), r2.getStudentNumber())
//                    .compare(r1.getSchool(), r2.getSchool())
//                    .result();
//        }
//    }
}
