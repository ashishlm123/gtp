package com.emts.gtp.fragment;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.emts.gtp.Api;
import com.emts.gtp.R;
import com.emts.gtp.helper.Logger;
import com.emts.gtp.helper.NetworkUtils;
import com.emts.gtp.helper.VolleyHelper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;


public class LimitsFragment extends Fragment {
    LinearLayout l1;
    ProgressBar progressBar;
    TextView errorText, tvBuyLimit, tvTransMinBuy, tvBuyBalance, tvPerTransMaxBuy,
            tvPerTransMaxSell, tvSellBalance, tvPerTransMinSell, tvSellLimit;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_limits, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        l1 = view.findViewById(R.id.l1);
        progressBar = view.findViewById(R.id.progress_bar);
        errorText = view.findViewById(R.id.error_text);

        tvBuyLimit = view.findViewById(R.id.tv_buy_limit);
        tvTransMinBuy = view.findViewById(R.id.tv__per_trans_min_buy);
        tvPerTransMaxBuy = view.findViewById(R.id.tv_per_trans_max_buy);
        tvBuyBalance = view.findViewById(R.id.tv_buy_balance);

        tvPerTransMinSell = view.findViewById(R.id.tv_per_trans_min_sell);
        tvPerTransMaxSell = view.findViewById(R.id.tv_per_trans_max_sell);
        tvSellBalance = view.findViewById(R.id.tv_sell_balance);
        tvSellLimit = view.findViewById(R.id.tv_sell_limit);

        if (NetworkUtils.isInNetwork(getActivity())) {
            limitsTask();
        } else {
            errorText.setText(getActivity().getResources().getString(R.string.error_no_internet));
            errorText.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.GONE);
            l1.setVisibility(View.GONE);
        }

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    private void limitsTask() {
        progressBar.setVisibility(View.VISIBLE);
        errorText.setVisibility(View.GONE);
        l1.setVisibility(View.GONE);

        VolleyHelper vHelper = VolleyHelper.getInstance(getActivity());
        HashMap<String, String> postParams = vHelper.getPostParams();

        vHelper.addVolleyRequestListeners(Api.getInstance().getLimits, Request.Method.GET, postParams, new VolleyHelper.VolleyHelperInterface() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onSuccess(String response) {
                progressBar.setVisibility(View.GONE);
                try {
                    JSONObject res = new JSONObject(response);
                    JSONArray responseData = res.getJSONArray("ResponseData");
                    if (responseData.length() >= 1) {
                        JSONObject det = responseData.getJSONObject(0);
                        JSONObject details = det.getJSONObject("Details");
                        if (details.getString("appCode").equals(Api.APP_STATUS_OK)) {
                            JSONObject bD = responseData.getJSONObject(1);
                            JSONObject balanceDetails = bD.getJSONObject("BalanceDetails");
                            tvBuyLimit.setText(balanceDetails.getString("daily_buy_limit") + "g");
                            tvTransMinBuy.setText(balanceDetails.getString("buy_click_min") + "g");
                            tvPerTransMaxBuy.setText(balanceDetails.getString("buy_click_max") + "g");
                            tvBuyBalance.setText(balanceDetails.getString("bal_daily_buy") + "g");

                            tvSellLimit.setText(balanceDetails.getString("daily_sell_limit") + "g");
                            tvPerTransMinSell.setText(balanceDetails.getString("sell_click_min") + "g");
                            tvPerTransMaxSell.setText(balanceDetails.getString("sell_click_max") + "g");
                            tvSellBalance.setText(balanceDetails.getString("bal_daily_sell") + "g");
                            l1.setVisibility(View.VISIBLE);

                        } else {
                            l1.setVisibility(View.GONE);
                            progressBar.setVisibility(View.GONE);
                            errorText.setText(details.getString("appCodeMessage"));
                            errorText.setVisibility(View.VISIBLE);
                        }
                    }
                } catch (JSONException e) {
                    Logger.e("limitsTask json ex: ", e.getMessage());
                }
            }

            @Override
            public void onError(String errorResponse, VolleyError volleyError) {
                progressBar.setVisibility(View.GONE);
                l1.setVisibility(View.GONE);
                String errorMsg = "Unexpected error !!! Please try again with internet access.";
                try {
                    JSONObject errorObj = new JSONObject(errorResponse);
                    errorMsg = errorObj.getJSONObject("Details").getString("appCodeMessage");
                } catch (Exception e) {
                    Logger.e("limitsTask error res", e.getMessage() + " ");
                }
                errorText.setText(errorMsg);
                errorText.setVisibility(View.VISIBLE);
            }
        }, "limitsTask");
    }


}
