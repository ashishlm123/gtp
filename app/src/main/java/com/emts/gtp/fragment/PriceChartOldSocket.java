//package com.emts.gtp.fragment;
//
//import android.content.res.Resources;
//import android.graphics.Bitmap;
//import android.graphics.BitmapFactory;
//import android.graphics.Color;
//import android.graphics.Typeface;
//import android.os.Bundle;
//import android.support.annotation.NonNull;
//import android.support.annotation.Nullable;
//import android.support.v4.app.Fragment;
//import android.support.v4.widget.SwipeRefreshLayout;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.AdapterView;
//import android.widget.ArrayAdapter;
//import android.widget.ImageView;
//import android.widget.Spinner;
//import android.widget.TextView;
//
//import com.android.volley.Request;
//import com.android.volley.VolleyError;
//import com.emts.gtp.Api;
//import com.emts.gtp.R;
//import com.emts.gtp.helper.Logger;
//import com.emts.gtp.helper.NetworkUtils;
//import com.emts.gtp.helper.VolleyHelper;
//import com.github.mikephil.charting.charts.LineChart;
//import com.github.mikephil.charting.components.AxisBase;
//import com.github.mikephil.charting.components.Description;
//import com.github.mikephil.charting.components.Legend;
//import com.github.mikephil.charting.components.XAxis;
//import com.github.mikephil.charting.components.YAxis;
//import com.github.mikephil.charting.data.Entry;
//import com.github.mikephil.charting.data.LineData;
//import com.github.mikephil.charting.data.LineDataSet;
//import com.github.mikephil.charting.formatter.IAxisValueFormatter;
//import com.github.nkzawa.emitter.Emitter;
//import com.github.nkzawa.socketio.client.IO;
//import com.github.nkzawa.socketio.client.Socket;
//
//import org.json.JSONArray;
//import org.json.JSONObject;
//
//import java.net.URISyntaxException;
//import java.text.SimpleDateFormat;
//import java.util.ArrayList;
//import java.util.Calendar;
//import java.util.Date;
//import java.util.HashMap;
//import java.util.List;
//import java.util.Objects;
//
//public class PriceChartOldSocket extends Fragment {
//    //    Socket mSocket;
//    TextView tvProductPrice, tvInrDcrRange;
//    ImageView iconBehaviour;
//
//    LineChart lineChart;
//    List<Entry> entries = new ArrayList<>();
//    Bitmap iconStable, iconIncreasing, iconDecreasing;
//    //    int colorStable, colorIncreasing, colorDecreasing;
//    int colorGraphFill;
//
//    boolean isFilterActive;
//    List<Entry> pastEntries = new ArrayList<>();
//    SwipeRefreshLayout swipeRefreshLayout;
//    View holderAvgPrice;
//
//    @Override
//    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
//                             Bundle savedInstanceState) {
//        return inflater.inflate(R.layout.fragment_price_chart, container, false);
//    }
//
//    @Override
//    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
//        super.onViewCreated(view, savedInstanceState);
//
//        holderAvgPrice = view.findViewById(R.id.holderAvgPrice);
//        holderAvgPrice.setVisibility(View.INVISIBLE);
////        productPriceText = view.findViewById(R.id.product_price_text);
//        swipeRefreshLayout = view.findViewById(R.id.swipeRefresh);
//        swipeRefreshLayout.setEnabled(false);
//        tvProductPrice = view.findViewById(R.id.product_price);
//        tvInrDcrRange = view.findViewById(R.id.tv_product_increase_decrease_range);
//        iconBehaviour = view.findViewById(R.id.product_icon_behaviour);
//
//        lineChart = view.findViewById(R.id.chart);
//        lineChart.setNoDataText("Loading Live Price ...");
//        lineChart.setNoDataTextColor(getResources().getColor(R.color.graphColor));
//        lineChart.setNoDataTextTypeface(Typeface.DEFAULT_BOLD);
//        lineChart.setDrawGridBackground(true);
//        lineChart.setDrawBorders(true);
////        lineChart.setDrawValues();
//        lineChart.setMaxVisibleValueCount(6);
//        lineChart.setVisibleXRangeMaximum(6);
//        lineChart.setAutoScaleMinMaxEnabled(true);
//        lineChart.setKeepPositionOnRotation(true);
//        //chart description
//        lineChart.setContentDescription("");
//        Description description = new Description();
//        description.setText("");
//        lineChart.setDescription(description);
//        //axis
//        XAxis xAxis = lineChart.getXAxis();
//        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
//        xAxis.setValueFormatter(new IAxisValueFormatter() {
//            @Override
//            public String getFormattedValue(float value, AxisBase axis) {
//                return String.format(java.util.Locale.US, "%.2f", value);
//            }
//        });
//
//        YAxis leftAxis = lineChart.getAxisLeft();
//        leftAxis.setPosition(YAxis.YAxisLabelPosition.INSIDE_CHART);
//        YAxis rightAxis = lineChart.getAxisRight();
//        rightAxis.setDrawLabels(false);
//
//        //legend
//        Legend legend = lineChart.getLegend();
//        legend.setEnabled(false);
//        lineChart.invalidate();
//
//
//        final Spinner spHour, spDay, spWeek, spMonth; //spMin,
//        ArrayList<String> hourLists, dayLists, weekLists, monthLists; //minLists,
//
////        spMin = view.findViewById(R.id.sp_min);
////        minLists = new ArrayList<>();
////        minLists.add("xx Min");
////        minLists.add("1 Min");
////        minLists.add("2 Min");
////        minLists.add("5 Min");
////        minLists.add("10 Min");
////        ArrayAdapter<String> minAdapter = new ArrayAdapter<>(Objects.requireNonNull(getActivity()),
////                R.layout.item_chart_spinner_textview, minLists);
////        minAdapter.setDropDownViewResource(R.layout.layout_textview);
////        spMin.setAdapter(minAdapter);
////        spMin.post(new Runnable() {
////            @Override
////            public void run() {
////                spMin.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
////                    @Override
////                    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
////                        priceChartFilterTask(adapterView, i);
////                    }
////
////                    @Override
////                    public void onNothingSelected(AdapterView<?> adapterView) {
////
////                    }
////                });
////            }
////        });
//
//        spHour = view.findViewById(R.id.sp_hour);
//        hourLists = new ArrayList<>();
//        hourLists.add("xx Hour");
//        hourLists.add("1 Hour");
//        hourLists.add("2 Hour");
//        hourLists.add("5 Hour");
//        hourLists.add("10 Hour");
//        ArrayAdapter<String> hourAdapter = new ArrayAdapter<>(Objects.requireNonNull(getActivity()),
//                R.layout.item_chart_spinner_textview, hourLists);
//        hourAdapter.setDropDownViewResource(R.layout.layout_textview);
//        spHour.setAdapter(hourAdapter);
//        spHour.post(new Runnable() {
//            @Override
//            public void run() {
//                spHour.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//                    @Override
//                    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
//                        priceChartFilterTask(adapterView, i, false);
//                    }
//
//                    @Override
//                    public void onNothingSelected(AdapterView<?> adapterView) {
//
//                    }
//                });
//            }
//        });
//
//        spDay = view.findViewById(R.id.sp_day);
//        dayLists = new ArrayList<>();
//        dayLists.add("xx Day");
//        dayLists.add("1 Day");
//        dayLists.add("2 Day");
//        dayLists.add("5 Day");
//        dayLists.add("10 Day");
//        ArrayAdapter<String> dayAdapter = new ArrayAdapter<>(Objects.requireNonNull(getActivity()),
//                R.layout.item_chart_spinner_textview, dayLists);
//        dayAdapter.setDropDownViewResource(R.layout.layout_textview);
//        spDay.setAdapter(dayAdapter);
//        spDay.post(new Runnable() {
//            @Override
//            public void run() {
//                spDay.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//                    @Override
//                    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
//                        priceChartFilterTask(adapterView, i, false);
//                    }
//
//                    @Override
//                    public void onNothingSelected(AdapterView<?> adapterView) {
//
//                    }
//                });
//            }
//        });
//
//        spWeek = view.findViewById(R.id.sp_week);
//        weekLists = new ArrayList<>();
//        weekLists.add("xx Week");
//        weekLists.add("1 Week");
//        weekLists.add("2 Week");
//        weekLists.add("5 Week");
//        weekLists.add("10 Week");
//        ArrayAdapter<String> weekAdapter = new ArrayAdapter<>(Objects.requireNonNull(getActivity()),
//                R.layout.item_chart_spinner_textview, weekLists);
//        weekAdapter.setDropDownViewResource(R.layout.layout_textview);
//        spWeek.setAdapter(weekAdapter);
//        spWeek.post(new Runnable() {
//            @Override
//            public void run() {
//                spWeek.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//                    @Override
//                    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
//                        priceChartFilterTask(adapterView, i, false);
//                    }
//
//                    @Override
//                    public void onNothingSelected(AdapterView<?> adapterView) {
//
//                    }
//                });
//            }
//        });
//
//        spMonth = view.findViewById(R.id.sp_month);
//        monthLists = new ArrayList<>();
//        monthLists.add("xx Month");
//        monthLists.add("1 Month");
//        monthLists.add("2 Month");
//        monthLists.add("5 Month");
//        monthLists.add("10 Month");
//        ArrayAdapter<String> monthAdapter = new ArrayAdapter<>(Objects.requireNonNull(getActivity()),
//                R.layout.item_chart_spinner_textview, monthLists);
//        monthAdapter.setDropDownViewResource(R.layout.layout_textview);
//        spMonth.setAdapter(monthAdapter);
//        spMonth.post(new Runnable() {
//            @Override
//            public void run() {
//                spMonth.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//                    @Override
//                    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
//                        priceChartFilterTask(adapterView, i, false);
//                    }
//
//                    @Override
//                    public void onNothingSelected(AdapterView<?> adapterView) {
//
//                    }
//                });
//            }
//        });
//
//
//        //ready the icons and color
//        Resources res = getResources();
//
//        iconStable = BitmapFactory.decodeResource(res, R.drawable.icon_stable);
//        iconIncreasing = BitmapFactory.decodeResource(res, R.drawable.icon_increase);
//        iconDecreasing = BitmapFactory.decodeResource(res, R.drawable.icon_decrease);
//
////        colorStable = res.getColor(R.color.priceStable);
////        colorIncreasing = res.getColor(R.color.priceIncrease);
////        colorDecreasing = res.getColor(R.color.priceDecline);
//        colorGraphFill = res.getColor(R.color.graphColor);
//
//        //load data of 1 day
//        if (NetworkUtils.isInNetwork(getActivity())) {
//            isFilterActive = true;
//            Spinner sp = new Spinner(getActivity());
//            sp.setId(R.id.sp_day);
//            priceChartFilterTask(sp, 1, true); //self made spinner to mock spDay and pos == 1 for 1 day data
//        }
//
//
////        connectToServerSocket();
//
//        //swipe to load live feeds
//        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
//            @Override
//            public void onRefresh() {
////                pastEntries.clear();
////                lineChart.invalidate();
////                lineChart.clear();
////                lineChart.setNoDataText("Loading Live Price ...");
////                lineChart.setNoDataTextColor(getResources().getColor(R.color.graphColor));
////
////                isFilterActive = false;
////                swipeRefreshLayout.setRefreshing(false);
////                if (mSocket != null) {
////                    if (!mSocket.connected()) {
////                        connectToServerSocket();
////                    }
////                }
//            }
//        });
//    }
//
//    private void priceChartFilterTask(final View spinner, int position, final boolean isLoadInit) {
//        if (position == 0) {
//            pastEntries.clear();
//            lineChart.invalidate();
//            lineChart.clear();
//            lineChart.setNoDataText("Loading Live Price ...");
//            lineChart.setNoDataTextColor(getResources().getColor(R.color.graphColor));
//
//            isFilterActive = false;
//            swipeRefreshLayout.setRefreshing(false);
//            return;
//        }
//        isFilterActive = true;
//        pastEntries.clear();
//        lineChart.invalidate();
//        lineChart.clear();
//        lineChart.setNoDataText("Loading past data");
//        lineChart.setNoDataTextColor(getResources().getColor(R.color.graphColor));
//
//        tvProductPrice.setText("");
//        iconBehaviour.setImageResource(0);
//        tvInrDcrRange.setText("");
//
//        //1,2,5,10
//        int spValue = 0;
//        if (position == 1) {
//            spValue = 1;
//        } else if (position == 2) {
//            spValue = 2;
//        } else if (position == 3) {
//            spValue = 5;
//        } else if (position == 4) {
//            spValue = 10;
//        }
//
//        String url = "";
////        if (spinner.getId() == R.id.sp_min) {
////            url = Api.getInstance().chartByHour + String.valueOf(spValue / 60);
////        } else
//        if (spinner.getId() == R.id.sp_hour) {
//            url = Api.getInstance().chartByHour + String.valueOf(spValue);
//        } else if (spinner.getId() == R.id.sp_day) {
//            url = Api.getInstance().chartByDay + String.valueOf(spValue);
//        } else if (spinner.getId() == R.id.sp_week) {
//            url = Api.getInstance().chartByDay + String.valueOf(spValue * 7);
//        } else if (spinner.getId() == R.id.sp_month) {
//            url = Api.getInstance().chartByMonth + String.valueOf(spValue);
//        }
//
//        VolleyHelper vHelper = VolleyHelper.getInstance(getActivity());
//        HashMap<String, String> postMap = vHelper.getPostParams();
//
//        vHelper.addVolleyRequestListeners(url, Request.Method.GET,
//                postMap, new VolleyHelper.VolleyHelperInterface() {
//                    @Override
//                    public void onSuccess(final String response) {
//                        lineChart.removeAllViews();
//                        try {
//                            final JSONArray priceResponse = new JSONArray(response);
//                            if (priceResponse.length() <= 0) {
//                                pastEntries.clear();
//                                lineChart.invalidate();
//                                lineChart.clear();
//                                lineChart.setNoDataText("No Data Available");
//                                lineChart.setNoDataTextColor(Color.RED);
//
//                                tvProductPrice.setText("");
//                                iconBehaviour.setImageResource(0);
//                                tvInrDcrRange.setText("");
//                                return;
//                            }
//                            //    "dtime": "2018-07-11T01:00:00.000Z"
////                            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
//                            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//                            final Calendar calendar = Calendar.getInstance();
////                            format.setTimeZone(TimeZone.getTimeZone("UTC"));
//                            for (int i = 0; i < priceResponse.length(); i++) {
//                                JSONObject eachPrice = priceResponse.getJSONObject(i);
//                                Double liveBuyPrice = Double.parseDouble(eachPrice.getString("avgprice"));
//
//                                calendar.setTime(format.parse(eachPrice.getString("dtime")));
//
//                                Entry entry = new Entry(calendar.getTimeInMillis(), liveBuyPrice.floatValue());
//                                pastEntries.add(entry);
//
//                            }
//                            if (isLoadInit) {
////                                entries.addAll(pastEntries);
////                                Collections.reverse(entries);
//                                List<Entry> entriesToShow;
//                                if (pastEntries.size() > 12) {
//                                    entriesToShow = pastEntries.subList(pastEntries.size() - 12, pastEntries.size());
//                                } else {
//                                    entriesToShow = new ArrayList<>(pastEntries);
//                                }
//                                LineDataSet dataSet = new LineDataSet(entriesToShow, "");
//                                dataSet.setColor(Color.GRAY);
//                                dataSet.setDrawFilled(true);
//                                dataSet.setFillColor(colorGraphFill);
//                                dataSet.setFillAlpha(120);
//                                dataSet.setDrawCircles(false);
//                                LineData lineData = new LineData(dataSet);
//                                lineChart.setData(lineData);
//                            } else {
//                                List<Entry> entriesToShow;
//                                if (pastEntries.size() > 12) {
//                                    entriesToShow = pastEntries.subList(pastEntries.size() - 12, pastEntries.size());
//                                } else {
//                                    entriesToShow = new ArrayList<>(pastEntries);
//                                }
//                                LineDataSet dataSet = new LineDataSet(entriesToShow, "");
//                                dataSet.setColor(Color.GRAY);
//                                dataSet.setDrawFilled(true);
//                                dataSet.setFillColor(colorGraphFill);
//                                dataSet.setFillAlpha(120);
//                                dataSet.setDrawCircles(false);
//                                LineData lineData = new LineData(dataSet);
//                                lineChart.setData(lineData);
//                            }
//
////                            if (spinner.getId() == R.id.sp_min) {
////                                lineChart.getXAxis().setValueFormatter(new IAxisValueFormatter() {
////                                    @Override
////                                    public String getFormattedValue(float value, AxisBase axis) {
////                                        Calendar calendar1 = Calendar.getInstance();
////                                        Date date = new Date();
////                                        date.setTime((long) value);
////                                        calendar1.setTime(date);
////                                        Logger.e("min", value + "");
////                                        return "00:00:" + String.format("%02d", calendar1.get(Calendar.MINUTE));
////                                    }
////                                });
////                            } else
//                            if (spinner.getId() == R.id.sp_hour) {
//                                lineChart.getXAxis().setValueFormatter(new IAxisValueFormatter() {
//                                    @Override
//                                    public String getFormattedValue(float value, AxisBase axis) {
//                                        Calendar calendar1 = Calendar.getInstance();
//                                        Date date = new Date();
//                                        date.setTime((long) value);
//                                        calendar1.setTime(date);
////                                        Logger.e("hour", value + "");
//                                        return String.format("%02d", calendar1.get(Calendar.HOUR)) +
//                                                ":" + String.format("%02d", calendar1.get(Calendar.MINUTE)) + ":00";
//                                    }
//                                });
//                            } else if (spinner.getId() == R.id.sp_day) {
//                                lineChart.getXAxis().setValueFormatter(new IAxisValueFormatter() {
//                                    @Override
//                                    public String getFormattedValue(float value, AxisBase axis) {
//                                        Calendar calendar1 = Calendar.getInstance();
//                                        Date date = new Date();
//                                        date.setTime((long) value);
//                                        calendar1.setTime(date);
////                                        Logger.e("day", value + "");
//                                        return String.format("%02d", calendar1.get(Calendar.HOUR_OF_DAY)) +
//                                                ":" + String.format("%02d", calendar1.get(Calendar.MINUTE)) +
//                                                (calendar1.get(Calendar.AM_PM) == Calendar.AM ? "AM" : "PM");
//                                    }
//                                });
//                            } else if (spinner.getId() == R.id.sp_week || spinner.getId() == R.id.sp_month) {
//                                lineChart.getXAxis().setValueFormatter(new IAxisValueFormatter() {
//                                    @Override
//                                    public String getFormattedValue(float value, AxisBase axis) {
//                                        Calendar calendar1 = Calendar.getInstance();
//                                        Date date = new Date();
//                                        date.setTime((long) value);
//                                        calendar1.setTime(date);
////                                        Logger.e("month", value + "");
//                                        return new SimpleDateFormat("MMM").format(calendar1.getTime())
//                                                + " " + calendar1.get(Calendar.DAY_OF_MONTH);
//                                    }
//                                });
//                            } else {
//                                lineChart.getXAxis().setValueFormatter(new IAxisValueFormatter() {
//                                    @Override
//                                    public String getFormattedValue(float value, AxisBase axis) {
//                                        Calendar calendar1 = Calendar.getInstance();
//                                        Date date = new Date();
//                                        date.setTime((long) value);
//                                        calendar1.setTime(date);
////                                        Logger.e("min", value + "");
//                                        return "00:00:" + String.format("%02d", calendar.get(Calendar.MINUTE));
//                                    }
//                                });
//                            }
//                            lineChart.invalidate();
//                            if (isLoadInit) {
//                                isFilterActive = false;
//                                connectToServerSocket();
//                            } else {
//                                holderAvgPrice.setVisibility(View.INVISIBLE);
//                            }
//
//                            if (pastEntries.size() < 1) return;
//                            float prevSellPrice = pastEntries.get(pastEntries.size() - 2).getY();
//                            float currentSellPrice = pastEntries.get(pastEntries.size() - 1).getY();
//
//                            tvProductPrice.setText(String.format("%.4f", currentSellPrice) + " MYR/g");
//
//                            float difference = Math.abs(prevSellPrice - (float) Math.abs(currentSellPrice));
//                            float changePercentage = (difference / prevSellPrice) * 100;
//                            if (prevSellPrice == currentSellPrice) {
//                                iconBehaviour.setImageBitmap(iconStable);
//                                tvInrDcrRange.setText("0.0000-0.0000%");
//                            } else if (prevSellPrice < currentSellPrice) {
//                                iconBehaviour.setImageBitmap(iconIncreasing);
//                                tvInrDcrRange.setText(String.format("%.4f", difference) + "-"
//                                        + String.format("%.4f", changePercentage) + "%");
//                            } else {
//                                iconBehaviour.setImageBitmap(iconDecreasing);
//                                tvInrDcrRange.setText("-" + String.format("%.4f", difference) + "-"
//                                        + String.format("%.4f", changePercentage) + "%");
//                            }
//                        } catch (Exception e) {
//                            Logger.e("priceChartFilterTask res ex", e.getMessage() + " ");
//                        }
//                    }
//
//                    @Override
//                    public void onError(String errorResponse, VolleyError volleyError) {
//                        String errorMsg = "Unexpected error !!! Please try again with internet access.";
//                        try {
//                            JSONObject errorObj = new JSONObject(errorResponse);
//                            errorMsg = errorObj.getJSONObject("Details").getString("appCodeMessage");
//                        } catch (Exception e) {
//                            Logger.e("priceChartFilterTask error res", e.getMessage() + " ");
//                        }
//                        lineChart.setNoDataText(errorMsg);
//                        lineChart.setNoDataTextColor(getResources().getColor(R.color.red));
//                    }
//                }, "priceChartFilterTask");
//
//    }
//
//    @Override
//    public void onDetach() {
//        super.onDetach();
//
//        VolleyHelper.getInstance(getActivity()).cancelRequest("productsListingTask");
//        if (mSocket != null) {
//            mSocket.disconnect();
//            mSocket.off();
//        }
//    }
//
//    private void connectToServerSocket() {
//        try {
//            if (mSocket != null) {
//                if (mSocket.connected()) {
//                    mSocket.off();
//                    mSocket.disconnect();
//                    mSocket.off();
//                }
//            }
//
//            String nodeServerHostUrl = Api.getInstance().liveChartUrl;
//            mSocket = IO.socket(nodeServerHostUrl);
//            mSocket.on(Socket.EVENT_CONNECT, new Emitter.Listener() {
//                @Override
//                public void call(Object... args) {
//                    Logger.e("nodeJs def connect call", "Object : ");
//                    if (mSocket.connected()) {
//                        Logger.e("nodeJs connected", mSocket.toString() + "");
//                    }
//                }
//            }).on(Socket.EVENT_DISCONNECT, new Emitter.Listener() {
//                @Override
//                public void call(Object... args) {
//                    Logger.e("nodeJs def. disconnect", "Object " + args[0]);
//                }
//            });
//            //listen to node before connect
//            listenToHeartBeat();
//            listenToIntIX();
//
//            mSocket.connect();
//            Logger.e("connectToServerSocket", ">>>>>>>>>>CONNECTING>>>>>>>>>>\n" + nodeServerHostUrl);
//        } catch (URISyntaxException e) {
//            Logger.e("connectToServerSocket ex", e.getMessage() + " ");
//        }
//    }
//
//    public void listenToHeartBeat() {
//        Logger.e("listenToHeartBeat EVENT:", "goodfeed");
//        mSocket.off("goodfeed");
//        mSocket.on("goodfeed", new Emitter.Listener() {
//            @Override
//            public void call(Object... args) {
//                Logger.e("listenToHeartBeat call", args[0].toString() + "");
//            }
//        });
//    }
//
//    public void listenToIntIX() {
//        Logger.e("listenToIntIX EVENT:", "intlX");
//        mSocket.off("intlX");
//        mSocket.on("intlX", new Emitter.Listener() {
//            @Override
//            public void call(final Object... args) {
////                Logger.e("listenToIntIX call", args[0].toString() + "");
//                if (getActivity() != null)
//                    getActivity().runOnUiThread(new Runnable() {
//                        @Override
//                        public void run() {
//                            try {
//                                final JSONObject eachChat = new JSONObject(args[0].toString());
//                                Double liveBuyPrice = Double.parseDouble(eachChat.getString("gp_livebuyprice_gm"));
//                                Double liveSellPrice = Double.parseDouble(eachChat.getString("gp_livesellprice_gm"));
//
//                                tvProductPrice.setText(String.format("%.4f", liveBuyPrice) + " MYR/g");
//
//
//                                float lastTime = 0;
//                                try {
//                                    lastTime = entries.get(entries.size() - 1).getX();
//                                } catch (Exception e) {
//                                    lastTime = 0;
//                                }
//
////                                entries.add(new Entry((float) entries.size(), liveBuyPrice.floatValue()));
////                                entries.add(new Entry(System.currentTimeMillis(), liveBuyPrice.floatValue()));
//                                entries.add(new Entry(lastTime + 1000, liveBuyPrice.floatValue()));
//
//                                // if data is showing from api not live return
//                                if (isFilterActive) return;
//
//                                List<Entry> entriesToShow;
//                                if (entries.size() > 12) {
//                                    entriesToShow = entries.subList(entries.size() - 12, entries.size());
//                                } else {
//                                    entriesToShow = new ArrayList<>(entries);
//                                }
//                                Logger.e("listen enteirsSize", entriesToShow.size() + " ** " + entries.size());
//                                for (int i = 0; i < entriesToShow.size(); i++) {
//                                    Logger.e(" X :: Y", entriesToShow.get(i).getX() + " :: " + entriesToShow.get(i).getY());
//                                }
//
//                                //formate second
////                                lineChart.getXAxis().setValueFormatter(new IAxisValueFormatter() {
////                                    @Override
////                                    public String getFormattedValue(float value, AxisBase axis) {
////                                        Calendar calendar1 = Calendar.getInstance();
////                                        Date date = new Date();
////                                        date.setTime((long) value);
////                                        calendar1.setTime(date);
//////                                        Logger.e("min", value + "");
////                                        return "00:00:" + String.format("%02d", calendar1.get(Calendar.SECOND));
////                                    }
////                                });
//
//
//                                LineDataSet dataSet = new LineDataSet(entriesToShow, "");
//                                dataSet.setColor(Color.GRAY);
//                                dataSet.setDrawFilled(true);
//                                dataSet.setFillColor(colorGraphFill);
//                                dataSet.setFillAlpha(120);
//                                dataSet.setDrawCircles(false);
//                                LineData lineData = new LineData(dataSet);
//                                lineChart.setData(lineData);
//                                lineChart.invalidate();
//
//                                if (entries.size() < 1) return;
//                                float prevSellPrice = entries.get(entries.size() - 2).getY();
//                                float difference = Math.abs(prevSellPrice - (float) Math.abs(liveBuyPrice));
//                                float changePercentage = (difference / prevSellPrice) * 100;
//                                if (prevSellPrice == liveBuyPrice.floatValue()) {
//                                    iconBehaviour.setImageBitmap(iconStable);
//                                    tvInrDcrRange.setText("0.0000-0.0000%");
//                                } else if (prevSellPrice < liveBuyPrice.floatValue()) {
//                                    iconBehaviour.setImageBitmap(iconIncreasing);
//                                    tvInrDcrRange.setText(String.format("%.4f", difference) + "-"
//                                            + String.format("%.4f", changePercentage) + "%");
//                                } else {
//                                    iconBehaviour.setImageBitmap(iconDecreasing);
//                                    tvInrDcrRange.setText("-" + String.format("%.4f", difference) + "-"
//                                            + String.format("%.4f", changePercentage) + "%");
//                                }
//                                holderAvgPrice.setVisibility(View.VISIBLE);
//                            } catch (Exception e) {
//                                Logger.e("listenToIntIX onUI thread ex", e.getMessage() + " ");
//                            }
//                        }
//                    });
//            }
//        });
//    }
//
//}
