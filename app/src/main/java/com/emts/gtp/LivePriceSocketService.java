package com.emts.gtp;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;

import com.emts.gtp.helper.Logger;
import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.socketio.client.IO;
import com.github.nkzawa.socketio.client.Socket;

import org.greenrobot.eventbus.EventBus;

import java.net.URISyntaxException;

public class LivePriceSocketService extends Service {
    // Binder given to clients
    private final IBinder mBinder = new LocalBinder();
    static Socket mSocket;

    /**
     * Class used for the client Binder.  Because we know this service always
     * runs in the same process as its clients, we don't need to deal with IPC.
     */
    public class LocalBinder extends Binder {
        LivePriceSocketService getService() {
            // Return this instance of LocalService so clients can call public methods
            return LivePriceSocketService.this;
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        Logger.e("LivePriceSocketService", "onCreate");
        connectToServerSocket();
    }

    @Override
    public void onDestroy() {
        if (mSocket != null) {
            mSocket.disconnect();
            mSocket.off();
        }
        super.onDestroy();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Logger.e("LivePriceSocketService", "onStartCommand");
        if (!mSocket.connected()) {
            connectToServerSocket();
        }
        return START_STICKY;
    }

    private void connectToServerSocket() {
        try {
            if (mSocket != null) {
                if (mSocket.connected()) {
                    mSocket.off();
                    mSocket.disconnect();
                    mSocket.off();
                }
            }

//            IO.Options opts = new IO.Options();
////            opts.forceNew = true;
////            opts.reconnection = true;
////        opts.query = "auth_token=" + authToken;
//            opts.transports = new String[]{WebSocket.NAME};

            String nodeServerHostUrl = Api.getInstance().socketUrl;
            mSocket = IO.socket(nodeServerHostUrl);
            mSocket.on(Socket.EVENT_CONNECT, new Emitter.Listener() {
                @Override
                public void call(Object... args) {
                    Logger.e("nodeJs def connect call", "Object : ");
                    if (mSocket.connected()) {
                        Logger.e("nodeJs connected", mSocket.toString() + "");
                    }
                }
            }).on(Socket.EVENT_DISCONNECT, new Emitter.Listener() {
                @Override
                public void call(Object... args) {
                    Logger.e("nodeJs def. disconnect", "Object " + args[0]);
                }
            });
            //listen to node before connect
            listenToHeartBeat();
            listenToIntIX();
            listenToTradeOpen();

            mSocket.connect();
            Logger.e("connectToServerSocket", ">>>>>>>>>>CONNECTING>>>>>>>>>>\n" + nodeServerHostUrl);
        } catch (URISyntaxException e) {
            Logger.e("connectToServerSocket ex", e.getMessage() + " ");
        }
    }

    public void listenToHeartBeat() {
        Logger.e("listenToHeartBeat EVENT:", "goodfeed");
        mSocket.off("goodfeed");
        mSocket.on("goodfeed", new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                Logger.e("listenToHeartBeat call", args[0].toString() + "");
            }
        });
    }

    public void listenToIntIX() {
//        Logger.e("listenToIntIX EVENT:", "intlX");
        mSocket.off("intlX");
        mSocket.on("intlX", new Emitter.Listener() {
            @Override
            public void call(final Object... args) {
//                Logger.e("listenToIntIX call", args[0].toString() + "");
                EventBus.getDefault().post(new IntIXMessageObj(args[0].toString()));
            }
        });
    }

    public void listenToTradeOpen() {
        Logger.e("listenToTradeOpen EVENT:", "tradeopen");
        mSocket.off("tradeopen");
        mSocket.on("tradeopen", new Emitter.Listener() {
            @Override
            public void call(final Object... args) {
                Logger.e("listenToTradeOpen call", args[0].toString() + "");
                EventBus.getDefault().post(new TradeOpenMesObj(args[0].toString()));

            }
        });
    }

    //EVENT BUS MESSAGE OBJECTS
    public static class TradeOpenMesObj {
        public String msg;

        public TradeOpenMesObj(String msg) {
            this.msg = msg;
        }
    }

    public static class IntIXMessageObj {
        public String msg;

        public IntIXMessageObj(String msg) {
            this.msg = msg;
        }
    }
}
