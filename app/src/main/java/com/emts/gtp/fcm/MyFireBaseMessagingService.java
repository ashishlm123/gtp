package com.emts.gtp.fcm;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.support.v4.app.NotificationCompat;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.emts.gtp.Api;
import com.emts.gtp.MainActivity;
import com.emts.gtp.R;
import com.emts.gtp.activity.SplashActivity;
import com.emts.gtp.helper.Logger;
import com.emts.gtp.helper.NetworkUtils;
import com.emts.gtp.helper.PreferenceHelper;
import com.emts.gtp.helper.VolleyHelper;
import com.emts.gtp.model.NotificationModel;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class MyFireBaseMessagingService extends FirebaseMessagingService {
    private static final String TAG = "MyFirebaseMsgService";
    /*
     * Called when message is received.
     *
     * @param remoteMessage Object representing the message received from Firebase Cloud Messaging.
     */

    @Override
    public void onNewToken(String token) {
        Logger.e(TAG, "Refreshed token: " + token);

        // If you want to send messages to this application instance or
        // manage this apps subscriptions on the server side, send the
        // Instance ID token to your app server.
        registerDeviceTokenToServer(token, this);
    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
       /* There are two types of messages: data messages and notification messages. Data messages are handled
         here in onMessageReceived whether the app is in the foreground or background. Data messages are the type
         traditionally used with GCM. Notification messages are only received here in onMessageReceived when the app
         is in the foreground. When the app is in the background an automatically generated notification is displayed.
         When the user taps on the notification they are returned to the app. Messages containing both notification
         and data payloads are treated as notification messages. The FireBase console always sends notification
         messages. For more see: https://firebase.google.com/docs/cloud-messaging/concept-options */

        // Not getting messages here? See why this may be: https://goo.gl/39bRNJ
        Logger.e(TAG, "From: " + remoteMessage.getFrom());

        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0) {
            Logger.e(TAG, "Message data payload: " + remoteMessage.getData());
//            sendNotification(remoteMessage.getData());
        }

        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null) {
            Logger.e(TAG, "Message Notification Body: " + remoteMessage.getNotification().getBody());
        }

        /* Also if you intend on generating your own notifications as a result of a received FCM
        message, here is where that should be initiated. See sendNotification method below. */
        sendNotification(this, remoteMessage.getNotification().getTitle(), remoteMessage.getNotification().getBody());
    }

    public static void sendNotification(Context context, String title, String body) {
        try {
            Intent intent = new Intent(context, SplashActivity.class);
            intent.putExtra("isNotification", true);
            PendingIntent pendingIntent = PendingIntent.getActivity(context,
                    0, intent, PendingIntent.FLAG_ONE_SHOT);

            Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(context)
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setContentTitle(title)
                    .setStyle(new NotificationCompat.BigTextStyle().bigText(body))
                    .setContentText(body)
                    .setAutoCancel(true)
                    .setSound(defaultSoundUri)
                    .setContentIntent(pendingIntent);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                notificationBuilder.setSmallIcon(R.drawable.ic_notification);
            }

            NotificationManager notificationManager =
                    (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

            int m = (int) ((new Date().getTime() / 1000L) % Integer.MAX_VALUE);
            notificationManager.notify(m /* ID of notification */, notificationBuilder.build());
        } catch (Exception e) {
            Logger.e(TAG + "notification data ex", e.getMessage() + " ");
        }
    }

    public static void registerDeviceTokenToServer(String deviceToken, Context context) {
        if (NetworkUtils.isInNetwork(context)) {
            updateDeviceToken(context, deviceToken);
        } else {
            PreferenceHelper.getInstance(context).edit().putBoolean(PreferenceHelper.FCM_TOKEN_UPDATED, false).apply();
        }
    }

    private static void updateDeviceToken(Context context, String token) {
        final PreferenceHelper prefsHelper = PreferenceHelper.getInstance(context);
        VolleyHelper volleyHelper = VolleyHelper.getInstance(context);
        HashMap<String, String> postParams = volleyHelper.getPostParams();
        postParams.put("tokenid", token);
        Logger.e("devic_update", postParams.get("tokenid"));

        volleyHelper.addVolleyRequestListeners(Api.getInstance().updatePushToken, Request.Method.POST,
                postParams, new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(String response) {
                        try {
                            JSONObject responseData = new JSONObject(response);
                            if (responseData.getJSONObject("ResponseData").getJSONObject("Details").getInt("appCode") == 1000) {
                                prefsHelper.edit().putBoolean(PreferenceHelper.FCM_TOKEN_UPDATED, true).apply();
                            } else {
                                prefsHelper.edit().putBoolean(PreferenceHelper.FCM_TOKEN_UPDATED, false).apply();
                            }
                        } catch (JSONException e) {
                            Logger.e("updateDeviceToken json exception ", e.getMessage());
                        }
                    }

                    @Override
                    public void onError(String errorResponse, VolleyError volleyError) {
                        prefsHelper.edit().putBoolean(PreferenceHelper.FCM_TOKEN_UPDATED, false).apply();
                    }
                }, "updateDeviceToken");

    }
}
