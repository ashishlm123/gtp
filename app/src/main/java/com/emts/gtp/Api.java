package com.emts.gtp;

/**
 * Created by theone on 10/18/2016.
 */

public class Api {
    private static String apiVersion = "v1.0";
    private static Api api;

    public static String APP_STATUS_OK = "1000";
    public static String DISALLOW_ACTION_PURCHASE = "1001";
    public static String DISALLOW_ACTION_SELL = "1002";
    public static String RETRY_AUTH_EXCEEDED = "1003";

    public static Api getInstance() {
        if (api != null) {
            return api;
        } else {
            api = new Api();
            return api;
        }
    }

    public final String apiKey = "sdkfhiuews3549835";

    public String getHost() {
//        return "http://devgtp.ace2u.com";
        return "https://demogtp.ace2u.com";
    }

    //    public String socketUrl = "https://shouty20.ace2u.com";
    public String socketUrl = "https://shouty10.ace2u.com";
    public String liveChartUrl = socketUrl + "/c30";
    public final String chartByHour = socketUrl + "/getHourly?hour=";
    public final String chartByDay = socketUrl + "/getDaily?day=";
    public final String chartByWeek = socketUrl + "/getWeekly?week=";
    public final String chartByMonth = socketUrl + "/getMonthly?month=";

    private String getApi() {
        return getHost() + "/CustomerTransaction-portlet/services/api/" + apiVersion + "/";
    }

    public final String updatePushToken = getApi() + "phone/update";
//
//    private String getAPi2() {
//        return getHost() + "/api/jsonws/user/";
//    }

    public String tncApi = getApi() + "content/terms-conditions";
    public String noticeApi = getApi() + "content/important-notice";
    public final String getUserBalanceApi = getApi() + "balance";
    public final String generateOTP = getApi() + "otp";
    public final String submitOtp = getApi() + "otp/verify";
    public final String updatePassword = getApi() + "myuser/chgpwd";
    public final String updatePasswordFromInside = getApi() + "myuser/chgmypwd";
    public final String getNotifications = getApi() + "announcements";
    public final String getLimits = getApi() + "balance";
    public final String aboutUs = getApi() + "content/about-us";
    public final String productList = getApi() + "products";
    public final String tradeBookSpotOrder = getApi() + "tradebook/";
    public final String tradeBookFutureOrder = getApi() + "tradebookfuture/";
    public final String unfulfillPO = getApi() + "unfillpo";
    public final String salesVerify = getApi() + "sales/verify";
    public final String purchaseVerify = getApi() + "purchase/verify";
    public final String salesConfirm = getApi() + "sales/";
    public final String purchaseConfirm = getApi() + "purchase/";
    public final String getUserDetails = getApi() + "myuser";
    public final String updateUserDetails = getApi() + "myuser/update";

    public final String askVerify = getApi() + "ask/verify";
    public final String bidVerify = getApi() + "bid/verify";
    public final String askConfirm = getApi() + "ask";
    public final String bidConfirm = getApi() + "bid";
    public final String uploadImage = getApi() + "myuser/upload-pic";
    public final String cancelBid = getApi() + "bid/cancel";
    public final String cancelAsk = getApi() + "ask/cancel";

}