package com.emts.gtp.activity;

import android.app.AlertDialog;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.RadioButton;
import android.widget.Switch;
import android.widget.TextView;

import com.emts.gtp.Database;
import com.emts.gtp.FingerPrintHelper;
import com.emts.gtp.R;
import com.emts.gtp.helper.AlertUtils;
import com.emts.gtp.helper.PreferenceHelper;

public class SettingsActivity extends BaseActivity {
    PreferenceHelper prefsHelper;
    FingerPrintHelper fingerPrintHelper;
    Database dbHelper;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_settings);

        prefsHelper = PreferenceHelper.getInstance(this);
        dbHelper = Database.getInstance(this);

        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            fingerPrintHelper = FingerPrintHelper.getInstance(this);
        }

        Toolbar toolbar = findViewById(R.id.toolbar);
        TextView toolbarTitle = toolbar.findViewById(R.id.custom_toolbar_title);
        toolbarTitle.setText("Setting");
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        Switch switchQuickTouch = findViewById(R.id.switch_quick_touch_sign_in);
        switchQuickTouch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                dbHelper.setQuickSignInFor(prefsHelper.getUsername(), b);
            }
        });
        switchQuickTouch.setChecked(dbHelper.isQuickSignInEnable(prefsHelper.getUsername()));

        if (fingerPrintHelper == null || !fingerPrintHelper.checkFingerPrintHardwareAvailability(this)) {
            switchQuickTouch.setChecked(false);
            switchQuickTouch.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View view, MotionEvent motionEvent) {
                    AlertUtils.showToast(SettingsActivity.this, "Fingerprint feature not available in this device.");
                    return true;
                }
            });
        }

        final TextView tvSet = findViewById(R.id.tv_set);
        tvSet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(SettingsActivity.this);
                LayoutInflater inflater = getLayoutInflater();
                final View alertDialogView = inflater.inflate(R.layout.layout_set_timeout, null, false);

                final RadioButton radioNotSet = alertDialogView.findViewById(R.id.radio_not_set);
                final RadioButton radioOneMin = alertDialogView.findViewById(R.id.radio_one_min);
                final RadioButton radiofiveMin = alertDialogView.findViewById(R.id.radio_five_min);
                final RadioButton radioTenMin = alertDialogView.findViewById(R.id.radio_ten_min);
                final RadioButton radioThirtyMin = alertDialogView.findViewById(R.id.radio_thirty_min);

                radioNotSet.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        radioNotSet.setChecked(true);
                        radioOneMin.setChecked(false);
                        radiofiveMin.setChecked(false);
                        radioTenMin.setChecked(false);
                        radioThirtyMin.setChecked(false);
                    }
                });

                radioOneMin.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        radioOneMin.setChecked(true);
                        radioNotSet.setChecked(false);
                        radiofiveMin.setChecked(false);
                        radioTenMin.setChecked(false);
                        radioThirtyMin.setChecked(false);
                    }
                });

                radiofiveMin.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        radiofiveMin.setChecked(true);
                        radioOneMin.setChecked(false);
                        radioNotSet.setChecked(false);
                        radioTenMin.setChecked(false);
                        radioThirtyMin.setChecked(false);
                    }
                });

                radioTenMin.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        radioTenMin.setChecked(true);
                        radioOneMin.setChecked(false);
                        radiofiveMin.setChecked(false);
                        radioNotSet.setChecked(false);
                        radioThirtyMin.setChecked(false);
                    }
                });

                radioThirtyMin.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        radioThirtyMin.setChecked(true);
                        radioOneMin.setChecked(false);
                        radiofiveMin.setChecked(false);
                        radioTenMin.setChecked(false);
                        radioNotSet.setChecked(false);
                    }
                });

                int timeInSec = prefsHelper.getInt(PreferenceHelper.TIME_OUT_IN_SEC, 0);
                if (timeInSec == 0) {
                    radioNotSet.setChecked(true);
                } else if (timeInSec == 60) {
                    radioOneMin.setChecked(true);
                } else if (timeInSec == 300) {
                    radiofiveMin.setChecked(true);
                } else if (timeInSec == 600) {
                    radioTenMin.setChecked(true);
                } else if (timeInSec == 1800) {
                    radioThirtyMin.setChecked(true);
                }

                Button btnCancel, btnSave;
                btnCancel = alertDialogView.findViewById(R.id.btn_cancel);
                btnSave = alertDialogView.findViewById(R.id.btn_save);

                builder.setView(alertDialogView);
                final AlertDialog alertDialog = builder.create();
                alertDialog.show();

                btnCancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        alertDialog.dismiss();
                    }
                });

                btnSave.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        int timeInSec = 0;
                        if (radioNotSet.isChecked()) {
                            tvSet.setText("Not Set");
                            timeInSec = 0;
                        } else if (radioOneMin.isChecked()) {
                            tvSet.setText("1 Min");
                            timeInSec = 60;
                        } else if (radiofiveMin.isChecked()) {
                            tvSet.setText("5 Min");
                            timeInSec = 300;
                        } else if (radioTenMin.isChecked()) {
                            tvSet.setText("10 Min");
                            timeInSec = 600;
                        } else if (radioThirtyMin.isChecked()) {
                            tvSet.setText("30 Min");
                            timeInSec = 1800;
                        }
                        prefsHelper.edit().putInt(PreferenceHelper.TIME_OUT_IN_SEC, timeInSec).apply();
                        alertDialog.dismiss();
                        resetDisconnectTimer();
                    }
                });
            }
        });


        int timeInSec = prefsHelper.getInt(PreferenceHelper.TIME_OUT_IN_SEC, 0);
        if (timeInSec == 0) {
            tvSet.setText("Not Set");
        } else if (timeInSec == 60) {
            tvSet.setText("1 Min");
        } else if (timeInSec == 300) {
            tvSet.setText("5 Min");
        } else if (timeInSec == 600) {
            tvSet.setText("10 Min");
        } else if (timeInSec == 1800) {
            tvSet.setText("30 Min");
        }
    }
}
