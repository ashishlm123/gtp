package com.emts.gtp.activity;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.emts.gtp.Api;
import com.emts.gtp.R;
import com.emts.gtp.adapter.ProductAdapter;
import com.emts.gtp.helper.AlertUtils;
import com.emts.gtp.helper.Logger;
import com.emts.gtp.helper.NetworkUtils;
import com.emts.gtp.helper.VolleyHelper;
import com.emts.gtp.model.Product;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

public class UnfulfillPOFilterActivity extends BaseActivity {
    ImageView tick1, tick2, tick3, tick4, tick5, tick6;
    RecyclerView rvProductListings;
    ArrayList<Product> producLists;
    ProductAdapter productAdapter;
    TextView tvProductError;
    Calendar startDateCalendar, endDateCalendar, calendarNow;

    public static final int HIGH_To_LOW = 1;
    public static final int LOW_TO_HIGH = 2;
    public static final int NONE = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_unfulfilled_po_filter);
        calendarNow = Calendar.getInstance();

        Button btnApply = findViewById(R.id.btn_apply);
        Button btnReset = findViewById(R.id.btn_reset);
        ImageView btnClose = findViewById(R.id.ico_close);

        rvProductListings = findViewById(R.id.rv_product_lists);
        tvProductError = findViewById(R.id.tv_product_error);

        Intent intent = getIntent();
        producLists = (ArrayList<Product>) intent.getSerializableExtra("productList");

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(UnfulfillPOFilterActivity.this);
        rvProductListings.setLayoutManager(linearLayoutManager);
        productAdapter = new ProductAdapter(UnfulfillPOFilterActivity.this, producLists);
        rvProductListings.setAdapter(productAdapter);
        rvProductListings.setVisibility(View.VISIBLE);
        tvProductError.setVisibility(View.GONE);

//        if (NetworkUtils.isInNetwork(UnfulfillPOFilterActivity.this)) {
//            getProductLists();
//        } else {
//            rvProductListings.setVisibility(View.GONE);
//            tvProductError.setText(getResources().getString(R.string.error_no_internet));
//            tvProductError.setVisibility(View.VISIBLE);
//        }

//        if (producLists.size() > 1) {
//            rvProductListings.setVisibility(View.VISIBLE);
//            rvProductListings.setAdapter(productAdapter);
//            tvProductError.setVisibility(View.GONE);
//        } else {
//            rvProductListings.setVisibility(View.GONE);
//            tvProductError.setText("Unknown Error Occured!!!");
//            tvProductError.setVisibility(View.VISIBLE);
//        }

        final CheckBox cbJList, cbBList;
        cbJList = findViewById(R.id.cb_j_list);
        cbBList = findViewById(R.id.cb_b_list);

        final TextView tvStartDate, tvExpiryDate;
        tvStartDate = findViewById(R.id.tv_start_date);
        tvExpiryDate = findViewById(R.id.tv_end_date);

        tvStartDate.setHint(calendarNow.get(Calendar.YEAR) + "-" + String.format("%02d", (calendarNow.get(Calendar.MONTH) + 1))
                + "-" + String.format("%02d", (calendarNow.get(Calendar.DAY_OF_MONTH))));

        tvExpiryDate.setHint(calendarNow.get(Calendar.YEAR) + "-" + String.format("%02d", (calendarNow.get(Calendar.MONTH) + 1))
                + "-" + String.format("%02d", (calendarNow.get(Calendar.DAY_OF_MONTH))));

        startDateCalendar = Calendar.getInstance();
        endDateCalendar = Calendar.getInstance();
        final DatePickerDialog.OnDateSetListener startDate = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int day) {
                startDateCalendar.set(Calendar.YEAR, year);
                startDateCalendar.set(Calendar.MONTH, month);
                startDateCalendar.set(Calendar.DAY_OF_MONTH, day);
                tvStartDate.setText(year + "-" + String.format("%02d", (month + 1)) + "-" +
                        String.format("%02d", (day)));
            }
        };

        tvStartDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new DatePickerDialog(UnfulfillPOFilterActivity.this, startDate, startDateCalendar
                        .get(Calendar.YEAR), startDateCalendar.get(Calendar.MONTH),
                        startDateCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        final DatePickerDialog.OnDateSetListener expiryDate = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                endDateCalendar.set(Calendar.YEAR, year);
                endDateCalendar.set(Calendar.MONTH, month);
                endDateCalendar.set(Calendar.DAY_OF_MONTH, day);
                tvExpiryDate.setText(year + "-" + String.format("%02d", (month + 1)) + "-"
                        + String.format("%02d", (day)));
            }
        };

        tvExpiryDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new DatePickerDialog(UnfulfillPOFilterActivity.this, expiryDate, endDateCalendar
                        .get(Calendar.YEAR), endDateCalendar.get(Calendar.MONTH),
                        endDateCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        final Spinner spAging = findViewById(R.id.sp_aging);
        String[] agingArray = getResources().getStringArray(R.array.agingArray);
        ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<>(UnfulfillPOFilterActivity.this,
                R.layout.layout_textview, agingArray);
        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spAging.setAdapter(spinnerAdapter);

        final RelativeLayout layPoQty, layUnfulfilQty1, layOneTwoBalance;
        final ImageView btnView, btnView1, btnView2;
        final TextView tvSelectedOneTwoBalType, tvSelectedUnfulfillQtyType, tvSelectedPoQtyType;

        layPoQty = findViewById(R.id.lay_po_qty);
        layUnfulfilQty1 = findViewById(R.id.lay_unfulfill_qty_1);
        layOneTwoBalance = findViewById(R.id.lay_one_two_balance);

        final LinearLayout poQtyType, unfulfill1Qtytype, oneTwoBalanceType;
        poQtyType = findViewById(R.id.po_qty_type);
        unfulfill1Qtytype = findViewById(R.id.unfulfill_1_type);
        oneTwoBalanceType = findViewById(R.id.one_two_balance_type);

        btnView = findViewById(R.id.btn_view);
        btnView1 = findViewById(R.id.btn_view1);
        btnView2 = findViewById(R.id.btn_view2);

        tvSelectedPoQtyType = layPoQty.findViewById(R.id.selected_poqty_type);
        tvSelectedUnfulfillQtyType = layUnfulfilQty1.findViewById(R.id.tv_selected_unfulfillqty_type);
        tvSelectedOneTwoBalType = layOneTwoBalance.findViewById(R.id.tv_selected_one_two_bal_type);


        tick1 = poQtyType.findViewById(R.id.tick1);
        tick2 = poQtyType.findViewById(R.id.tick2);
        tick3 = unfulfill1Qtytype.findViewById(R.id.tick1);
        tick4 = unfulfill1Qtytype.findViewById(R.id.tick2);
        tick5 = oneTwoBalanceType.findViewById(R.id.tick1);
        tick6 = oneTwoBalanceType.findViewById(R.id.tick2);

        layPoQty.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                RelativeLayout layLowToHigh, layHighToLow;
                layLowToHigh = poQtyType.findViewById(R.id.low_to_high);
                layHighToLow = poQtyType.findViewById(R.id.high_to_low);

                if (poQtyType.getVisibility() == View.GONE) {
                    btnView.setImageResource(R.drawable.btn_viewless_arrow);
                    poQtyType.setVisibility(View.VISIBLE);

                } else {
                    btnView.setImageResource(R.drawable.btn_viewmore_arrow);
                    poQtyType.setVisibility(View.GONE);
                }

                layLowToHigh.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (tick1.getVisibility() == View.GONE) {
                            tick1.setVisibility(View.VISIBLE);
                            tvSelectedPoQtyType.setText("Low To High");
                            tvSelectedPoQtyType.setVisibility(View.VISIBLE);
                            tick2.setVisibility(View.GONE);
                        } else {
                            tick1.setVisibility(View.GONE);
                            tvSelectedPoQtyType.setVisibility(View.GONE);
                        }
                    }
                });

                layHighToLow.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (tick2.getVisibility() == View.GONE) {
                            tick2.setVisibility(View.VISIBLE);
                            tvSelectedPoQtyType.setText("High To Low");
                            tvSelectedPoQtyType.setVisibility(View.VISIBLE);
                            tick1.setVisibility(View.GONE);
                        } else {
                            tick2.setVisibility(View.GONE);
                            tvSelectedPoQtyType.setVisibility(View.GONE);
                        }
                    }
                });
            }
        });

        layUnfulfilQty1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                RelativeLayout layLowToHigh, layHighToLow;

                layLowToHigh = unfulfill1Qtytype.findViewById(R.id.low_to_high);
                layHighToLow = unfulfill1Qtytype.findViewById(R.id.high_to_low);
                if (unfulfill1Qtytype.getVisibility() == View.GONE) {
                    btnView1.setImageResource(R.drawable.btn_viewless_arrow);
                    unfulfill1Qtytype.setVisibility(View.VISIBLE);
                } else {
                    btnView1.setImageResource(R.drawable.btn_viewmore_arrow);
                    unfulfill1Qtytype.setVisibility(View.GONE);
                }

                layLowToHigh.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (tick3.getVisibility() == View.GONE) {
                            tick3.setVisibility(View.VISIBLE);
                            tvSelectedUnfulfillQtyType.setText("Low To High");
                            tvSelectedUnfulfillQtyType.setVisibility(View.VISIBLE);
                            tick4.setVisibility(View.GONE);
                        } else {
                            tick3.setVisibility(View.GONE);
                            tvSelectedUnfulfillQtyType.setVisibility(View.GONE);
                        }
                    }
                });

                layHighToLow.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (tick4.getVisibility() == View.GONE) {
                            tick4.setVisibility(View.VISIBLE);
                            tvSelectedUnfulfillQtyType.setText("High To Low");
                            tvSelectedUnfulfillQtyType.setVisibility(View.VISIBLE);
                            tick3.setVisibility(View.GONE);
                        } else {
                            tick4.setVisibility(View.GONE);
                            tvSelectedUnfulfillQtyType.setVisibility(View.GONE);
                        }
                    }
                });
            }
        });

        layOneTwoBalance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                RelativeLayout layLowToHigh, layHighToLow;

                layLowToHigh = oneTwoBalanceType.findViewById(R.id.low_to_high);
                layHighToLow = oneTwoBalanceType.findViewById(R.id.high_to_low);

                if (oneTwoBalanceType.getVisibility() == View.GONE) {
                    btnView2.setImageResource(R.drawable.btn_viewless_arrow);
                    oneTwoBalanceType.setVisibility(View.VISIBLE);
                } else {
                    btnView2.setImageResource(R.drawable.btn_viewmore_arrow);
                    oneTwoBalanceType.setVisibility(View.GONE);
                }

                layLowToHigh.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (tick5.getVisibility() == View.GONE) {
                            tick5.setVisibility(View.VISIBLE);
                            tvSelectedOneTwoBalType.setText("Low To High");
                            tvSelectedOneTwoBalType.setVisibility(View.VISIBLE);
                            tick6.setVisibility(View.GONE);
                        } else {
                            tick5.setVisibility(View.GONE);
                            tvSelectedOneTwoBalType.setVisibility(View.GONE);
                        }
                    }
                });

                layHighToLow.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (tick6.getVisibility() == View.GONE) {
                            tick6.setVisibility(View.VISIBLE);
                            tvSelectedOneTwoBalType.setText("High To Low");
                            tvSelectedOneTwoBalType.setVisibility(View.VISIBLE);
                            tick5.setVisibility(View.GONE);
                        } else {
                            tick6.setVisibility(View.GONE);
                            tvSelectedOneTwoBalType.setVisibility(View.GONE);
                        }
                    }
                });
            }
        });

        btnApply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (tvStartDate.getText().toString().trim().length() > 0 && tvExpiryDate.getText().toString().trim().length() > 0) {
                    Logger.e("sDate:" + startDateCalendar.getTime(), "eDate:" + endDateCalendar.getTime());
                    if (!startDateCalendar.getTime().before(endDateCalendar.getTime())) {
                        AlertUtils.showToast(getApplicationContext(), "End date must be after start date");
                        return;
                    }
                }


                Intent intent = new Intent();
                intent.putExtra("productList", producLists);
                intent.putExtra("jList", cbJList.isChecked());
                intent.putExtra("bList", cbBList.isChecked());
                intent.putExtra("startDate", tvStartDate.getText().toString().trim());
                intent.putExtra("endDate", tvExpiryDate.getText().toString().trim());
                intent.putExtra("agingPos", spAging.getSelectedItemPosition());
                int shortPo = NONE;
                if (tick1.getVisibility() == View.VISIBLE) {
                    shortPo = LOW_TO_HIGH;
                } else if (tick2.getVisibility() == View.VISIBLE) {
                    shortPo = HIGH_To_LOW;
                }
                intent.putExtra("shortPo", shortPo);
                int shortUnfulfill = NONE;
                if (tick3.getVisibility() == View.VISIBLE) {
                    shortUnfulfill = LOW_TO_HIGH;
                } else if (tick4.getVisibility() == View.VISIBLE) {
                    shortUnfulfill = HIGH_To_LOW;
                }
                intent.putExtra("shortUnfulfill", shortUnfulfill);
                int short1_2 = NONE;
                if (tick5.getVisibility() == View.VISIBLE) {
                    short1_2 = LOW_TO_HIGH;
                } else if (tick6.getVisibility() == View.VISIBLE) {
                    short1_2 = HIGH_To_LOW;
                }
                intent.putExtra("short1_2", short1_2);


                setResult(RESULT_OK, intent);
                UnfulfillPOFilterActivity.this.finish();
            }
        });

        btnReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                for (int i = 0; i < producLists.size(); i++) {
                    producLists.get(i).setSelected(false);
                }
                productAdapter.notifyDataSetChanged();

                cbBList.setChecked(false);
                cbJList.setChecked(false);

                tvStartDate.setText("");
                tvStartDate.setHint(calendarNow.get(Calendar.YEAR) + "-"
                        + String.format("%02d", (calendarNow.get(Calendar.MONTH) + 1))
                        + "-" + String.format("%02d", (calendarNow.get(Calendar.DAY_OF_MONTH))));

                tvExpiryDate.setText("");
                tvExpiryDate.setHint(calendarNow.get(Calendar.YEAR) + "-"
                        + String.format("%02d", (calendarNow.get(Calendar.MONTH) + 1))
                        + "-" + String.format("%02d", (calendarNow.get(Calendar.DAY_OF_MONTH))));

                spAging.setSelection(0);

                poQtyType.setVisibility(View.GONE);
                btnView.setImageResource(R.drawable.btn_viewmore_arrow);
                tvSelectedPoQtyType.setText("");
                unfulfill1Qtytype.setVisibility(View.GONE);
                btnView1.setImageResource(R.drawable.btn_viewmore_arrow);
                tvSelectedUnfulfillQtyType.setText("");
                oneTwoBalanceType.setVisibility(View.GONE);
                btnView2.setImageResource(R.drawable.btn_viewmore_arrow);
                tvSelectedOneTwoBalType.setText("");
                tick1.setVisibility(View.GONE);
                tick2.setVisibility(View.GONE);
                tick3.setVisibility(View.GONE);
                tick4.setVisibility(View.GONE);
                tick5.setVisibility(View.GONE);
                tick6.setVisibility(View.GONE);

                setResult(RESULT_CANCELED);
            }
        });

        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                UnfulfillPOFilterActivity.this.finish();
            }
        });

        //pre fill
        cbJList.setChecked(intent.getBooleanExtra("jList", false));
        cbBList.setChecked(intent.getBooleanExtra("bList", false));

        SimpleDateFormat sD1 = new SimpleDateFormat("yyyy-MM-dd");
        String sStart = intent.getStringExtra("startDate");
        try {
            startDateCalendar.setTime(sD1.parse(sStart));
            tvStartDate.setText(sStart);
        } catch (ParseException e) {
        } catch (NullPointerException e) {
        }
        String eStart = intent.getStringExtra("endDate");
        try {
            endDateCalendar.setTime(sD1.parse(eStart));
            tvExpiryDate.setText(eStart);
        } catch (ParseException e) {
        } catch (NullPointerException e) {
        }
        spAging.setSelection(intent.getIntExtra("agingPos", 0));
        int shortPo = intent.getIntExtra("shortPo", UnfulfillPOFilterActivity.NONE);
        if (shortPo == UnfulfillPOFilterActivity.HIGH_To_LOW) {
            tick1.setVisibility(View.VISIBLE);
            tick2.setVisibility(View.GONE);
        } else if (shortPo == UnfulfillPOFilterActivity.LOW_TO_HIGH) {
            tick2.setVisibility(View.VISIBLE);
            tick1.setVisibility(View.GONE);
        } else {
            tick2.setVisibility(View.GONE);
            tick1.setVisibility(View.GONE);
        }
        int shortUnfulfill = intent.getIntExtra("shortUnfulfill", UnfulfillPOFilterActivity.NONE);
        if (shortUnfulfill == UnfulfillPOFilterActivity.HIGH_To_LOW) {
            tick2.setVisibility(View.VISIBLE);
            tick4.setVisibility(View.GONE);
        } else if (shortUnfulfill == UnfulfillPOFilterActivity.LOW_TO_HIGH) {
            tick4.setVisibility(View.VISIBLE);
            tick3.setVisibility(View.GONE);
        } else {
            tick4.setVisibility(View.GONE);
            tick3.setVisibility(View.GONE);
        }
        int short1_2 = intent.getIntExtra("short1_2", UnfulfillPOFilterActivity.NONE);
        if (short1_2 == UnfulfillPOFilterActivity.HIGH_To_LOW) {
            tick5.setVisibility(View.VISIBLE);
            tick6.setVisibility(View.GONE);
        } else if (short1_2 == UnfulfillPOFilterActivity.LOW_TO_HIGH) {
            tick6.setVisibility(View.VISIBLE);
            tick5.setVisibility(View.GONE);
        } else {
            tick6.setVisibility(View.GONE);
            tick5.setVisibility(View.GONE);
        }
    }

//    private void getProductLists() {
//        VolleyHelper vHelper = VolleyHelper.getInstance(this);
//        HashMap<String, String> postParams = vHelper.getPostParams();
//        vHelper.addVolleyRequestListeners(Api.getInstance().productList, Request.Method.GET, postParams,
//                new VolleyHelper.VolleyHelperInterface() {
//                    @Override
//                    public void onSuccess(String response) {
//                        try {
//                            JSONObject res = new JSONObject(response);
//                            JSONArray responseArray = res.getJSONArray("ResponseData");
//                            if (responseArray.length() >= 1) {
//                                JSONObject response1 = responseArray.getJSONObject(0);
//                                JSONObject responseDetails = response1.getJSONObject("Details");
//                                if (responseDetails.getString("appCode").equals(Api.APP_STATUS_OK)) {
//                                    JSONObject response2 = responseArray.getJSONObject(1);
//                                    JSONArray productsArray = response2.getJSONArray("Products");
//                                    for (int i = 0; i < productsArray.length(); i++) {
//                                        JSONObject eachProduct = productsArray.getJSONObject(i);
//                                        Product product = new Product();
//                                        product.setpId(eachProduct.getString("product_id"));
//                                        product.setpName(eachProduct.getString("product_name"));
//                                        producLists.add(product);
//                                    }
//                                    productAdapter.notifyDataSetChanged();
//                                    rvProductListings.setVisibility(View.VISIBLE);
//                                    tvProductError.setVisibility(View.GONE);
//                                }
//                            }
//                        } catch (JSONException e) {
//                            rvProductListings.setVisibility(View.GONE);
//                            tvProductError.setText("Unknown Error Occured...");
//                            tvProductError.setVisibility(View.VISIBLE);
//                            Logger.e("getProductLists json ex", e.getMessage());
//                        }
//                    }
//
//                    @Override
//                    public void onError(String errorResponse, VolleyError volleyError) {
//                        String errorMsg = "Unexpected error !!! Please try again with internet access.";
//                        try {
//                            JSONObject errorObj = new JSONObject(errorResponse);
//                            errorMsg = errorObj.getJSONObject("Details").getString("appCodeMessage");
//                        } catch (Exception e) {
//                            Logger.e("getProductLists error res", e.getMessage() + " ");
//                        }
//                        Logger.e("error message", errorMsg);
//                    }
//                }, "getProductLists");
//    }
}
