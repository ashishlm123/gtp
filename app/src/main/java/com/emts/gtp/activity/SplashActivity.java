package com.emts.gtp.activity;

import android.app.AlertDialog;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.annotation.RequiresApi;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.WindowManager;
import android.widget.RemoteViews;
import android.widget.Toast;

import com.emts.gtp.Database;
import com.emts.gtp.LockScreenWidget;
import com.emts.gtp.MainActivity;
import com.emts.gtp.R;
import com.emts.gtp.fcm.MyFireBaseMessagingService;
import com.emts.gtp.helper.PreferenceHelper;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.FirebaseApp;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.firebase.messaging.FirebaseMessagingService;

public class SplashActivity extends AppCompatActivity {
    private static final int CODE_DRAW_OVER_OTHER_APP_PERMISSION = 293;
    private int splash_timer = 3000;
    PreferenceHelper helper;
    Database dbHelper;

    Handler mHandler;
    Runnable mRunnable = new Runnable() {
        @Override
        public void run() {
            //TODO bypass other redirect to Main page
//            if (true) {
//                helper.edit().putString(PreferenceHelper.SESSION_PIN_CODE, "123456").apply();
//                helper.setUsername("tiseno");
//                helper.setPassword("ace12345");
//                //helper.setUsername("joeystudent0001");
//                //helper.setPassword("1111");
//                startActivity(new Intent(getApplicationContext(), MainActivity.class));
//                SplashActivity.this.finish();
//                return;
//            }

            if (helper.getBoolean(PreferenceHelper.FIRST_TNC, true)) {
                startActivity(new Intent(getApplicationContext(), TermsAndConditionsActivity.class));
            } else if (helper.getBoolean(PreferenceHelper.FIRST_GET_STARTED, true)) {
                startActivity(new Intent(getApplicationContext(), ImportantNoteActivity.class));
            } else if (helper.isSessionPinSet()) {
                if (dbHelper.isAccountAdded()) {
                    startActivity(new Intent(getApplicationContext(), AccountListActivity.class));
                } else {
                    startActivity(new Intent(getApplicationContext(), SetUpAccountActivity.class));
                }
            } else {
                startActivity(new Intent(getApplicationContext(), SetUpSessionPinCode.class));
            }
            SplashActivity.this.finish();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FirebaseApp.initializeApp(this);

        helper = PreferenceHelper.getInstance(this);
        //if logged in and click on notification go for notification not login
        if (helper.getBoolean("isLoggedIn", false)
                && getIntent().getBooleanExtra("isNotification", false)) {
            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
            intent.putExtra("isNotification", true);
            startActivity(intent);
            SplashActivity.this.finish();
            return;
        }

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        setContentView(R.layout.activity_splash);

        dbHelper = Database.getInstance(this);
        mHandler = new Handler();

//        drawOverAppPremission();
        initializeService();

        //update devicePush toke if not
        if (!PreferenceHelper.getInstance(this).getBoolean(PreferenceHelper.FCM_TOKEN_UPDATED, false)) {
            FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(new OnSuccessListener<InstanceIdResult>() {
                @Override
                public void onSuccess(InstanceIdResult instanceIdResult) {
                    String deviceToken = instanceIdResult.getToken();
                    MyFireBaseMessagingService.registerDeviceTokenToServer(deviceToken, SplashActivity.this);
                }
            });
        }

        //for click on notificaiton should go to notification frag not splash and login
        helper.edit().putBoolean("isLoggedIn", false).apply();
//        createNotification(this, "test noti");
    }

    @Override
    protected void onResume() {
        super.onResume();
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        mHandler.postDelayed(mRunnable, splash_timer);
    }

    @Override
    protected void onPause() {
        super.onPause();
        mHandler.removeCallbacks(mRunnable);
    }

    @Override
    protected void onStop() {
        super.onStop();
        mHandler.removeCallbacks(mRunnable);
    }

    private void drawOverAppPremission() {
        //Check if the application has draw over other apps permission or not?
        //This permission is by default available for API<23. But for API > 23
        //you have to ask for the permission in runtime.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && !Settings.canDrawOverlays(this)) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("Lock Screen Live Price Update");
            builder.setMessage("For the feature of live price update over lock screen you need to provide permission for the app. Please give draw permission.");
            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @RequiresApi(api = Build.VERSION_CODES.M)
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    //If the draw over permission is not available open the settings screen
                    //to grant the permission.
                    Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION,
                            Uri.parse("package:" + getPackageName()));
                    startActivityForResult(intent, CODE_DRAW_OVER_OTHER_APP_PERMISSION);
                }
            });
            builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                }
            });
            builder.show();
        } else {
            initializeService();
        }
    }

    /**
     * Set and initialize the view elements.
     */
    private void initializeService() {
//        createNotification(this, "124-421");
        if (Build.VERSION.SDK_INT > 25) {
            startForegroundService(new Intent(getApplicationContext(), LockScreenWidget.class));
        } else {
            startService(new Intent(getApplicationContext(), LockScreenWidget.class));
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == CODE_DRAW_OVER_OTHER_APP_PERMISSION) {

            //Check if the permission is granted or not.
            if (resultCode == RESULT_OK) {
                initializeService();
            } else { //Permission is not available
                Toast.makeText(this,
                        "Draw over other app permission not available.",
                        Toast.LENGTH_SHORT).show();
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public void onBackPressed() {
        mHandler.removeCallbacks(mRunnable);
        super.onBackPressed();
    }

    //custom layout notification test
    public void createNotification(Context context, String body) {
        Intent notificationIntent;

        int id = (int) System.currentTimeMillis();

        NotificationCompat.BigPictureStyle notifystyle = new NotificationCompat.BigPictureStyle();
//        notifystyle.bigPicture(bitmap);
        RemoteViews contentView = new RemoteViews(context.getPackageName(), R.layout.notification_live_feed);
//        contentView.setImageViewBitmap(R.id.image, BitmapFactory.decodeResource(getResource(), R.drawable.app));
        contentView.setTextViewText(R.id.tv_noti_ace_buy, body);
        contentView.setTextViewText(R.id.tv_noti_ace_sell, body);

        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context)
                .setSmallIcon(R.drawable.app_logo)
                .setStyle(notifystyle)
                .setCustomBigContentView(contentView)
                .setContentText(body);
        NotificationManager mNotificationManager = (NotificationManager) context
                .getSystemService(Context.NOTIFICATION_SERVICE);

        notificationIntent = new Intent(context, SplashActivity.class);
        notificationIntent.putExtra("isNotification", true);
        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        PendingIntent contentIntent = PendingIntent.getActivity(context, id, notificationIntent, 0);

        Notification notification = mBuilder.build();
        notification.contentIntent = contentIntent;
        notification.flags |= Notification.FLAG_NO_CLEAR; //Do not clear the notification
        notification.defaults |= Notification.DEFAULT_LIGHTS; // LED
        notification.defaults |= Notification.DEFAULT_VIBRATE; //Vibration
        notification.defaults |= Notification.DEFAULT_SOUND; // Sound

        mNotificationManager.notify(123654, notification);
    }
}
