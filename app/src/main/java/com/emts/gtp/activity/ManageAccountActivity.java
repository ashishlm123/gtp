package com.emts.gtp.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.emts.gtp.Database;
import com.emts.gtp.R;
import com.emts.gtp.adapter.AccountAdapter;
import com.emts.gtp.helper.AlertUtils;
import com.emts.gtp.model.Account;

import java.util.ArrayList;

public class ManageAccountActivity extends AppCompatActivity {
    ArrayList<Account> acList;
    AccountAdapter accountAdapter;
    Database dbHelper;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        dbHelper = Database.getInstance(this);
        setContentView(R.layout.activity_manage_account);

        Toolbar toolbar = findViewById(R.id.toolbar);
        final TextView toolbarTitle = toolbar.findViewById(R.id.custom_toolbar_title);
        toolbarTitle.setText("Manage Account");
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        acList = new ArrayList<>();
        acList = dbHelper.getAllAccounts();

        accountAdapter = new AccountAdapter(ManageAccountActivity.this, acList);
        RecyclerView recyclerView = findViewById(R.id.account_list);
        recyclerView.setAdapter(accountAdapter);
        accountAdapter.setSignIn(false);
        LinearLayoutManager layoutManager = new LinearLayoutManager(ManageAccountActivity.this);
        recyclerView.setLayoutManager(layoutManager);

        TextView tvAddAccount = findViewById(R.id.tv_add_ac);
        tvAddAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(ManageAccountActivity.this);
                LayoutInflater inflater = getLayoutInflater();
                final View alertDialogView = inflater.inflate(R.layout.alert_add_new_account, null, false);

                Button btnCancel = alertDialogView.findViewById(R.id.btn_cancel);
                Button btnSave = alertDialogView.findViewById(R.id.btn_save);


                builder.setView(alertDialogView);
                final AlertDialog alertDialog = builder.create();
                alertDialog.show();

                btnSave.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        EditText edtScreenName = alertDialogView.findViewById(R.id.edt_screen_name);

                        if (edtScreenName.getText().toString().trim().length() > 0) {
                            if (dbHelper.isAccountAdded(edtScreenName.getText().toString().trim())) {
                                AlertUtils.showAppAlert(ManageAccountActivity.this, R.drawable.icon_not_accept,
                                        "Screen Name already added", "OK", null);
                                return;
                            }
                            if (!dbHelper.addAccount(edtScreenName.getText().toString().trim())) {
                                AlertUtils.showToast(getApplicationContext(), "Cannot add account");
                                return;
                            }
                            Account account = new Account();
                            account.setAcName(edtScreenName.getText().toString().trim());
                            acList.add(account);
                            accountAdapter.notifyItemInserted(acList.size() - 1);
                            AlertUtils.showToast(ManageAccountActivity.this,
                                    "You have successfully added a new account");
                            alertDialog.dismiss();
                            AlertUtils.hideInputMethod(ManageAccountActivity.this, view);
                        }
                    }
                });

                btnCancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        alertDialog.dismiss();
                        AlertUtils.hideInputMethod(ManageAccountActivity.this, view);
                    }
                });
            }
        });
    }


}
