package com.emts.gtp.activity;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.TextView;

import com.emts.gtp.R;
import com.emts.gtp.helper.AlertUtils;
import com.emts.gtp.helper.Logger;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class TradebookFutureOrderFilterActivity extends BaseActivity {
    Calendar startDateCalendar, endDateCalendar, calendarNow;
    String status = "";

    Date sDate, eDate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_future_order_filter);

        calendarNow = Calendar.getInstance();

        final Intent intent = getIntent();
        String getStatus = intent.getStringExtra("status");
        String getStartDate = intent.getStringExtra("startDate");
        String getEndDate = intent.getStringExtra("endDate");

        Button btnApply = findViewById(R.id.btn_apply);
        Button btnReset = findViewById(R.id.btn_reset);
        ImageView btnClose = findViewById(R.id.ico_close);

        final CheckBox cbQueued, cbExpired, cbMatched, cbCancelled, cbPushed;
        cbQueued = findViewById(R.id.cb_queued);
        cbExpired = findViewById(R.id.cb_expired);
        cbMatched = findViewById(R.id.cb_matched);
        cbCancelled = findViewById(R.id.cb_cancelled);
        cbPushed = findViewById(R.id.cb_pushed);

        if (getStatus.contains("Queued")) {
            cbQueued.setChecked(true);
        }
        if (getStatus.contains("Expired")) {
            cbExpired.setChecked(true);
        }
        if (getStatus.contains("Matched")) {
            cbMatched.setChecked(true);
        }
        if (getStatus.contains("Cancelled")) {
            cbCancelled.setChecked(true);
        }
        if (getStatus.contains("Pushed")) {
            cbPushed.setChecked(true);
        }
        final TextView tvStartDate, tvExpiryDate;
        tvStartDate = findViewById(R.id.tv_start_date);
        tvExpiryDate = findViewById(R.id.tv_end_date);

        tvStartDate.setHint(calendarNow.get(Calendar.YEAR) + "-" + String.format("%02d", (calendarNow.get(Calendar.MONTH) + 1))
                + "-" + String.format("%02d", (calendarNow.get(Calendar.DAY_OF_MONTH))));

        tvExpiryDate.setHint(calendarNow.get(Calendar.YEAR) + "-" + String.format("%02d", (calendarNow.get(Calendar.MONTH) + 1))
                + "-" + String.format("%02d", (calendarNow.get(Calendar.DAY_OF_MONTH))));

        startDateCalendar = Calendar.getInstance();
        startDateCalendar.set(Calendar.HOUR_OF_DAY, 0);
        startDateCalendar.set(Calendar.MINUTE, 0);
        startDateCalendar.set(Calendar.SECOND, 0);
        endDateCalendar = Calendar.getInstance();
        endDateCalendar.set(Calendar.HOUR_OF_DAY, 0);
        endDateCalendar.set(Calendar.MINUTE, 0);
        endDateCalendar.set(Calendar.SECOND, 0);
        final DatePickerDialog.OnDateSetListener startDate = new DatePickerDialog.OnDateSetListener() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onDateSet(DatePicker view, int year, int month, int day) {
                startDateCalendar.set(Calendar.YEAR, year);
                startDateCalendar.set(Calendar.MONTH, month);
                startDateCalendar.set(Calendar.DAY_OF_MONTH, day);
                startDateCalendar.set(Calendar.HOUR_OF_DAY, 0);
                startDateCalendar.set(Calendar.MINUTE, 0);
                startDateCalendar.set(Calendar.SECOND, 0);
                tvStartDate.setText(year + "-" + String.format("%02d", (month + 1)) + "-" +
                        String.format("%02d", (day)));
            }
        };

        tvStartDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new DatePickerDialog(TradebookFutureOrderFilterActivity.this, startDate, startDateCalendar
                        .get(Calendar.YEAR), startDateCalendar.get(Calendar.MONTH),
                        startDateCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        final DatePickerDialog.OnDateSetListener expiryDate = new DatePickerDialog.OnDateSetListener() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                endDateCalendar.set(Calendar.YEAR, year);
                endDateCalendar.set(Calendar.MONTH, month);
                endDateCalendar.set(Calendar.DAY_OF_MONTH, day);
                endDateCalendar.set(Calendar.HOUR_OF_DAY, 0);
                endDateCalendar.set(Calendar.MINUTE, 0);
                endDateCalendar.set(Calendar.SECOND, 0);
                tvExpiryDate.setText(year + "-" + String.format("%02d", (month + 1)) + "-"
                        + String.format("%02d", (day)));
            }
        };

        tvExpiryDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new DatePickerDialog(TradebookFutureOrderFilterActivity.this, expiryDate, endDateCalendar
                        .get(Calendar.YEAR), endDateCalendar.get(Calendar.MONTH),
                        endDateCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        @SuppressLint("SimpleDateFormat") SimpleDateFormat sD1 = new SimpleDateFormat("yyyy-MM-dd");
        try {
            sDate = sD1.parse(getStartDate);
            startDateCalendar.setTime(sDate);
            tvStartDate.setText(getStartDate);
        } catch (ParseException e) {
            Logger.e("sDate parse ex", e.getMessage());
        }

        try {
            eDate = sD1.parse(getEndDate);
            endDateCalendar.setTime(eDate);
            tvExpiryDate.setText(getEndDate);
        } catch (ParseException e) {
            Logger.e("eDate parse ex", e.getMessage());
        }

        btnApply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (tvStartDate.getText().toString().trim().length() > 0 && tvExpiryDate.getText().toString().trim().length() > 0) {
                    Logger.e("sDate:" + startDateCalendar.getTime(), "eDate:" + endDateCalendar.getTime());
                    Logger.e("sDate 11111:" + startDateCalendar.getTime().getTime(),
                            "eDate 22222:" + endDateCalendar.getTime().getTime());
                    if (startDateCalendar.getTime().getTime() != endDateCalendar.getTime().getTime()) {
                        if (!startDateCalendar.getTime().before(endDateCalendar.getTime())) {
                            AlertUtils.showToast(getApplicationContext(), "End date must be after start date");
                            return;
                        }
                    }
                }

                if (cbCancelled.isChecked()) {
                    status = status + "Cancelled";
                }
                if (cbMatched.isChecked()) {
                    status = status + " " + "Matched";
                }
                if (cbExpired.isChecked()) {
                    status = status + " " + "Expired";
                }
                if (cbPushed.isChecked()) {
                    status = status + " " + "Pushed";
                }
                if (cbQueued.isChecked()) {
                    status = status + " " + "Queued";
                }

                Intent intent = new Intent();
                intent.putExtra("status", status);
                intent.putExtra("startDate", tvStartDate.getText().toString().trim());
                intent.putExtra("endDate", tvExpiryDate.getText().toString().trim());
                setResult(RESULT_OK, intent);
                TradebookFutureOrderFilterActivity.this.finish();
            }
        });

        btnReset.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onClick(View view) {
                cbCancelled.setChecked(false);
                cbExpired.setChecked(false);
                cbMatched.setChecked(false);
                cbPushed.setChecked(false);
                cbQueued.setChecked(false);

                tvStartDate.setText("");
                tvStartDate.setHint(calendarNow.get(Calendar.YEAR) + "-"
                        + String.format("%02d", (calendarNow.get(Calendar.MONTH) + 1))
                        + "-" + String.format("%02d", (calendarNow.get(Calendar.DAY_OF_MONTH))));

                tvExpiryDate.setText("");
                tvExpiryDate.setHint(calendarNow.get(Calendar.YEAR) + "-"
                        + String.format("%02d", (calendarNow.get(Calendar.MONTH) + 1))
                        + "-" + String.format("%02d", (calendarNow.get(Calendar.DAY_OF_MONTH))));

                setResult(RESULT_CANCELED);
            }
        });

        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TradebookFutureOrderFilterActivity.this.finish();
            }
        });
    }
}
