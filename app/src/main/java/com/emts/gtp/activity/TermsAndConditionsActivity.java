package com.emts.gtp.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.emts.gtp.R;
import com.emts.gtp.helper.PreferenceHelper;

public class TermsAndConditionsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_tnc);

        Toolbar toolbar = findViewById(R.id.toolbar);
        final TextView toolbarTitle = toolbar.findViewById(R.id.custom_toolbar_title);
        toolbarTitle.setText("Terms and Conditions");
        toolbar.setNavigationIcon(null);
    }

    public void acceptRejectTnC(View view) {
        if (view.getId() == R.id.btnCancel) {
            //nothing for now
        } else if (view.getId() == R.id.btnAccept) {
            PreferenceHelper.getInstance(this).edit().putBoolean(PreferenceHelper.FIRST_TNC, false).apply();
            startActivity(new Intent(getApplicationContext(), ImportantNoteActivity.class));
        }
        TermsAndConditionsActivity.this.finish();
    }
}
