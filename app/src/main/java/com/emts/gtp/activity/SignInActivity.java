package com.emts.gtp.activity;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.hardware.fingerprint.FingerprintManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.emts.gtp.Api;
import com.emts.gtp.Database;
import com.emts.gtp.FingerPrintHelper;
import com.emts.gtp.MainActivity;
import com.emts.gtp.R;
import com.emts.gtp.helper.AlertUtils;
import com.emts.gtp.helper.Logger;
import com.emts.gtp.helper.PreferenceHelper;
import com.emts.gtp.helper.VolleyHelper;
import com.emts.gtp.model.Account;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;

public class SignInActivity extends AppCompatActivity {
    FingerPrintHelper fingerPrintHelper;
    PreferenceHelper prefsHelper;
    Dialog alertFingerPrint;
    Database dbHelper;
    Account account;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        setContentView(R.layout.activity_sign_in);
        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        Intent intent = getIntent();
        if (intent == null)
            return;

        dbHelper = Database.getInstance(this);
        prefsHelper = PreferenceHelper.getInstance(this);

        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            // only for gingerbread and newer versions
            fingerPrintHelper = FingerPrintHelper.getInstance(this);
            fingerPrintHelper.setFingerPrintCallback(fingerPrintAuthCallback);
        }

        account = (Account) intent.getSerializableExtra("account");

        final TextView tvAccountName = findViewById(R.id.accountName);
        tvAccountName.setText(account.getAcName());
        final EditText etPassword = findViewById(R.id.etPassword);
        TextView tvFingerPrint = findViewById(R.id.tvFingerPrint);
        Button btnSignIn = findViewById(R.id.btnSignIn);
        btnSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (TextUtils.isEmpty(etPassword.getText().toString().trim())) {
                    AlertUtils.showSnack(SignInActivity.this, view, "Please enter password to proceed");
                } else {
                    prefsHelper.setUsername(account.getAcName());
//                    prefsHelper.setPassword(etPassword.getText().toString().trim());
                    dbHelper.updatePassword(account.getAcName(), etPassword.getText().toString().trim());
                    signInWithPasswordTask();
                    AlertUtils.hideInputMethod(SignInActivity.this, view);
                }
            }
        });
        TextView tvForgerPassword = findViewById(R.id.tvForgetPassword);
        tvForgerPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), GenerateOtpActivity.class);
                intent.putExtra("screen_name", tvAccountName.getText().toString().trim());
                startActivity(intent);
            }
        });

        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            if (!fingerPrintHelper.checkFingerPrintHardwareAvailability(this)) {
                tvFingerPrint.setVisibility(View.GONE);
            } else {
                tvFingerPrint.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
//                        if (account.isQuickSignInEnable()) {
//                            //start fingerprint auth
////                            fingerPrintHelper.startAuth();
//                            showFingerPrintAuthDialog();
//                            return;
//                        }
                        if (!fingerPrintHelper.checkFingerPrintAvailability(SignInActivity.this)) {
                            AlertUtils.showAppAlert(SignInActivity.this, R.drawable.icon_not_accept,
                                    "Please setup fingerprint from phone settings.",
                                    "OK", new AlertUtils.OnAlertButtonClickListener() {
                                        @Override
                                        public void onAlertButtonClick(boolean isPositiveButton) {
                                            try {
                                                startActivity(new Intent(android.provider.Settings.ACTION_SECURITY_SETTINGS));
                                            } catch (Exception e) {
                                                startActivity(new Intent(android.provider.Settings.ACTION_SETTINGS));
                                            }
                                        }
                                    });
                            return;
                        }
                        if (account.isQuickSignInEnable()) {
                            //start fingerprint auth
//                            fingerPrintHelper.startAuth();
                            showFingerPrintAuthDialog();
                        } else {
                            showEnableAlert();
                        }
                    }
                });
            }
        } else {
            tvFingerPrint.setVisibility(View.GONE);
        }

        if (fingerPrintHelper != null) {
            //show finger print dialog initially
            if (fingerPrintHelper.checkFingerPrintAvailability(SignInActivity.this) &&
                    account.isQuickSignInEnable()) {
                showFingerPrintAuthDialog();
            }
        }
    }

    public void startFingerPrintAuth() {
        if (fingerPrintHelper == null) return;
        if (fingerPrintHelper.checkFingerPrintAvailability(SignInActivity.this) &&
                account.isQuickSignInEnable()) {
            //start fingerprint auth
            fingerPrintHelper.startAuth();
        }
    }


    public void stopFingerPrintAuth() {
        if (fingerPrintHelper == null) return;
        fingerPrintHelper.stopAuth();
    }

    @Override
    protected void onResume() {
        super.onResume();
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        clearUserNamePassword();
        startFingerPrintAuth();
    }

    @Override
    protected void onStop() {
        super.onStop();
        stopFingerPrintAuth();
    }

    private void showEnableAlert() {
        AlertUtils.showAppAlert(SignInActivity.this, R.drawable.icon_not_accept,
                "Please sign in to enable sign in with quick touch/touch ID in Account Setting",
                "OK", new AlertUtils.OnAlertButtonClickListener() {
                    @Override
                    public void onAlertButtonClick(boolean isPositiveButton) {
                    }
                });
    }

    private void showFingerPrintAuthDialog() {
//        if (alertFingerPrint != null) {
//            alertFingerPrint.show();
//            return;
//        }
        AlertDialog.Builder mBuilder = new AlertDialog.Builder(SignInActivity.this);
        View view = LayoutInflater.from(SignInActivity.this).inflate(R.layout.alert_fingerprint,
                null, false);
        mBuilder.setView(view);
        alertFingerPrint = mBuilder.create();
        alertFingerPrint.show();
        Button btnCancel = alertFingerPrint.findViewById(R.id.cancel_button);
        alertFingerPrint.setCancelable(false);
        alertFingerPrint.setCanceledOnTouchOutside(false);
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertFingerPrint.dismiss();

                //stop fingerprint auth
//                fingerPrintHelper.stopAuth();
            }
        });
    }

    private void updateFingerPrintAlert(int icon, String status, int statusColor) {
        if (alertFingerPrint == null) return;
//        TextView tvFingerDesc = view.findViewById(R.id.fingerprint_description);
        ImageView imgFingerPrint = alertFingerPrint.findViewById(R.id.fingerprint_icon);
        imgFingerPrint.setImageResource(icon);
        TextView tvFingerStatus = alertFingerPrint.findViewById(R.id.fingerprint_status);
        tvFingerStatus.setText(status);
        tvFingerStatus.setTextColor(getResources().getColor(statusColor));
        if (!alertFingerPrint.isShowing()) {
            alertFingerPrint.show();
        }
    }


    private void signInWithPasswordTask() {
        final ProgressDialog pDialog = AlertUtils.showProgressDialog(SignInActivity.this, "Please wait...");

        VolleyHelper vHelper = VolleyHelper.getInstance(SignInActivity.this);
        HashMap<String, String> postMap = vHelper.getPostParams();

        //Use basic authentication using getBalanceApi !!!
        vHelper.addVolleyRequestListeners(Api.getInstance().getUserBalanceApi, Request.Method.GET,
                postMap, new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(final String response) {
                        pDialog.dismiss();
                        try {
                            final JSONObject resObj = new JSONObject(response);
                            JSONArray resDataArray = resObj.getJSONArray("ResponseData");
                            JSONObject appObj = resDataArray.getJSONObject(0).getJSONObject("Details");
                            try {
                                JSONObject userDetailObj = resDataArray.getJSONObject(1).getJSONObject("UserDetails");
                                String profilePic = userDetailObj.getString("image_url");
                                if (!TextUtils.isEmpty(profilePic) && !profilePic.equalsIgnoreCase("null")) {
                                    prefsHelper.edit().putString(PreferenceHelper.PROFILE_PIC, Api.getInstance().getHost()
                                            + profilePic).commit();
                                }
                            } catch (Exception e) {
                            }
                            if (appObj.getInt("appCode") == 1000) {
                                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP
                                        | Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(intent);
                            }
                        } catch (Exception e) {
                            Logger.e("signInWithPasswordTask res ex", e.getMessage() + " ");
                            AlertUtils.showAppAlert(SignInActivity.this, R.drawable.icon_not_accept,
                                    "Invalid Username or Password !!!", "OK", null);
                            clearUserNamePassword();
                        }
                    }

                    @Override
                    public void onError(String errorResponse, VolleyError volleyError) {
                        pDialog.dismiss();
                        String errorMsg = "Unexpected error !!! Please try again with internet access.";
                        try {
                            JSONObject errorObj = new JSONObject(errorResponse);
                            errorMsg = errorObj.getJSONObject("Details").getString("appCodeMessage");
                        } catch (Exception e) {
                            Logger.e("signInWithPasswordTask error res", e.getMessage() + " ");
                        }
                        AlertUtils.showAppAlert(SignInActivity.this, R.drawable.icon_not_accept,
                                "Invalid Username or Password !!!", "OK", null);
                        clearUserNamePassword();
                    }
                }, "signInWithPasswordTask");
    }

    private void clearUserNamePassword() {
        prefsHelper.setUsername("");
//        prefsHelper.setPassword("");
    }

    FingerPrintHelper.FingerPrintAuthCallback fingerPrintAuthCallback = new FingerPrintHelper.FingerPrintAuthCallback() {
        @Override
        public void onNoFingerPrintHardwareFound() {
            Logger.e("FingerPrintHelper", "onNoFingerPrintHardwareFound");
        }

        @Override
        public void onNoFingerPrintRegistered() {
            Logger.e("FingerPrintHelper", "onNoFingerPrintRegistered");
        }

        @Override
        public void onBelowMarshmallow() {
            Logger.e("FingerPrintHelper", "onBelowMarshmallow");
        }

        @Override
        public void onAuthSuccess(FingerprintManager.CryptoObject cryptoObject) {
            Logger.e("FingerPrintHelper", "onAuthSuccess : " + cryptoObject);
            updateFingerPrintAlert(R.drawable.btn_right_fingersensor, "Fingerprint recognized",
                    R.color.fingerPrintDialogSuccess);

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    prefsHelper.setUsername(account.getAcName());
                    Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP
                            | Intent.FLAG_ACTIVITY_NEW_TASK);
                    alertFingerPrint.dismiss();
                    startActivity(intent);
                }
            }, 1500);
        }

        @Override
        public void onAuthFailed(int errorCode, String errorMessage) {
            Logger.e("FingerPrintHelper", "Error Code: " + errorCode + " Error Message: " + errorMessage);
            switch (errorCode) {    //Parse the error code for recoverable/non recoverable error.
                case FingerPrintHelper.AuthErrorCodes.CANNOT_RECOGNIZE_ERROR:
                    //Cannot recognize the fingerprint scanned.
                    updateFingerPrintAlert(R.drawable.btn_not_recognized_fingersensor, "Fingerprint not recognized\nTry again",
                            R.color.fingerPrintDialogFail);
                    break;
                case FingerPrintHelper.AuthErrorCodes.NON_RECOVERABLE_ERROR:
                    //This is not recoverable error. Try other options for user authentication. like pin, password.
                    break;
                case FingerPrintHelper.AuthErrorCodes.RECOVERABLE_ERROR:
                    //Any recoverable error. Display message to the user.
                    break;
            }
        }
    };
}
