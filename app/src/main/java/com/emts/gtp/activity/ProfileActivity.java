package com.emts.gtp.activity;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.CursorLoader;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.emts.gtp.Api;
import com.emts.gtp.R;
import com.emts.gtp.helper.AlertUtils;
import com.emts.gtp.helper.Logger;
import com.emts.gtp.helper.NetworkUtils;
import com.emts.gtp.helper.PreferenceHelper;
import com.emts.gtp.helper.VolleyHelper;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;


public class ProfileActivity extends BaseActivity {
    MenuItem menuItem;
    public static final int CAMERA_CAPTURE_IMAGE_REQUEST_CODE = 9873;
    public static final int MEDIA_TYPE_IMAGE = 1;
    public static final int MEDIA_TYPE_VIDEO = 2;
    public static final int RESULT_LOAD_IMAGE = 123;
    public static final int RESULT_LOAD_VIDEO = 124;
    public static Uri fileUri = null;

    String selectedImagePath;
    ImageView profilePic;

    TextView tvScreenName, tvUserId;

    EditText etEmailAddress, etFirstName, etMiddleName, etLastName, etPhoneNo, etJobTitle;
    TextView tvErrEmail, tvErrTitle, tvErrFirstName, tvErrMiddleName, tvErrLastName, tvErrPhnNo, tvErrGender, tvErrJobTitle;

    Spinner spTitle, spGender;
    ArrayAdapter<String> genderAdapter, titleAdapter;
    ArrayList<String> titleLists;
    ArrayList<String> genderLists;

    LinearLayout btnHolder;
    Button btnChangePic, btnDeletePic, btnSave, btnCancel;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        //Handling of file exposure
        if (Build.VERSION.SDK_INT >= 24) {
            try {
                Method m = StrictMode.class.getMethod("disableDeathOnFileUriExposure");
                m.invoke(null);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        final Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        TextView toolbarTitle = toolbar.findViewById(R.id.custom_toolbar_title);
        toolbarTitle.setText("My Profile");

        tvScreenName = findViewById(R.id.tv_screen_name);
        tvUserId = findViewById(R.id.tv_user_id);
        tvErrEmail = findViewById(R.id.tv_err_email_address);
        tvErrTitle = findViewById(R.id.tv_err_title);
        tvErrFirstName = findViewById(R.id.tv_err_first_name);
        tvErrMiddleName = findViewById(R.id.tv_err_middle_name);
        tvErrLastName = findViewById(R.id.tv_err_last_name);
        tvErrPhnNo = findViewById(R.id.tv_err_phn_no);
        tvErrGender = findViewById(R.id.tv_err_gender);
        tvErrJobTitle = findViewById(R.id.tv_err_job_title);

        etEmailAddress = findViewById(R.id.et_email_address);
        etFirstName = findViewById(R.id.et_first_name);
        etMiddleName = findViewById(R.id.et_middle_name);
        etLastName = findViewById(R.id.et_last_name);
        etPhoneNo = findViewById(R.id.et_phn_no);
        etJobTitle = findViewById(R.id.et_job_title);

        spTitle = findViewById(R.id.sp_title);
        titleLists = new ArrayList<>();
        titleLists.add("Mr.");
        titleLists.add("Mrs./Miss.");
        titleAdapter = new ArrayAdapter<>(ProfileActivity.this, R.layout.layout_profile_sp_textview, titleLists);
        titleAdapter.setDropDownViewResource(R.layout.layout_profile_sp_textview);
        spTitle.setAdapter(titleAdapter);

        spGender = findViewById(R.id.sp_gender);
        genderLists = new ArrayList<>();
        genderLists.add("Male");
        genderLists.add("Female");
        genderAdapter = new ArrayAdapter<>(ProfileActivity.this, R.layout.layout_profile_sp_textview, genderLists);
        genderAdapter.setDropDownViewResource(R.layout.layout_profile_sp_textview);
        spGender.setAdapter(genderAdapter);

        profilePic = findViewById(R.id.img_profile_pic);
        btnChangePic = findViewById(R.id.btn_change);
        btnDeletePic = findViewById(R.id.btn_delete);
        btnHolder = findViewById(R.id.btn_holder);
        btnSave = findViewById(R.id.btn_save);
        btnCancel = findViewById(R.id.btn_cancel);

        etEmailAddress.setEnabled(false);
        etFirstName.setEnabled(false);
        etJobTitle.setEnabled(false);
        etMiddleName.setEnabled(false);
        etPhoneNo.setEnabled(false);

        spGender.setEnabled(false);
        genderAdapter = new ArrayAdapter<>(ProfileActivity.this, R.layout.layout_textview, genderLists);
        spGender.setAdapter(genderAdapter);

        spTitle.setEnabled(false);
        titleAdapter = new ArrayAdapter<>(ProfileActivity.this, R.layout.layout_textview, titleLists);
        spTitle.setAdapter(titleAdapter);

        etLastName.setEnabled(false);

        profilePic.setEnabled(false);
        btnChangePic.setEnabled(false);
        btnChangePic.setBackgroundResource(R.drawable.bg_rec_primary_disabled_curved);
        btnDeletePic.setEnabled(false);
        btnDeletePic.setBackgroundResource(R.drawable.bg_rec_primary_disabled_curved);

        toolbar.inflateMenu(R.menu.menu_edit_profile);
        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                ProfileActivity.this.menuItem = item;
                if (item.getItemId() == R.id.edit_profile) {
                    item.setVisible(false);
                    etEmailAddress.setEnabled(true);
                    etFirstName.setEnabled(true);
                    etLastName.setEnabled(true);
                    etJobTitle.setEnabled(true);
                    etMiddleName.setEnabled(true);
                    etPhoneNo.setEnabled(true);

                    spGender.setEnabled(true);
                    genderAdapter = new ArrayAdapter<>(ProfileActivity.this, R.layout.layout_profile_sp_textview, genderLists);
                    spGender.setAdapter(genderAdapter);

                    spTitle.setEnabled(true);
                    titleAdapter = new ArrayAdapter<>(ProfileActivity.this, R.layout.layout_profile_sp_textview, titleLists);
                    spTitle.setAdapter(titleAdapter);
                    profilePic.setEnabled(true);

                    btnChangePic.setEnabled(true);
                    btnChangePic.setBackgroundResource(R.drawable.bg_lightblue_curved);
                    btnDeletePic.setEnabled(true);
                    btnDeletePic.setBackgroundResource(R.drawable.bg_lightblue_curved);

                    btnHolder.setVisibility(View.VISIBLE);
                }
                return false;
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        if (NetworkUtils.isInNetwork(this)) {
            getUserDetailsTask();
        } else {
            AlertUtils.showSnack(ProfileActivity.this, toolbar, getResources().getString(R.string.error_no_internet));
        }

        profilePic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertForPhoto();
            }
        });

        btnChangePic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (NetworkUtils.isInNetwork(getApplicationContext())) {
                    if (!TextUtils.isEmpty(selectedImagePath)) {
                        uploadImageTask(selectedImagePath);
                    } else {
                        AlertUtils.showToast(ProfileActivity.this, "Please select image first.");
                    }
                } else {
                    AlertUtils.showSnack(ProfileActivity.this, toolbar, getResources().getString(R.string.error_no_internet));
                }
            }
        });


        btnDeletePic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectedImagePath = "";
                profilePic.setImageResource(R.drawable.img_profile_pic);
            }
        });

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (NetworkUtils.isInNetwork(getApplicationContext())) {
                    if (validation()) {
                        updateUserDetailsTask(tvUserId.getText().toString().trim(), etEmailAddress.getText().toString().trim(),
                                etPhoneNo.getText().toString().trim(), etFirstName.getText().toString().trim(),
                                etMiddleName.getText().toString().trim(), etLastName.getText().toString().trim(),
                                (String) spGender.getSelectedItem(), etJobTitle.getText().toString().trim());
                    } else {
                        AlertUtils.showSnack(ProfileActivity.this, view, "Please complete the profile with valid input");
                    }
                } else {
                    AlertUtils.showSnack(ProfileActivity.this, toolbar, getResources().getString(R.string.error_no_internet));
                }
            }
        });


    }

    private void updateUserDetailsTask(String userId, String email, String phone, String firstName,
                                       String middleName, String lastName, String gender, String jobTitle) {
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setTitle("Please Wait..");
        progressDialog.show();

        VolleyHelper volleyHelper = VolleyHelper.getInstance(this);
        HashMap<String, String> postParams = volleyHelper.getPostParams();
        postParams.put("userid", userId);
        postParams.put("email", email);
        postParams.put("phone0", phone);
        postParams.put("firstname", firstName);
        postParams.put("middlename", middleName);
        postParams.put("lastname", lastName);

        if (gender.equalsIgnoreCase("Male")) {
            postParams.put("maleuser", "true");
        } else {
            postParams.put("maleuser", "false");
        }

        postParams.put("jobtitle", jobTitle);

        volleyHelper.addVolleyRequestListeners(Api.getInstance().updateUserDetails, Request.Method.POST, postParams,
                new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(String response) {
                        try {
                            progressDialog.dismiss();
                            JSONObject res = new JSONObject(response);
                            JSONObject responseData = res.getJSONObject("ResponseData");
                            JSONObject details = responseData.getJSONObject("Details");
                            if (details.getString("appCode").equals(Api.APP_STATUS_OK)) {
                                menuItem.setVisible(true);
                                etEmailAddress.setEnabled(false);
                                etFirstName.setEnabled(false);
                                etLastName.setEnabled(false);
                                etJobTitle.setEnabled(false);
                                etMiddleName.setEnabled(false);
                                etPhoneNo.setEnabled(false);

                                spGender.setEnabled(false);
                                genderAdapter = new ArrayAdapter<>(ProfileActivity.this, R.layout.layout_textview, genderLists);
                                spGender.setAdapter(genderAdapter);

                                spTitle.setEnabled(false);
                                titleAdapter = new ArrayAdapter<>(ProfileActivity.this, R.layout.layout_textview, titleLists);
                                spTitle.setAdapter(titleAdapter);

                                profilePic.setEnabled(false);
                                profilePic.setEnabled(false);

                                btnChangePic.setEnabled(true);
                                btnChangePic.setBackgroundResource(R.drawable.bg_lightblue_curved);
                                btnDeletePic.setEnabled(true);
                                btnDeletePic.setBackgroundResource(R.drawable.bg_lightblue_curved);

                                btnHolder.setVisibility(View.GONE);

                                AlertUtils.showToast(ProfileActivity.this, "Profile updated successfully.");
                            }
                        } catch (JSONException e) {
                            progressDialog.dismiss();
                            try {
                                JSONObject res = new JSONObject(response);
                                JSONObject errorDetails = res.getJSONObject("Details");
                                AlertUtils.simpleAlert(ProfileActivity.this, "Error",
                                        errorDetails.getString("appCodeMessage"), "OK", "", null);

                            } catch (JSONException e1) {
                                e1.printStackTrace();
                            }
                            Logger.e("updateUserDetailsTask json ex", e.getMessage());
                        }
                    }

                    @Override
                    public void onError(String errorResponse, VolleyError volleyError) {
                        progressDialog.dismiss();
                        String errorMsg = "Unexpected error !!! Please try again with internet access.";
                        try {
                            JSONObject errorObj = new JSONObject(errorResponse);
                            errorMsg = errorObj.getJSONObject("Details").getString("appCodeMessage");
                        } catch (Exception e) {
                            Logger.e("updateUserDetailsTask error res", e.getMessage() + " ");
                        }
                        AlertUtils.showToast(ProfileActivity.this, errorMsg);
                    }
                }, "updateUserDetailsTask");

    }

    private void getUserDetailsTask() {
        VolleyHelper vHelper = VolleyHelper.getInstance(this);
        HashMap<String, String> postParams = vHelper.getPostParams();

        vHelper.addVolleyRequestListeners(Api.getInstance().getUserDetails, Request.Method.GET, postParams,
                new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(String response) {
                        try {
                            JSONObject res = new JSONObject(response);
                            JSONArray responseArray = res.getJSONArray("ResponseData");
                            if (responseArray.length() >= 1) {
                                JSONObject response1 = responseArray.getJSONObject(0);
                                JSONObject details = response1.getJSONObject("Details");
                                if (details.getString("appCode").equals(Api.APP_STATUS_OK)) {
                                    JSONObject response2 = responseArray.getJSONObject(1);
                                    JSONObject userDetails = response2.getJSONObject("UserDetails");
                                    tvScreenName.setText(userDetails.getString("screenname"));
                                    tvUserId.setText(userDetails.getString("userid"));
                                    etEmailAddress.setText(userDetails.getString("email"));
                                    etPhoneNo.setText(userDetails.getString("phone0"));
                                    etFirstName.setText(userDetails.getString("firstname"));
                                    etMiddleName.setText(userDetails.getString("middlename"));
                                    etLastName.setText(userDetails.getString("lastname"));

                                    boolean isMale = Boolean.parseBoolean(userDetails.getString("maleuser"));
                                    if (isMale) {
                                        spGender.setSelection(0);
                                        spTitle.setSelection(0);
                                    } else {
                                        spGender.setSelection(1);
                                        spTitle.setSelection(1);
                                    }
                                    etJobTitle.setText(userDetails.getString("jobtitle"));

                                    String imgUrl = userDetails.getString("image_url");
                                    if (!TextUtils.isEmpty(imgUrl) && !imgUrl.equals("null")) {
                                        Glide.with(ProfileActivity.this).load(Api.getInstance().getHost() + imgUrl)
                                                .apply(new RequestOptions().diskCacheStrategy(DiskCacheStrategy.NONE).skipMemoryCache(true))
                                                .into(profilePic);
                                        prefsHelper.edit().putString(PreferenceHelper.PROFILE_PIC,
                                                Api.getInstance().getHost() + imgUrl).commit();
                                    } else {
                                        prefsHelper.edit().putString(PreferenceHelper.PROFILE_PIC, "").commit();
                                    }

                                } else {
                                    AlertUtils.showToast(getApplicationContext(), details.getString("appCodeMessage"));
                                }
                            }
                        } catch (JSONException e) {
                            Logger.e("getUserDetailsTask json ex: ", e.getMessage());
                        }
                    }

                    @Override
                    public void onError(String errorResponse, VolleyError volleyError) {
                        String errorMsg = "Unexpected error !!! Please try again with internet access.";
                        try {
                            JSONObject errorObj = new JSONObject(errorResponse);
                            errorMsg = errorObj.getJSONObject("Details").getString("appCodeMessage");
                        } catch (Exception e) {
                            Logger.e("getUserDetailsTask error res", e.getMessage() + " ");
                        }
                        AlertUtils.showToast(ProfileActivity.this, errorMsg);
                    }
                }, "getUserDetailsTask");
    }

    private void uploadImageTask(String imageFilePath) {
        final ProgressDialog progressDialog = new ProgressDialog(ProfileActivity.this);
        progressDialog.setTitle("Please Wait..");
        progressDialog.show();
        VolleyHelper volleyHelper = VolleyHelper.getInstance(this);
        HashMap<String, String> postParams = volleyHelper.getPostParams();
        HashMap<String, String> filePath = new HashMap<>();
        filePath.put("newimage", imageFilePath);

        volleyHelper.addMultipartRequest(Api.getInstance().uploadImage, Request.Method.POST,
                postParams, filePath, new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(String response) {
                        progressDialog.dismiss();
                        try {
                            JSONObject res = new JSONObject(response);
                            JSONObject resObject = res.getJSONObject("ResponseData");
                            JSONObject resDetails = resObject.getJSONObject("Details");
                            if (resDetails.getString("appCode").equals(Api.APP_STATUS_OK)) {
                                AlertUtils.showToast(ProfileActivity.this, "Profile Pic Updated Successfully.");
                            }
                        } catch (JSONException e) {
                            Logger.e("uploadImageTask json ex", e.getMessage());
                        }
                    }

                    @Override
                    public void onError(String errorResponse, VolleyError volleyError) {
                        progressDialog.dismiss();
                        String errorMsg = "Unexpected error !!! Please try again with internet access.";
                        try {
                            JSONObject errorObj = new JSONObject(errorResponse);
                            errorMsg = errorObj.getJSONObject("Details").getString("appCodeMessage");
                        } catch (Exception e) {
                            Logger.e("uploadImageTask error res", e.getMessage() + " ");
                        }
                        AlertUtils.showToast(ProfileActivity.this, errorMsg);
                    }
                }, "uploadImageTask");

    }

    private boolean validation() {
        boolean isValid = true;

        if (etEmailAddress.getText().toString().trim().equals("")) {
            tvErrEmail.setText("This field is required...");
            tvErrEmail.setVisibility(View.VISIBLE);
            isValid = false;
        } else {
            tvErrEmail.setVisibility(View.GONE);
        }

        if (etFirstName.getText().toString().trim().equals("")) {
            tvErrFirstName.setText("This field is required...");
            tvErrFirstName.setVisibility(View.VISIBLE);
            isValid = false;
        } else {
            tvErrFirstName.setVisibility(View.GONE);
        }

        if (etLastName.getText().toString().trim().equals("")) {
            tvErrLastName.setText("This field is required...");
            tvErrLastName.setVisibility(View.VISIBLE);
            isValid = false;
        } else {
            tvErrLastName.setVisibility(View.GONE);
        }

//        if (etJobTitle.getText().toString().trim().equals("")) {
//            etJobTitle.setText("This field is required...");
//            etJobTitle.setVisibility(View.VISIBLE);
//            isValid = false;
//        } else {
//            etJobTitle.setVisibility(View.GONE);
//        }

//        if (etPhoneNo.getText().toString().trim().equals("")) {
//            etPhoneNo.setText("This field is required...");
//            etPhoneNo.setVisibility(View.VISIBLE);
//            isValid = false;
//        } else {
//            etPhoneNo.setVisibility(View.GONE);
//        }

        return isValid;
    }

    private void alertForPhoto() {
        final CharSequence[] options = {"Take Photo", "Choose From Gallery", "Cancel"};
        AlertDialog.Builder builder = new AlertDialog.Builder(ProfileActivity.this);
        builder.setTitle("Choose Medium");
        builder.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if (options[i].equals("Take Photo")) {
                    if (ContextCompat.checkSelfPermission(ProfileActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                            != PackageManager.PERMISSION_GRANTED) {
                        ActivityCompat.requestPermissions(ProfileActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                                CAMERA_CAPTURE_IMAGE_REQUEST_CODE);
                    } else {
                        takePhoto();
                    }
                } else if (options[i].equals("Choose From Gallery")) {
                    if (ContextCompat.checkSelfPermission(ProfileActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                            != PackageManager.PERMISSION_GRANTED) {

                        ActivityCompat.requestPermissions(ProfileActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                                RESULT_LOAD_IMAGE);
                    } else {
                        chooseFrmGallery(false);
                    }
                } else if (options[i].equals("Cancel")) {
                    dialogInterface.dismiss();
                }
            }
        });
        builder.show();
    }

    public void takePhoto() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
        fileUri = getOutputMediaFileUri(MEDIA_TYPE_IMAGE);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);

        // start the image capture Intent
        startActivityForResult(intent, CAMERA_CAPTURE_IMAGE_REQUEST_CODE);
    }

    public void chooseFrmGallery(boolean isVideo) {
        if (isVideo) {
            Intent i = new Intent(
                    Intent.ACTION_PICK,
                    android.provider.MediaStore.Video.Media.EXTERNAL_CONTENT_URI);

            startActivityForResult(i, RESULT_LOAD_VIDEO);
        } else {
            Intent i = new Intent(
                    Intent.ACTION_PICK,
                    android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

            startActivityForResult(i, RESULT_LOAD_IMAGE);
        }
    }

    public Uri getOutputMediaFileUri(int type) {
        return Uri.fromFile(getOutputMediaFile(type));
    }

    private static File getOutputMediaFile(int type) {
        File mediaStorageDir = new File(Environment.getExternalStorageDirectory(),
                "file_gtp");

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Logger.e("getOutputMediaFile", "Oops! Failed create "
                        + "file_gtp" + " directory");
                return null;
            }
        }

        // Create a media file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss",
                Locale.getDefault()).format(new Date());
        File mediaFile;
        if (type == MEDIA_TYPE_VIDEO) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator
                    + "VID_" + timeStamp + ".mp4");
        } else if (type == MEDIA_TYPE_IMAGE) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator
                    + "IMG_" + timeStamp + ".jpg");
        } else {
            return null;
        }
        return mediaFile;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == RESULT_LOAD_IMAGE) {
                Uri selectedImageUri = data.getData();
                String[] projection = {MediaStore.MediaColumns.DATA};
                CursorLoader cursorLoader = new CursorLoader(this, selectedImageUri,
                        projection, null, null, null);
                Cursor cursor = cursorLoader.loadInBackground();
                int column_index = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
                cursor.moveToFirst();
                String selImgPath = cursor.getString(column_index);
                Logger.e("imagePath loaded from gallery", selImgPath);

                CropImage.activity(selectedImageUri)
                        .setGuidelines(CropImageView.Guidelines.ON)
                        .setFixAspectRatio(true)
                        .setAspectRatio(1, 1)
                        .start(ProfileActivity.this);
            } else if (requestCode == CAMERA_CAPTURE_IMAGE_REQUEST_CODE) {
                if (fileUri != null) {
                    CropImage.activity(fileUri)
                            .setGuidelines(CropImageView.Guidelines.ON)
                            .setFixAspectRatio(true)
                            .setAspectRatio(1, 1)
                            .start(ProfileActivity.this);
                }
            } else if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
                CropImage.ActivityResult result = CropImage.getActivityResult(data);
                Uri resultUri = result.getUri();
                selectedImagePath = resultUri.getPath();
                if (resultUri.getPath() != null) {
                    Glide.with(ProfileActivity.this).load(resultUri).into(profilePic);
                }
            }
        }
    }

    //Add this method to show Dialog when the required permission has been granted to the app.
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[],
                                           @NonNull int[] grantResults) {
        switch (requestCode) {
            case CAMERA_CAPTURE_IMAGE_REQUEST_CODE: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    //Show dialog if the read permission has been granted.
                    takePhoto();
                } else {
                    //Permission has not been granted. Notify the user.
                    AlertUtils.showToast(ProfileActivity.this, "Permission Denied!!!");
                }
            }
        }
    }
}
