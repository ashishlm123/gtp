package com.emts.gtp.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.emts.gtp.Database;
import com.emts.gtp.R;
import com.emts.gtp.helper.AlertUtils;

public class SetUpAccountActivity extends AppCompatActivity {
    LayoutInflater inflater;
    Database dbHelper;
    LinearLayout llAccountList;
    Button btnDone;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_setup_account);

        Toolbar toolbar = findViewById(R.id.toolbar);
        TextView toolbarTitle = toolbar.findViewById(R.id.custom_toolbar_title);
        toolbarTitle.setText("Setup Account");
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        llAccountList = findViewById(R.id.screenField);
        inflater = LayoutInflater.from(this);
        dbHelper = Database.getInstance(this);

        btnDone = findViewById(R.id.btn_done);
        btnDone.setEnabled(false);
        TextView addAccount = findViewById(R.id.tv_add_ac);
        addAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addAccountField();
            }
        });
        addAccountField();

        btnDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertUtils.showAppAlert(SetUpAccountActivity.this, R.drawable.icon_accecpt,
                        "You have successfully added your account and setup your session pin code. " +
                                "Select an account to sign in now.", "OK", new AlertUtils.OnAlertButtonClickListener() {
                            @Override
                            public void onAlertButtonClick(boolean isPositiveButton) {
                                startActivity(new Intent(getApplicationContext(), AccountListActivity.class));
                                SetUpAccountActivity.this.finish();
                            }
                        });
            }
        });
    }

    private void addAccountField() {
        View newAccountView = inflater.inflate(R.layout.item_account_add, null, false);
        final EditText accountName = newAccountView.findViewById(R.id.et_screen_name);
        Button btnSave = newAccountView.findViewById(R.id.btn_save);
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (accountName.getText().toString().trim().length() > 0) {
                    if (dbHelper.isAccountAdded(accountName.getText().toString().trim())) {
                        AlertUtils.showAppAlert(SetUpAccountActivity.this, R.drawable.icon_not_accept,
                                "Screen Name already added", "OK", null);
                        return;
                    }
                    if (!dbHelper.addAccount(accountName.getText().toString().trim())) {
                        AlertUtils.showToast(getApplicationContext(), "Cannot add account");
                    }
                    view.setVisibility(View.GONE);
                    accountName.setEnabled(false);
                    btnDone.setEnabled(true);
                    btnDone.setBackgroundResource(R.color.colorPrimary);
                } else {
                    AlertUtils.showSnack(SetUpAccountActivity.this, llAccountList, "Screen Name is required");
                }
            }
        });
        llAccountList.addView(newAccountView);
    }
}
