package com.emts.gtp.activity;

import android.annotation.SuppressLint;
import android.app.KeyguardManager;
import android.arch.lifecycle.Lifecycle;
import android.arch.lifecycle.LifecycleObserver;
import android.arch.lifecycle.OnLifecycleEvent;
import android.arch.lifecycle.ProcessLifecycleOwner;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.view.WindowManager;

import com.emts.gtp.DialogSessionPinCode;
import com.emts.gtp.helper.Logger;
import com.emts.gtp.helper.PreferenceHelper;

import java.util.Timer;
import java.util.TimerTask;

public class BaseActivity extends AppCompatActivity {
    private static long lastInteractionTime = System.currentTimeMillis();
    static FragmentManager fm;
    static PreferenceHelper prefsHelper;

    static int disconnectTimeOut = 0;
    static boolean appWasInBackground;
    private int ACTIVITY_TRANSITION_DELAY_MS = 1400;

    @SuppressLint("HandlerLeak")
    private static Handler disconnectHandler = new Handler();

    private static Runnable disconnectCallback = new Runnable() {
        @Override
        public void run() {
            // Perform any required operation on disconnect
            Logger.e("disconnectTimeOut disconnect callback", disconnectTimeOut + "");
            Logger.e("lastinteractiontime disconnect callback", lastInteractionTime + "");
            Logger.e("systemcurrenmillis", System.currentTimeMillis() + "");
            Logger.e("differencesInTime callback", (System.currentTimeMillis() - lastInteractionTime) + "");
            if (System.currentTimeMillis() - lastInteractionTime >= disconnectTimeOut) {
                showSessionPinDialog();
                stopDisconnectTimer();
                return;
            }
            disconnectHandler.postDelayed(disconnectCallback, disconnectTimeOut);
        }
    };

    private static void showSessionPinDialog() {
        try {
            DialogSessionPinCode mDialog = DialogSessionPinCode.getInstance();
            if (mDialog.isAdded()) {
//                if (!mDialog.getDialog().isShowing()) {
//                    fm.beginTransaction().remove(mDialog).commit();
//                }
                Logger.e("sessionPinDialog ******", "already added ****************");
                return;
            }
            Logger.e("mDialog", "intialized");
            mDialog.setDialogInterface(new DialogSessionPinCode.OnDialogSessionPinCodeInterface() {
                @Override
                public void onDialogButtonClicked(boolean isPositive) {
                    if (!isPositive) {
                        android.os.Process.killProcess(android.os.Process.myPid());
                    } else {
                        resetDisconnectTimer();
                    }
                }
            });
            mDialog.show(fm, "baseactivitydialog");
        } catch (Exception e) {
        }
    }

    public static void resetDisconnectTimer() {
        disconnectTimeOut = prefsHelper.getInt(PreferenceHelper.TIME_OUT_IN_SEC, 0) * 1000;
        Logger.e("disconnectTimeOut", disconnectTimeOut + "");
        if (disconnectTimeOut > 0) {
            Logger.e("resetDisconnectTimer", "called");
            disconnectHandler.removeCallbacks(disconnectCallback);
            disconnectHandler.postDelayed(disconnectCallback, disconnectTimeOut);
        } else {
            disconnectHandler.removeCallbacks(disconnectCallback);
        }
    }

    public static void stopDisconnectTimer() {
        disconnectHandler.removeCallbacks(disconnectCallback);
    }

    @Override
    public void onUserInteraction() {
        lastInteractionTime = System.currentTimeMillis();
//        Logger.e("onUserInteraction", lastInteractionTime + "");
        resetDisconnectTimer();
    }

    @Override
    protected void onResume() {
        super.onResume();
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        fm = getSupportFragmentManager();
        prefsHelper = PreferenceHelper.getInstance(this);
        resetDisconnectTimer();

        if (prefsHelper.getBoolean(PreferenceHelper.SHOULD_SHOW_SESSION_PIN_DIALOG, false)) {
            showSessionPinDialog();
            prefsHelper.edit().putBoolean(PreferenceHelper.SHOULD_SHOW_SESSION_PIN_DIALOG, false).apply();
            return;
        }

//        new Handler().postDelayed(new Runnable() {
//            @Override
//            public void run() {
        if (appWasInBackground) {
            showSessionPinDialog();
        }
//            }
//        }, ACTIVITY_TRANSITION_DELAY_MS);
        stopActivityTransitionTimer();
    }

    @Override
    protected void onPause() {
        super.onPause();
        startActivityTransitionTimer();
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        appWasInBackground = false;
    }

    @Override
    protected void onStart() {
        super.onStart();

        IntentFilter filter = new IntentFilter();
        filter.addAction(Intent.ACTION_USER_PRESENT);
        filter.addAction(Intent.ACTION_SCREEN_OFF);
        registerReceiver(lockReceiver, filter);

//      //register observer
//        ProcessLifecycleOwner.get().getLifecycle().addObserver(appLifecycleListener);
    }

    @Override
    protected void onDestroy() {
        unregisterReceiver(lockReceiver);
//        ProcessLifecycleOwner.get().getLifecycle().removeObserver(appLifecycleListener);
        super.onDestroy();
    }


    /**
     * Screen lock receiver was needed
     */
    LockReceiver lockReceiver = new LockReceiver();

    class LockReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
//            Logger.e("baseActivity screenLockReceiver onReceive", intent + " :: " + intent.getAction());
//            if (intent.getAction().equals(Intent.ACTION_USER_PRESENT)) {
//                Logger.e("baseActivity screenLockReceiver onReceive", "Phone unlocked");
//                KeyguardManager keyguardManager = (KeyguardManager) context.getSystemService(Context.KEYGUARD_SERVICE);
//                if (keyguardManager != null)
//                    if (keyguardManager.isKeyguardSecure()) {
//                        //phone was unlocked, do stuff here
//                        showSessionPinDialog();
//                    }
//            } else if (intent.getAction().equals(Intent.ACTION_SCREEN_OFF)) {
//                Logger.e("baseActivity screenLockReceiver onReceive", "Phone locked");
//                prefsHelper.edit().putBoolean(PreferenceHelper.SHOULD_SHOW_SESSION_PIN_DIALOG, true).commit();
//            }
        }
    }


    //SOLUTION 2:
//    AppLifecycleListener appLifecycleListener = new AppLifecycleListener();
//
//    public class AppLifecycleListener implements LifecycleObserver {
//
//        @OnLifecycleEvent(Lifecycle.Event.ON_START)
//        public void onMoveToForeground() {
//            // app moved to foreground
////            showSessionPinDialog();
//            appWasInBackground = false;
//        }
//
//        @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
//        public void onMoveToBackground() {
//            // app moved to background
//            appWasInBackground = true;
//        }
//    }

    static private Timer mActivityTransitionTimer;
    static private TimerTask mActivityTransitionTimerTask;

    public void startActivityTransitionTimer() {
        mActivityTransitionTimer = new Timer();
        mActivityTransitionTimerTask = new TimerTask() {
            public void run() {
                appWasInBackground = true;
            }
        };

        mActivityTransitionTimer.schedule(mActivityTransitionTimerTask,
                ACTIVITY_TRANSITION_DELAY_MS);
    }

    public void stopActivityTransitionTimer() {
        if (mActivityTransitionTimerTask != null) {
            mActivityTransitionTimerTask.cancel();
        }

        if (mActivityTransitionTimer != null) {
            mActivityTransitionTimer.cancel();
        }

        appWasInBackground = false;
    }


//  //SOLUTION 1:

//
//    public boolean isApplicationSentToBackground(final Context context) {
//        ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
//        List<ActivityManager.RunningTaskInfo> tasks = am.getRunningTasks(1);
//        if (!tasks.isEmpty()) {
//            ComponentName topActivity = tasks.get(0).topActivity;
//            if (!topActivity.getPackageName().equals(context.getPackageName())) {
//                Logger.e("baseActivity onPause()", "the application is in background");
//                return true;
//            }
//        }
//        return false;
//
////        boolean isInBackground = true;
////        ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
////        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT_WATCH) {
////            List<ActivityManager.RunningAppProcessInfo> runningProcesses = am.getRunningAppProcesses();
////            for (ActivityManager.RunningAppProcessInfo processInfo : runningProcesses) {
////                if (processInfo.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND) {
////                    for (String activeProcess : processInfo.pkgList) {
////                        if (activeProcess.equals(context.getPackageName())) {
////                            isInBackground = false;
////                        }
////                    }
////                }
////            }
////        } else {
////            List<ActivityManager.RunningTaskInfo> taskInfo = am.getRunningTasks(1);
////            ComponentName componentInfo = taskInfo.get(0).topActivity;
////            if (componentInfo.getPackageName().equals(context.getPackageName())) {
////                isInBackground = false;
////            }
////        }
////        return isInBackground;
//    }

//    @Override
//    public void onPause() {
////        if (isApplicationSentToBackground(this)) {
////            //with delay
//////            new Handler().postDelayed(new Runnable() {
//////                @Override
//////                public void run() {
////            showSessionPinDialog();
//////                }
//////            }, 1000);
////        } else {
////            Logger.e("baseActivity onPause()", "the application is not in background");
////        }
//        super.onPause();
//    }

}

