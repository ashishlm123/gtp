package com.emts.gtp.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.View;
import android.widget.TextView;

import com.emts.gtp.R;
import com.emts.gtp.model.NotificationModel;

public class NotificationDetailsActivity extends BaseActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification_details);

        Toolbar toolbar = findViewById(R.id.toolbar);
        TextView toolbarTitle = toolbar.findViewById(R.id.custom_toolbar_title);
        toolbarTitle.setText("Notification Details");
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        Intent intent = getIntent();
        NotificationModel notificationModel = (NotificationModel) intent.getSerializableExtra("notification");


        TextView notiTitle, notiBody;
        notiTitle = findViewById(R.id.tv_noti_title);
        notiTitle.setText(notificationModel.getNotiTitle());
        notiBody = findViewById(R.id.tv_noti_body);
        notiBody.setText(Html.fromHtml(notificationModel.getNotiBody()));


    }
}
