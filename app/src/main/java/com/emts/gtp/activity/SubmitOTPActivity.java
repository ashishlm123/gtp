package com.emts.gtp.activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.emts.gtp.Api;
import com.emts.gtp.R;
import com.emts.gtp.helper.AlertUtils;
import com.emts.gtp.helper.Logger;
import com.emts.gtp.helper.NetworkUtils;
import com.emts.gtp.helper.VolleyHelper;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class SubmitOTPActivity extends AppCompatActivity {

    int seconds, minutes;
    TextView timerExpiry;
    CountDownTimer timer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        setContentView(R.layout.activity_submit_otp);

        Intent intent = getIntent();
        long otpExpiry = intent.getLongExtra("otp_expiry", 136000);
        String otpVal = intent.getStringExtra("otp_val");
        final String screenName = intent.getStringExtra("screen_name");


        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        TextView toolbarTitle = toolbar.findViewById(R.id.custom_toolbar_title);
        toolbarTitle.setText("Forgot Password");

        final EditText edtOTP = findViewById(R.id.edt_OTP);
        final Button btnSubmitOtp = findViewById(R.id.btn_submit);
        btnSubmitOtp.setEnabled(false);

        timerExpiry = findViewById(R.id.otp_expiry_timer);

        timer = new CountDownTimer(otpExpiry, 1000) { // adjust the milli seconds here

            @SuppressLint({"DefaultLocale", "SetTextI18n"})
            public void onTick(long millisUntilFinished) {
                int seconds = (int) (millisUntilFinished / 1000);
                int minutes = seconds / 60;
                seconds = seconds % 60;
                timerExpiry.setText(" " + String.format("%02d", minutes)
                        + ":" + String.format("%02d", seconds));
            }

            public void onFinish() {
                AlertUtils.showToast(getApplicationContext(), "OTP time out");
                onBackPressed();
            }
        }.start();


        edtOTP.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.length() == 6) {
                    btnSubmitOtp.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                    btnSubmitOtp.setEnabled(true);
                } else {
                    btnSubmitOtp.setBackgroundColor(getResources().getColor(R.color.btnDisable));
                    btnSubmitOtp.setEnabled(false);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        btnSubmitOtp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (NetworkUtils.isInNetwork(SubmitOTPActivity.this)) {
                    submitOtpTask(edtOTP.getText().toString().trim(), screenName);
                }
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
    }

    @Override
    public void onBackPressed() {
        if (timer != null) timer.cancel();
        super.onBackPressed();
    }

    private void submitOtpTask(String otp, final String screenName) {
        VolleyHelper vHelper = VolleyHelper.getInstance(this);
        HashMap<String, String> postParams = vHelper.getPostParams();
        postParams.put("otp_val", otp);
        postParams.put("screenname", screenName);
//        postParams.put("screenname", PreferenceHelper.getInstance(getApplicationContext()).getDefaultUserName());

        vHelper.addVolleyRequestListeners(Api.getInstance().submitOtp, Request.Method.POST, postParams,
                new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(String response) {
                        try {
                            JSONObject res = new JSONObject(response);
                            JSONObject responseData = res.getJSONObject("ResponseData");
                            JSONObject details = responseData.getJSONObject("Details");
                            if (details.getString("appCode").equals(Api.APP_STATUS_OK)) {
                                Intent intent = new Intent(getApplicationContext(), ChangePasswordActivity.class);
                                intent.putExtra("screen_name", screenName);
                                if (timer != null) timer.cancel();
                                SubmitOTPActivity.this.finish();
                                startActivity(intent);
                            } else {
                                AlertUtils.showToast(getApplicationContext(), details.getString("appCodeMessage"));
                            }
                        } catch (JSONException e) {
                            Logger.e("submitOtpTask json ex", e.getMessage());
                            try {
                                JSONObject errorObj = new JSONObject(response);
                                AlertUtils.showToast(SubmitOTPActivity.this,
                                        errorObj.getJSONObject("Details").getString("appCodeMessage"));
                            } catch (Exception e1) {
                                Logger.e("submitOtpTask ex ex", e1.getMessage() + " ");
                            }
                        }
                    }

                    @Override
                    public void onError(String errorResponse, VolleyError volleyError) {
                        String errorMsg = "Unexpected error !!! Please try again with internet access.";
                        try {
                            JSONObject errorObj = new JSONObject(errorResponse);
                            errorMsg = errorObj.getJSONObject("Details").getString("appCodeMessage");
                        } catch (Exception e) {
                            Logger.e("submitOtpTask error res", e.getMessage() + " ");
                        }
                        AlertUtils.showToast(SubmitOTPActivity.this, errorMsg);
                    }
                }, "submitOtpTask");
    }
}
