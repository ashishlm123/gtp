package com.emts.gtp.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.WindowManager;
import android.widget.TextView;

import com.emts.gtp.Database;
import com.emts.gtp.R;
import com.emts.gtp.adapter.AccountAdapter;
import com.emts.gtp.model.Account;

import java.util.ArrayList;

public class AccountListActivity extends AppCompatActivity {
    Database dbHelper;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        setContentView(R.layout.activity_account_listing);
        Toolbar toolbar = findViewById(R.id.toolbar);
        final TextView toolbarTitle = toolbar.findViewById(R.id.custom_toolbar_title);
        toolbarTitle.setText("Sign In");
        toolbar.setNavigationIcon(null);
        toolbar.inflateMenu(R.menu.menu);
        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                if (item.getItemId() == R.id.manageac) {
                    startActivity(new Intent(getApplicationContext(), ManageAccountActivity.class));
                    return true;
                }
                return false;
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        RecyclerView recyclerView = findViewById(R.id.account_list);
        LinearLayoutManager layoutManager = new LinearLayoutManager(AccountListActivity.this);
        recyclerView.setLayoutManager(layoutManager);

        dbHelper = Database.getInstance(this);
        ArrayList<Account> acList = dbHelper.getAllAccounts();
        AccountAdapter adapter = new AccountAdapter(AccountListActivity.this, acList);
        adapter.setSignIn(true);
        recyclerView.setAdapter(adapter);
    }
}
