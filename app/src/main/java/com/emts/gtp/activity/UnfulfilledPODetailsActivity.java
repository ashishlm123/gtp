package com.emts.gtp.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.emts.gtp.R;
import com.emts.gtp.model.UnfulfilledPOModel;

public class UnfulfilledPODetailsActivity extends BaseActivity {
    UnfulfilledPOModel unfulfilledPo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_unfulfilled_podetails);

        Toolbar toolbar = findViewById(R.id.toolbar);
        TextView toolbarTitle = toolbar.findViewById(R.id.custom_toolbar_title);
        toolbarTitle.setText("Unfulfilled PO Details");
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        Intent intent = getIntent();
        if (intent != null) {
            unfulfilledPo = (UnfulfilledPOModel) intent.getSerializableExtra("unfulfilledPo");
        }

        TextView tvPoNo, tvPoDate, tvVendorCode, tvProduct, tvPoQty,
                tvPoUnitPrice, tvPoAmt, tvPoGst, tvPoTotal, tvGrnRef,
                tvUnfulfillQty1, tvDraftGrnQty_2, tvBalance_1_2, tvGtpBookingNo;


        tvPoNo = findViewById(R.id.tv_po_num);
        tvPoNo.setText(unfulfilledPo.getPoNo());

        tvPoDate = findViewById(R.id.tv_po_date);
        tvPoDate.setText(unfulfilledPo.getPoDate());

        tvVendorCode = findViewById(R.id.tv_vendor_code);
        tvVendorCode.setText(unfulfilledPo.getPoVendorCode());

        tvProduct = findViewById(R.id.tv_product);
        tvProduct.setText(unfulfilledPo.getPoProduct());

        tvPoQty = findViewById(R.id.tv_po_qty);
        tvPoQty.setText(unfulfilledPo.getPoQty());

        tvUnfulfillQty1 = findViewById(R.id.tv_unfulfill_qty1);
        tvUnfulfillQty1.setText(unfulfilledPo.getPoUnfulfillQty_1());

        tvDraftGrnQty_2 = findViewById(R.id.tv_draft_grn_qty);
        tvDraftGrnQty_2.setText(unfulfilledPo.getDraftGrnQty_2());

        tvBalance_1_2 = findViewById(R.id.tv_one_two_balance);
        tvBalance_1_2.setText(unfulfilledPo.getBalance_1_2());

        tvPoUnitPrice = findViewById(R.id.tv_po_unit_price);
        tvPoUnitPrice.setText(unfulfilledPo.getPoUnitPrice());

        tvPoAmt = findViewById(R.id.tv_po_amt);
        tvPoAmt.setText(unfulfilledPo.getPoAmount());

        tvPoGst = findViewById(R.id.tv_po_gst);
        tvPoGst.setText(unfulfilledPo.getPoGst());

        tvPoTotal = findViewById(R.id.tv_po_total);
        tvPoTotal.setText(unfulfilledPo.getPoTotal());

        tvGrnRef = findViewById(R.id.tv_grn_ref);
        tvGrnRef.setText(unfulfilledPo.getGrnRef());

        tvGtpBookingNo = findViewById(R.id.tv_gtp_booking_no);
        tvGtpBookingNo.setText(unfulfilledPo.getGtpBookingNo());

    }
}
