package com.emts.gtp.activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.emts.gtp.Database;
import com.emts.gtp.R;
import com.emts.gtp.helper.PreferenceHelper;

public class SetUpSessionPinCode extends AppCompatActivity {
    @SuppressLint("NewApi")
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        setContentView(R.layout.activity_setup_sessionpin);

        Toolbar toolbar = findViewById(R.id.toolbar);
        final TextView toolbarTitle = toolbar.findViewById(R.id.custom_toolbar_title);
        toolbarTitle.setText("Setup Session Pin");
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        final EditText etSessionPin = findViewById(R.id.et_pincode);
        final Button btnNext = findViewById(R.id.btn_next);
        etSessionPin.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                String pinLength = etSessionPin.getText().toString();
                if (pinLength.length() == 6) {
                    btnNext.setEnabled(true);
                    btnNext.setBackgroundResource(R.color.colorPrimary);
                } else {
                    btnNext.setEnabled(false);
                    btnNext.setBackgroundResource(R.color.btnDisable);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });

        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PreferenceHelper preferenceHelper = PreferenceHelper.getInstance(SetUpSessionPinCode.this);
                preferenceHelper.edit().putString(PreferenceHelper.SESSION_PIN_CODE, etSessionPin.getText().toString().trim()).apply();
                if (Database.getInstance(SetUpSessionPinCode.this).isAccountAdded()) {
                    startActivity(new Intent(getApplicationContext(), AccountListActivity.class));
                } else {
                    startActivity(new Intent(getApplicationContext(), SetUpAccountActivity.class));
                }
                SetUpSessionPinCode.this.finish();
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
    }
}
