package com.emts.gtp.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.View;
import android.view.WindowManager;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.emts.gtp.Api;
import com.emts.gtp.Database;
import com.emts.gtp.R;
import com.emts.gtp.helper.Logger;
import com.emts.gtp.helper.NetworkUtils;
import com.emts.gtp.helper.PreferenceHelper;
import com.emts.gtp.helper.VolleyHelper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class ImportantNoteActivity extends AppCompatActivity {
    PreferenceHelper prefsHelper;
    ProgressBar progressBar;
    Button btnGetStarted;
    TextView tvNotice;
    WebView webView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        setContentView(R.layout.activity_important_note);

        Toolbar toolbar = findViewById(R.id.toolbar);
        final TextView toolbarTitle = toolbar.findViewById(R.id.custom_toolbar_title);
        toolbarTitle.setText("GTP");
        toolbar.setNavigationIcon(null);

        webView = findViewById(R.id.wv);
        webView.setWebViewClient(new WebViewClient());
        progressBar = findViewById(R.id.progress);
        tvNotice = findViewById(R.id.tvNotice);
        btnGetStarted = findViewById(R.id.btnGetStarted);
        btnGetStarted.setVisibility(View.GONE);
        btnGetStarted.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                prefsHelper.edit().putBoolean(PreferenceHelper.FIRST_GET_STARTED, false).apply();

                if (prefsHelper.isSessionPinSet()) {
                    Database dbHelper = Database.getInstance(ImportantNoteActivity.this);
                    if (dbHelper.isAccountAdded()) {
                        startActivity(new Intent(getApplicationContext(), AccountListActivity.class));
                    } else {
                        startActivity(new Intent(getApplicationContext(), SetUpAccountActivity.class));
                    }
                } else {
                    startActivity(new Intent(getApplicationContext(), SetUpSessionPinCode.class));
                }
                ImportantNoteActivity.this.finish();
            }
        });

        prefsHelper = PreferenceHelper.getInstance(this);

        if (NetworkUtils.isInNetwork(this)) {
            getImportantNoticeTask();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
    }

    private void getImportantNoticeTask() {
        progressBar.setVisibility(View.VISIBLE);

        VolleyHelper vHelper = VolleyHelper.getInstance(this);
        HashMap<String, String> postMap = vHelper.getPostParams();

        vHelper.addVolleyRequestListeners(Api.getInstance().noticeApi, Request.Method.GET,
                postMap, new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(final String response) {
                        progressBar.setVisibility(View.GONE);
                        try {
                            final JSONObject resObj = new JSONObject(response);
                            JSONArray resArray = resObj.getJSONArray("ResponseData");
                            if (resArray.length() >= 2) {
                                JSONObject jsonObject = resArray.getJSONObject(1);
//                                tvNotice.setText(Html.fromHtml("<![CDATA[" +
//                                        jsonObject.getJSONObject("article").getString("article_body"))
//                                        + "]]>");
                                webView.loadData("<html><body>" +
                                        jsonObject.getJSONObject("article").getString("article_body") +
                                        "</body></html>", "text/html", "utf-8");
                                webView.setVisibility(View.VISIBLE);
                            }
                            tvNotice.setVisibility(View.GONE);
                        } catch (JSONException e) {
                            Logger.e("getImportantNoticeTask res ex", e.getMessage() + " ");

                            try {
                                final JSONObject resObj = new JSONObject(response);
                                webView.loadData("<html><body>" +
                                        resObj.getJSONObject("Details").getString("appCodeMessage") +
                                        "</body></html>", "text/html", "utf-8");
                                webView.setVisibility(View.VISIBLE);
                            } catch (JSONException e1) {
                                Logger.e("getImportantNoticeTask res ex ko ex", e1.getMessage() + " ");
                            }
                        }
                        btnGetStarted.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void onError(String errorResponse, VolleyError volleyError) {
                        progressBar.setVisibility(View.GONE);
                        String errorMsg = "Unexpected error !!! Please try again with internet access.";
                        try {
                            JSONObject errorObj = new JSONObject(errorResponse);
                            errorMsg = errorObj.getJSONObject("Details").getString("appCodeMessage");
                        } catch (Exception e) {
                            Logger.e("getImportantNoticeTask error res", e.getMessage() + " ");
                        }
                        tvNotice.setText(Html.fromHtml(errorMsg));
                        tvNotice.setVisibility(View.VISIBLE);
                        webView.setVisibility(View.GONE);
                        btnGetStarted.setVisibility(View.VISIBLE);
                    }
                }, "getImportantNoticeTask");
    }
}
