package com.emts.gtp.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.emts.gtp.Api;
import com.emts.gtp.Database;
import com.emts.gtp.R;
import com.emts.gtp.helper.AlertUtils;
import com.emts.gtp.helper.Logger;
import com.emts.gtp.helper.NetworkUtils;
import com.emts.gtp.helper.VolleyHelper;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class ChangePasswordActivity extends AppCompatActivity {
    EditText edtNewPassword, edtConfirmPassword;
    TextView tvErrNewPassword, tvErrConfirmPassword;
    String screenName = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_change_password);

        Toolbar toolbar = findViewById(R.id.toolbar);
        TextView toolbarTitle = toolbar.findViewById(R.id.custom_toolbar_title);
        toolbarTitle.setText("Forgot Password");
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        Intent intent = getIntent();
        if (intent != null) {
            screenName = intent.getStringExtra("screen_name");
        }

        edtNewPassword = findViewById(R.id.edt_new_password);
        edtConfirmPassword = findViewById(R.id.edt_con_password);

        tvErrNewPassword = findViewById(R.id.tv_new_pswd);
        tvErrConfirmPassword = findViewById(R.id.tv_err_confirm_pswd);

        final Button btnDone = findViewById(R.id.btn_done);
        btnDone.setEnabled(false);

        edtConfirmPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (!TextUtils.isEmpty(charSequence)) {
                    if (charSequence.toString().equals(edtNewPassword.getText().toString().trim())) {
                        btnDone.setEnabled(true);
                        btnDone.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                    } else {
                        btnDone.setEnabled(false);
                        btnDone.setBackgroundColor(getResources().getColor(R.color.btnDisable));
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });

        btnDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (validation()) {
                    if (NetworkUtils.isInNetwork(ChangePasswordActivity.this)) {
                        changePasswordTask(edtNewPassword.getText().toString().trim(),
                                edtConfirmPassword.getText().toString().trim(), screenName);
                    } else {
                        AlertUtils.showToast(ChangePasswordActivity.this, getResources().getString(R.string.error_no_internet));
                    }
                }
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
    }

    private boolean validation() {
        boolean isValid = true;

        if (edtNewPassword.getText().toString().trim().equals("")) {
            tvErrNewPassword.setVisibility(View.VISIBLE);
            isValid = false;
        } else {
            tvErrNewPassword.setVisibility(View.GONE);
        }

        if (edtConfirmPassword.getText().toString().trim().equals("")) {
            tvErrConfirmPassword.setVisibility(View.VISIBLE);
            isValid = false;
        } else {
            tvErrConfirmPassword.setVisibility(View.GONE);
        }
        return isValid;
    }

    private void changePasswordTask(final String pswd1, final String pswd2, final String screenName) {
        AlertUtils.hideInputMethod(ChangePasswordActivity.this, tvErrConfirmPassword);
        final ProgressDialog pDialog = AlertUtils.showProgressDialog(ChangePasswordActivity.this, "Please wait...");
        VolleyHelper vHelper = VolleyHelper.getInstance(this);
        HashMap<String, String> postParams = vHelper.getPostParams();
//        postParams.put("userId", "32501");
        postParams.put("password1", pswd1);
        postParams.put("password2", pswd2);
        postParams.put("screenName", screenName);
//        postParams.put("screenName", "joeystudent0001");

        vHelper.addVolleyRequestListeners(Api.getInstance().updatePassword, Request.Method.POST, postParams, new VolleyHelper.VolleyHelperInterface() {
            @Override
            public void onSuccess(String response) {
                pDialog.dismiss();
                try {
                    JSONObject res = new JSONObject(response);
                    JSONObject responseData = res.getJSONObject("ResponseData");
                    JSONObject details = responseData.getJSONObject("Details");
                    if (details.getString("appCode").equals(Api.APP_STATUS_OK)) {
//                        PreferenceHelper.getInstance(ChangePasswordActivity.this).setPassword(pswd2);
                        Database.getInstance(ChangePasswordActivity.this).updatePassword(screenName, pswd2);
                        AlertUtils.simpleAlert(ChangePasswordActivity.this, "Success",
                                "Your password has been successfully changed.", "OK", "", new
                                        AlertUtils.OnAlertButtonClickListener() {
                                            @Override
                                            public void onAlertButtonClick(boolean isPositiveButton) {
                                                onBackPressed();
                                            }
                                        });
                    }
                } catch (JSONException e) {
                    Logger.e("catch changePasswordTask json ex", e.getMessage());
                    try {
                        JSONObject res = new JSONObject(response);
                        AlertUtils.showAppAlert(ChangePasswordActivity.this, R.drawable.icon_not_accept,
                                res.getJSONObject("Details").getString("appCodeMessage"), "OK", null);
                    } catch (Exception e1) {
                        Logger.e("catch changePasswordTask ex ", e1.getMessage());
                    }
                }
            }

            @Override
            public void onError(String errorResponse, VolleyError volleyError) {
                pDialog.dismiss();
                String errorMsg = "Unexpected error !!! Please try again with internet access.";
                try {
                    JSONObject errorObj = new JSONObject(errorResponse);
                    errorMsg = errorObj.getJSONObject("Details").getString("appCodeMessage");
                } catch (Exception e) {
                    Logger.e("changePasswordTask error res", e.getMessage() + " ");
                }
                AlertUtils.simpleAlert(ChangePasswordActivity.this, "Error !!!", errorMsg,
                        "OK", "", null);
            }
        }, "changePasswordTask");
    }
}
