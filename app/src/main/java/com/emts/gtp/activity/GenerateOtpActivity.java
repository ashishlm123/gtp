package com.emts.gtp.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.emts.gtp.Api;
import com.emts.gtp.Database;
import com.emts.gtp.R;
import com.emts.gtp.helper.AlertUtils;
import com.emts.gtp.helper.Logger;
import com.emts.gtp.helper.NetworkUtils;
import com.emts.gtp.helper.PreferenceHelper;
import com.emts.gtp.helper.VolleyHelper;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class GenerateOtpActivity extends AppCompatActivity {
    TextView edtScreenName;
    //    EditText edtTextVerification;
    TextView tvErrScreeName;
    //    TextView tvErrTextVerification;
    Database database;
    String screenName = "";
    PreferenceHelper prefsHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        setContentView(R.layout.activity_forgot_password);

        database = Database.getInstance(this);
        prefsHelper = PreferenceHelper.getInstance(this);

        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        TextView toolbarTitle = toolbar.findViewById(R.id.custom_toolbar_title);
        toolbarTitle.setText("Forgot Password");

        Intent intent = getIntent();

        if (intent != null) {
            screenName = intent.getStringExtra("screen_name");
        }

        edtScreenName = findViewById(R.id.edt_screen_name);
        edtScreenName.setText(screenName);
//        edtTextVerification = findViewById(R.id.edt_text_verification);

        tvErrScreeName = findViewById(R.id.tv_err_screen_name);
//        tvErrTextVerification = findViewById(R.id.tv_err_text_verification);

        final Button btnNext = findViewById(R.id.btn_next);
        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (validation()) {
                    if (NetworkUtils.isInNetwork(GenerateOtpActivity.this)) {
//                        generateOtpTask(edtScreenName.getText().toString().trim());
                        generateOtpTask(screenName);
                    } else {
                        AlertUtils.showSnack(GenerateOtpActivity.this, btnNext,
                                getResources().getString(R.string.error_no_internet));
                    }
                }
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
    }

    private void generateOtpTask(final String screenName) {
        VolleyHelper vHelper = VolleyHelper.getInstance(this);
        HashMap<String, String> postParams = vHelper.getPostParams();
        vHelper.addVolleyRequestListeners(Api.getInstance().generateOTP + "/" + screenName,
                Request.Method.GET, postParams, new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(String response) {
                        try {
                            JSONObject res = new JSONObject(response);
                            JSONObject responseData = res.getJSONObject("ResponseData");
                            JSONObject responseDetails = responseData.getJSONObject("Details");

                            if (responseDetails.getString("appCode").equals(Api.APP_STATUS_OK)) {
                                long presentOtpTimeStamps = Long.parseLong(responseDetails.getString("otp_timestamp"));
                                long expiryOtpTimeStamps = Long.parseLong(responseDetails.getString("otp_expiry"));
                                long remainingOtpTime = expiryOtpTimeStamps - presentOtpTimeStamps;
                                System.out.println("Seconds: " + remainingOtpTime);
                                Intent intent = new Intent(getApplicationContext(), SubmitOTPActivity.class);
                                intent.putExtra("otp_expiry", remainingOtpTime);
                                intent.putExtra("screen_name", screenName);
                                intent.putExtra("otp_val", responseDetails.getString("otp_val"));
                                GenerateOtpActivity.this.finish();
                                startActivity(intent);
                            }

                        } catch (JSONException e) {
                            Logger.e("generateOtpTask json ex", e.getMessage());
                            try {
                                JSONObject res = new JSONObject(response);
                                AlertUtils.showAppAlert(GenerateOtpActivity.this, R.drawable.icon_not_accept,
                                        res.getJSONObject("Details").getString("appCodeMessage"), "OK", null);
                            } catch (Exception e1) {
                                Logger.e("generateOtpTask ex ex", e1.getMessage());
                            }
                        }
                    }

                    @Override
                    public void onError(String errorResponse, VolleyError volleyError) {
                        String errorMsg = "Unexpected error !!! Please try again with internet access.";
                        try {
                            JSONObject errorObj = new JSONObject(errorResponse);
                            errorMsg = errorObj.getJSONObject("Details").getString("appCodeMessage");
                        } catch (Exception e) {
                            Logger.e("generateOtpTask error res", e.getMessage() + " ");
                        }
                        AlertUtils.showToast(GenerateOtpActivity.this, errorMsg);
                    }
                }, "generateOtpTask");
    }

    public boolean validation() {
        boolean isValid = true;

        if (edtScreenName.getText().toString().trim().equals("")) {
            tvErrScreeName.setText("This field is required...");
            tvErrScreeName.setVisibility(View.VISIBLE);
            isValid = false;
            return isValid;
        } else {
            tvErrScreeName.setVisibility(View.GONE);
        }

//        if (edtTextVerification.getText().toString().trim().equals("")) {
//            tvErrTextVerification.setVisibility(View.VISIBLE);
//            isValid = false;
//        } else {
//            tvErrTextVerification.setVisibility(View.GONE);
//        }

        if (!database.isScreenName(edtScreenName.getText().toString().trim())) {
            tvErrScreeName.setText("The screen name you requested is not registered in our database.");
            tvErrScreeName.setVisibility(View.VISIBLE);
            isValid = false;
        } else {
            tvErrScreeName.setVisibility(View.GONE);
        }

        return isValid;
    }
}


