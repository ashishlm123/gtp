package com.emts.gtp.activity;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.emts.gtp.Api;
import com.emts.gtp.Database;
import com.emts.gtp.R;
import com.emts.gtp.helper.AlertUtils;
import com.emts.gtp.helper.Logger;
import com.emts.gtp.helper.NetworkUtils;
import com.emts.gtp.helper.PreferenceHelper;
import com.emts.gtp.helper.VolleyHelper;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class PasswordActivity extends BaseActivity {
    EditText etCurrentPswd, etNewPswd, etConPswd;
    TextView tvErrCurrPswd, tvErrNewPswd, tvErrConPswd;
    PreferenceHelper prefsHelper;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_password);

        prefsHelper = PreferenceHelper.getInstance(this);

        Toolbar toolbar = findViewById(R.id.toolbar);
        TextView toolbarTitle = toolbar.findViewById(R.id.custom_toolbar_title);
        toolbarTitle.setText("Password");
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        etCurrentPswd = findViewById(R.id.et_current_password);
        etNewPswd = findViewById(R.id.et_new_password);
        etConPswd = findViewById(R.id.et_confirm_password);
//        etCustomQuestion = findViewById(R.id.et_custom_question);
//        etAnswer = findViewById(R.id.et_answer);
//
//        spQuestions = findViewById(R.id.sp_question);
//        ArrayList<String> spQuestionLists = new ArrayList<>();
//        spQuestionLists.add("Write my own question");
//        spQuestionLists.add("What is your first pet's name?");
//        spQuestionLists.add("What is your favourite teacher's name?");
//        spQuestionLists.add("What is the name of your hometown?");
//        spQuestionLists.add("What is the name of place where you were born?");
//
//        ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<>(PasswordActivity.this,
//                R.layout.layout_textview, spQuestionLists);
//        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//        spQuestions.setAdapter(spinnerAdapter);

        tvErrCurrPswd = findViewById(R.id.tv_err_current_password);
        tvErrNewPswd = findViewById(R.id.tv_err_new_password);
        tvErrConPswd = findViewById(R.id.tv_err_confirm_pswd);
//        tvErrCustomQuestion = findViewById(R.id.tv_err_custom_question);
//        tvErrAnswer = findViewById(R.id.tv_err_answer);
//        tvCustomQuestion = findViewById(R.id.tv_custom_question);
//
//        spQuestions.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
//                if (i == 0) {
//                    etCustomQuestion.setVisibility(View.VISIBLE);
//                    tvCustomQuestion.setVisibility(View.VISIBLE);
//                } else {
//                    etCustomQuestion.setVisibility(View.GONE);
//                    tvCustomQuestion.setVisibility(View.GONE);
//                    tvErrCustomQuestion.setVisibility(View.GONE);
//                }
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> adapterView) {
//
//            }
//        });

        Button btnCancel = findViewById(R.id.btn_cancel);
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        Button btnSave = findViewById(R.id.btn_save);
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (validation()) {
                    if (NetworkUtils.isInNetwork(PasswordActivity.this)) {
                        changePasswordTask();
                    } else {
                        AlertUtils.showToast(PasswordActivity.this, getResources().getString(R.string.error_no_internet));
                    }
                }
            }
        });

    }

    public boolean validation() {
        boolean isValid = true;

        if (etCurrentPswd.getText().toString().trim().equals("")) {
            tvErrCurrPswd.setVisibility(View.VISIBLE);
            isValid = false;
        } else {
            tvErrCurrPswd.setVisibility(View.GONE);
        }

        if (etNewPswd.getText().toString().trim().equals("")) {
            tvErrNewPswd.setVisibility(View.VISIBLE);
            isValid = false;
        } else {
            tvErrNewPswd.setVisibility(View.GONE);
        }

        if (etConPswd.getText().toString().trim().equals("")) {
            tvErrConPswd.setVisibility(View.VISIBLE);
            isValid = false;
        } else {
            if (!etConPswd.getText().toString().trim().equals(etNewPswd.getText().toString().trim())) {
                tvErrConPswd.setText("Password do not match!!");
                tvErrConPswd.setVisibility(View.VISIBLE);
                isValid = false;
            } else {
                tvErrConPswd.setVisibility(View.GONE);
            }
        }

//        if (etCustomQuestion.getVisibility() == View.VISIBLE) {
//            if (etCustomQuestion.getText().toString().trim().equals("")) {
//                tvErrCustomQuestion.setVisibility(View.VISIBLE);
//                isValid = false;
//            } else {
//                tvErrCustomQuestion.setVisibility(View.GONE);
//            }
//        }
//
//        if (etAnswer.getText().toString().trim().equals("")) {
//            tvErrAnswer.setVisibility(View.VISIBLE);
//            isValid = false;
//        } else {
//            tvErrAnswer.setVisibility(View.GONE);
//        }

        return isValid;
    }

    private void changePasswordTask() {
        final ProgressDialog progressDialog = AlertUtils.showProgressDialog(PasswordActivity.this, "Please wait...");
        VolleyHelper volleyHelper = VolleyHelper.getInstance(this);
        HashMap<String, String> postParams = volleyHelper.getPostParams();
        postParams.put("oldpassword", etCurrentPswd.getText().toString().trim());
        postParams.put("password1", etNewPswd.getText().toString().trim());
        postParams.put("password2", etConPswd.getText().toString().trim());

        volleyHelper.addVolleyRequestListeners(Api.getInstance().updatePasswordFromInside, Request.Method.POST, postParams, new VolleyHelper.VolleyHelperInterface() {
            @Override
            public void onSuccess(String response) {
                progressDialog.dismiss();
                try {
                    JSONObject res = new JSONObject(response);
                    JSONObject responseData = res.getJSONObject("ResponseData");
                    JSONObject details = responseData.getJSONObject("Details");
                    if (details.getString("appCode").equals(Api.APP_STATUS_OK)) {
//                        PreferenceHelper.getInstance(PasswordActivity.this).setPassword(etConPswd.getText().toString().trim());
                        Database.getInstance(PasswordActivity.this).updatePassword(PreferenceHelper.getInstance(PasswordActivity.this).getUsername(),
                                etConPswd.getText().toString().trim());
                        AlertUtils.simpleAlert(PasswordActivity.this, "Success",
                                "Your password has been successfully changed.", "OK", "", new
                                        AlertUtils.OnAlertButtonClickListener() {
                                            @Override
                                            public void onAlertButtonClick(boolean isPositiveButton) {
                                                if (isPositiveButton) {
                                                    onBackPressed();
                                                }
                                            }
                                        });
                    }
                } catch (JSONException e) {
                    Logger.e("catch changePasswordTask json ex", e.getMessage());
                    try {
                        JSONObject res = new JSONObject(response);
                        AlertUtils.showAppAlert(PasswordActivity.this, R.drawable.icon_not_accept,
                                res.getJSONObject("Details").getString("appCodeMessage"), "OK", null);
                    } catch (Exception e1) {
                        Logger.e("catch changePasswordTask ex ", e1.getMessage());
                    }
                }
            }

            @Override
            public void onError(String errorResponse, VolleyError volleyError) {
                progressDialog.dismiss();
                String errorMsg = "Unexpected error !!! Please try again with internet access.";
                try {
                    JSONObject errorObj = new JSONObject(errorResponse);
                    errorMsg = errorObj.getJSONObject("Details").getString("appCodeMessage");
                } catch (Exception e) {
                    Logger.e("changePasswordTask error res", e.getMessage() + " ");
                }
                AlertUtils.simpleAlert(PasswordActivity.this, "Error", errorMsg, "OK", "", null);
            }
        }, "changePasswordTask");
    }
}
