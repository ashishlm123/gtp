package com.emts.gtp.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.emts.gtp.Api;
import com.emts.gtp.R;
import com.emts.gtp.helper.AlertUtils;
import com.emts.gtp.helper.Logger;
import com.emts.gtp.helper.NetworkUtils;
import com.emts.gtp.helper.VolleyHelper;
import com.emts.gtp.model.OrderModel;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class OrderDetailsActivity extends BaseActivity {
    OrderModel orderModel;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_details);
        Button btnCancel = findViewById(R.id.btn_cancel);

        TextView tvOrderDetails, tvDate, tvId, tvOrderNo, tvOrderPrice, tvBookBy, tvXauWeight, tvProductType, tvAceBuySell, tvStatus;
        tvOrderDetails = findViewById(R.id.tv_order_details);
        tvDate = findViewById(R.id.tv_date);
        tvId = findViewById(R.id.tv_id);
        tvOrderNo = findViewById(R.id.tv_order_no);
        tvOrderPrice = findViewById(R.id.tv_price);
        tvBookBy = findViewById(R.id.tv_bookby);
        tvXauWeight = findViewById(R.id.tv_xau_weight);
        tvProductType = findViewById(R.id.tv_product_type);
        tvAceBuySell = findViewById(R.id.tv_ace_buy_sell);
        tvStatus = findViewById(R.id.tv_status);

        Intent intent = getIntent();
        if (intent == null) {
            return;
        }
        boolean isFutureOrder = intent.getBooleanExtra("isFutureOrder", false);
        Logger.e("isFutureOrder", String.valueOf(isFutureOrder));
        if (isFutureOrder) {
            orderModel = (OrderModel) intent.getSerializableExtra("futureOrder");

            tvOrderDetails.setText("Future Order Details");
            tvDate.setText(orderModel.getOrderDate());
            tvId.setText(orderModel.getOrderId());
            if (orderModel.getGrossTotal().equals("")) {
                tvOrderNo.setText("-");
            } else {
                tvOrderNo.setText("RM" + " " + String.format("%,.2f", Double.parseDouble(orderModel.getGrossTotal())));
            }
            tvOrderPrice.setText(orderModel.getPrice());
            tvBookBy.setText(orderModel.getBookBy());
            tvXauWeight.setText(orderModel.getXauWeight());
            tvProductType.setText(orderModel.getProductType());
            tvAceBuySell.setText(orderModel.getAceBuySell());
            tvStatus.setText(orderModel.getOrderStatus());
            if (orderModel.getOrderStatus().equalsIgnoreCase("Queued")) {
                btnCancel.setVisibility(View.VISIBLE);
            } else {
                btnCancel.setVisibility(View.GONE);
            }


            btnCancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (NetworkUtils.isInNetwork(getApplicationContext())) {
                        AlertUtils.simpleAlert(OrderDetailsActivity.this, "Cancel Order",
                                "Are you sure you want to cancel the order?", "OK", "Cancel",
                                new AlertUtils.OnAlertButtonClickListener() {
                                    @Override
                                    public void onAlertButtonClick(boolean isPositiveButton) {
                                        if (isPositiveButton) {
                                            if (orderModel.getCustomerOrderType().equalsIgnoreCase("sell")) {
//                                                cancelBidTask();
                                                cancelAskTask();
                                            } else {
//                                                cancelAskTask();
                                                cancelBidTask();
                                            }
                                        }
                                    }
                                });
                    } else {
                        AlertUtils.showSnack(OrderDetailsActivity.this, view, getResources().getString(R.string.error_no_internet));
                    }
                }
            });
        } else {
            btnCancel.setVisibility(View.GONE);
            orderModel = (OrderModel) intent.getSerializableExtra("spotOrder");

            tvOrderDetails.setText("Spot Order Details");
            tvDate.setText(orderModel.getOrderDate());
            tvId.setText(orderModel.getOrderId());
            if (orderModel.getGrossTotal().equals("")) {
                tvOrderNo.setText("-");
            } else {
                tvOrderNo.setText("RM" + " " + String.format("%,.2f", Double.parseDouble(orderModel.getGrossTotal())));
            }
            tvOrderPrice.setText(orderModel.getPrice());
            tvBookBy.setText(orderModel.getBookBy());
            tvXauWeight.setText(orderModel.getXauWeight());
            tvProductType.setText(orderModel.getProductType());
            tvAceBuySell.setText(orderModel.getAceBuySell());
            tvStatus.setText(orderModel.getOrderStatus());
        }

        Toolbar toolbar = findViewById(R.id.toolbar);
        TextView toolbarTitle = toolbar.findViewById(R.id.custom_toolbar_title);
        if (isFutureOrder) {
            toolbarTitle.setText("Future Order Details");
        } else {
            toolbarTitle.setText("Spot Order Details");
        }
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });


    }

    private void cancelBidTask() {
        final ProgressDialog progressDialog = new ProgressDialog(OrderDetailsActivity.this);
        progressDialog.setTitle("Please Wait...");
        progressDialog.show();
        VolleyHelper vHelper = VolleyHelper.getInstance(this);
        HashMap<String, String> postParams = vHelper.getPostParams();

        postParams.put("bidOrderId", orderModel.getOrderId());

        vHelper.addVolleyRequestListeners(Api.getInstance().cancelBid, Request.Method.POST, postParams, new VolleyHelper.VolleyHelperInterface() {
            @Override
            public void onSuccess(String response) {
                try {
                    JSONObject res = new JSONObject(response);
                    JSONObject responseData = res.getJSONObject("ResponseData");
                    JSONObject details = responseData.getJSONObject("Details");
                    if (details.getString("appCode").equals(Api.APP_STATUS_OK)) {
                        progressDialog.dismiss();
                        AlertUtils.showToast(OrderDetailsActivity.this, "Order Successfully Cancelled.");
                        onBackPressed();
                    }
                } catch (JSONException e) {
                    progressDialog.dismiss();
                    Logger.e("cancelBidTask json ex ", e.getMessage());
                    try {
                        JSONObject res = new JSONObject(response);
                        AlertUtils.simpleAlert(OrderDetailsActivity.this, "Error !!!",
                                res.getString("appCodeMessage"), "OK", "", null);
                    } catch (JSONException e1) {
                        Logger.e("cancelBidTask ex in ex", e1.getMessage());
                    }
                }
            }

            @Override
            public void onError(String errorResponse, VolleyError volleyError) {
                progressDialog.dismiss();
                String errorMsg = "Unexpected error !!! Please try again with internet access.";
                try {
                    JSONObject errorObj = new JSONObject(errorResponse);
                    errorMsg = errorObj.getJSONObject("Details").getString("appCodeMessage");
                } catch (Exception e) {
                    Logger.e("cancelBidTask error res", e.getMessage() + " ");
                }
                AlertUtils.simpleAlert(OrderDetailsActivity.this, "Error !!!",
                        errorMsg, "OK", "", null);
            }
        }, "cancelBidTask");
    }

    private void cancelAskTask() {
        final ProgressDialog progressDialog = new ProgressDialog(OrderDetailsActivity.this);
        progressDialog.setTitle("Please Wait...");
        progressDialog.show();
        VolleyHelper vHelper = VolleyHelper.getInstance(this);
        HashMap<String, String> postParams = vHelper.getPostParams();

        postParams.put("askOrderId", orderModel.getOrderId());

        vHelper.addVolleyRequestListeners(Api.getInstance().cancelAsk, Request.Method.POST, postParams, new VolleyHelper.VolleyHelperInterface() {
            @Override
            public void onSuccess(String response) {
                try {
                    JSONObject res = new JSONObject(response);
                    JSONObject responseData = res.getJSONObject("ResponseData");
                    JSONObject details = responseData.getJSONObject("Details");
                    if (details.getString("appCode").equals(Api.APP_STATUS_OK)) {
                        progressDialog.dismiss();
                        AlertUtils.showToast(OrderDetailsActivity.this, "Order Successfully Cancelled.");
                        onBackPressed();
                    }
                } catch (JSONException e) {
                    progressDialog.dismiss();
                    Logger.e("cancelAskTask json ex ", e.getMessage());
                }
            }

            @Override
            public void onError(String errorResponse, VolleyError volleyError) {
                progressDialog.dismiss();
                String errorMsg = "Unexpected error !!! Please try again with internet access.";
                try {
                    JSONObject errorObj = new JSONObject(errorResponse);
                    errorMsg = errorObj.getJSONObject("Details").getString("appCodeMessage");
                } catch (Exception e) {
                    Logger.e("cancelAskTask error res", e.getMessage() + " ");
                }
                Logger.e("cancelAskTask ex ", errorMsg);
            }
        }, "cancelAskTask");
    }
}
