package com.emts.gtp.helper;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Build;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.text.Html;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.emts.gtp.R;

/**
 * Created by User on 2016-10-19.
 */

public class AlertUtils {
    public static interface OnAlertButtonClickListener {
        void onAlertButtonClick(boolean isPositiveButton);
    }

    public static void showAppAlert(Context context, int titleImage, String body, String buttonTitle, final OnAlertButtonClickListener listener) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View cusView = inflater.inflate(R.layout.alert_account_success, null, false);
        ImageView imgAlert = cusView.findViewById(R.id.alertImage);
        imgAlert.setImageResource(titleImage);
        TextView cusMessage = cusView.findViewById(R.id.alertMessage);
        cusMessage.setText(body);
        Button btnAlert = cusView.findViewById(R.id.btnAlert);
        btnAlert.setText(buttonTitle);

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setView(cusView);

        final AlertDialog dialog = builder.create();
        try {
//            WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
//            lp.copyFrom(dialog.getWindow().getAttributes());
//            lp.width = WindowManager.LayoutParams.MATCH_PARENT;
//            lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
//            dialog.getWindow().setAttributes(lp);

//            dialog.getWindow().setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT);

            int width = (int)(context.getResources().getDisplayMetrics().widthPixels);
//            int height = (int)(context.getResources().getDisplayMetrics().heightPixels*0.90);

            dialog.getWindow().setLayout(width, RelativeLayout.LayoutParams.WRAP_CONTENT);
        } catch (Exception e) {
        }

        btnAlert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                if (listener != null) {
                    listener.onAlertButtonClick(true);
                }
            }
        });
        dialog.show();
    }

    public static void simpleAlert(Context context, String title, String message, String positiveText,
                                   String negativeText, final OnAlertButtonClickListener listener) {
//        AlertDialog.Builder builder = new AlertDialog.Builder(context, R.style.AlertDialogCustom);
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
//        new ContextThemeWrapper(context, R.style.AlertDialogCustom);

        if (!TextUtils.isEmpty(title)) {
            builder.setTitle(title);
        }
        builder.setMessage(Html.fromHtml(message));

        if (!TextUtils.isEmpty(positiveText)) {
            builder.setPositiveButton(positiveText, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    if (listener != null) {
                        listener.onAlertButtonClick(true);
                    }
                }
            });
        }
        if (!TextUtils.isEmpty(negativeText)) {
            builder.setNegativeButton(negativeText, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    if (listener != null) {
                        listener.onAlertButtonClick(false);
                    }
                }
            });
        }
//        builder.show();
        Dialog dialog = builder.create();
        try {
            if (Build.VERSION.SDK_INT < 16) {
                dialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
            } else {
                View decorView = dialog.getWindow().getDecorView();
                int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
                decorView.setSystemUiVisibility(uiOptions);
            }
        } catch (Exception e) {
            Logger.e("showAlertMessage noStatus", e.getMessage() + " ");
        }
        dialog.show();
    }


    public static void simpleAlert(Context context, boolean cancelable, String title, String message, String positiveText,
                                   String negativeText, final OnAlertButtonClickListener listener) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context, R.style.AlertDialogCustom);
//        new ContextThemeWrapper(context, R.style.AlertDialogCustom);
        if (!TextUtils.isEmpty(title)) {
            builder.setTitle(title);
        }
        builder.setCancelable(cancelable);
        builder.setMessage(Html.fromHtml(message));
        if (!TextUtils.isEmpty(positiveText)) {
            builder.setPositiveButton(positiveText, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    if (listener != null) {
                        listener.onAlertButtonClick(true);
                    }
                }
            });
        }
        if (!TextUtils.isEmpty(negativeText)) {
            builder.setNegativeButton(negativeText, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    if (listener != null) {
                        listener.onAlertButtonClick(false);
                    }
                }
            });
        }
        builder.show();
    }

    public static void simpleAlert(Context context, String title, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context, R.style.AlertDialogCustom);
        if (!TextUtils.isEmpty(title)) {
            builder.setTitle(title);
        }
        builder.setMessage(Html.fromHtml(message));
        builder.show();
    }

    public static void showSnack(Context context, View view, String message) {
        if (TextUtils.isEmpty(message)) return;
        Snackbar snackbar = Snackbar.make(view, Html.fromHtml(message), Snackbar.LENGTH_LONG);
        View snackBarView = snackbar.getView();
        TextView tv = snackBarView.findViewById(android.support.design.R.id.snackbar_text);
        tv.setTextColor(Color.WHITE);
        tv.setGravity(Gravity.CENTER);
        snackBarView.setBackgroundColor(ContextCompat.getColor(context,
                R.color.mainButtonColor));
        snackbar.show();
    }

    public static void showToast(Context context, String message) {
        try {
            Toast.makeText(context, Html.fromHtml(message), Toast.LENGTH_SHORT).show();
        } catch (Exception e) {
            Logger.e("alertUtils showToast ex", e.getMessage() + "");
        }
    }

    public static ProgressDialog showProgressDialog(Context context, String message) {
        ProgressDialog pDialog = new ProgressDialog(context, R.style.AlertDialogCustom);
        pDialog.setCanceledOnTouchOutside(false);
        pDialog.setMessage(message);
        pDialog.show();
        return pDialog;
    }

    public static void hideInputMethod(Context context, View view) {
        try {
            InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
            if (imm != null) {
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
            }
        } catch (Exception e) {
        }
    }

    public static void showInputMethod(Context context, View view) {
        try {
            InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
            if (imm != null) {
                imm.showSoftInput(view, InputMethodManager.SHOW_IMPLICIT);
            }
        } catch (Exception e) {
        }
    }

//
//    public static LoadToast showToastProgress(Context context, String message) {
//        LoadToast lProgress = new LoadToast(context);
//        lProgress.setText(message);
//        lProgress.setProgressColor(context.getResources().getColor(R.color.colorPrimaryDark));
//        lProgress.setTextColor(context.getResources().getColor(R.color.green));
//        lProgress.setBackgroundColor(context.getResources().getColor(R.color.appGrayDivider));
//        // Calculate ActionBar's height
//        TypedValue tv = new TypedValue();
//        if (context.getTheme().resolveAttribute(android.R.attr.actionBarSize, tv, true)) {
//            int actionBarHeight = TypedValue.complexToDimensionPixelSize(tv.data,
//                    context.getResources().getDisplayMetrics());
//            lProgress.setTranslationY(actionBarHeight + 100);
//        }
//        return lProgress;
//    }
}
