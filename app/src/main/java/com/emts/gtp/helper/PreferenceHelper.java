package com.emts.gtp.helper;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.text.TextUtils;

import java.util.Map;
import java.util.Set;

/**
 * Created by User on 2016-03-04.
 */
public class PreferenceHelper implements SharedPreferences {
    public static final String SHOULD_SHOW_SESSION_PIN_DIALOG = "show_dialog";
    private static final String APP_SHARED_PREFS = "gtp_prefs";
    public static final String FCM_TOKEN_UPDATED = "fcm_token_update";
    public static final String IS_LOGIN = "is_login";
    private static final String TOKEN = "app_user_token";
    private static final String APP_USER_ID = "app_user_id";
    public static final String SESSION_PIN_CODE = "session_pin_code";

    //1 time prefs
    public static final String FIRST_TNC = "first_tnc";
    public static final String FIRST_GET_STARTED = "first_get_started";
//    public static final String IS_QUICK_TOUCH_ENABLE = "quick_touch_enable";
    public static final String TIME_OUT_IN_SEC = "time_out_sec";
    public static final String USERNAME = "username";
    public static final String PROFILE_PIC = "profile_pic";
//    private static final String PASSWORD = "password";

    private SharedPreferences prefs;
    private static PreferenceHelper helper;

    public static PreferenceHelper getInstance(Context context) {
        if (helper != null) {
            return helper;
        } else {
            helper = new PreferenceHelper(context);
            return helper;
        }

    }

    private PreferenceHelper(Context context) {
        prefs = context.getSharedPreferences(APP_SHARED_PREFS, Context.MODE_PRIVATE);
    }

    public SharedPreferences getPrefs() {
        return prefs;
    }

    @Override
    public Map<String, ?> getAll() {
        return prefs.getAll();
    }

    @Override
    public String getString(String s, String s2) {
        return prefs.getString(s, s2);
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    @Override
    public Set<String> getStringSet(String s, Set<String> strings) {
        return prefs.getStringSet(s, strings);
    }

    @Override
    public int getInt(String s, int i) {
        return prefs.getInt(s, i);
    }

    @Override
    public long getLong(String s, long l) {
        return prefs.getLong(s, l);
    }

    @Override
    public float getFloat(String s, float v) {
        return prefs.getFloat(s, v);
    }

    @Override
    public boolean getBoolean(String s, boolean b) {
        return prefs.getBoolean(s, b);
    }

    @Override
    public boolean contains(String s) {
        return prefs.contains(s);
    }

    @Override
    public Editor edit() {
        return prefs.edit();
    }

    @Override
    public void registerOnSharedPreferenceChangeListener(OnSharedPreferenceChangeListener onSharedPreferenceChangeListener) {

    }

    @Override
    public void unregisterOnSharedPreferenceChangeListener(OnSharedPreferenceChangeListener onSharedPreferenceChangeListener) {

    }

    public boolean isLogin() {
        return prefs.getBoolean(IS_LOGIN, false);
    }

    public String getUserId() {
        return prefs.getString(APP_USER_ID, "-1");
    }

    public String getToken() {
        return prefs.getString(TOKEN, "");
    }

    public boolean isSessionPinSet() {
        return !TextUtils.isEmpty(prefs.getString(SESSION_PIN_CODE, ""));
    }

    public String getSessionPinCode() {
        return prefs.getString(SESSION_PIN_CODE, "");
    }

    public String getUsername() {
//        return prefs.getString(USERNAME, "joeystudent0001");
        return prefs.getString(USERNAME, getDefaultUserName());
    }

//    public String getPassword() {
////        return prefs.getString(PASSWORD, "1111");
//        return prefs.getString(PASSWORD, getDefaultPassword());
//    }

    public void setUsername(String username) {
        if (TextUtils.isEmpty(username)) {
            //set defaults for now
            prefs.edit().putString(USERNAME, getDefaultUserName()).commit();
            return;
        }
        prefs.edit().putString(USERNAME, username).commit();
    }

//    public void setPassword(String password) {
//        if (TextUtils.isEmpty(password)) {
//            //set defaults for now
//            prefs.edit().putString(PASSWORD, getDefaultPassword()).commit();
//            return;
//        }
//        prefs.edit().putString(PASSWORD, password).commit();
//    }


    //Default User Sessions

    /**
     * Pre- Shared User Session
     *
     * @return username or screen name
     */
    public String getDefaultUserName() {
        return "mobilechgpwd";
    }

    /**
     * Pre- Shared User Session
     *
     * @return password
     */
    public String getDefaultPassword() {
        return "8888";
    }

}
