package com.emts.gtp.helper;

import android.text.InputFilter;
import android.text.Spanned;
import android.text.TextUtils;
import android.widget.EditText;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class InputHelper {

    public static void limitAfterDecimal(EditText etInput, int limitAfterDecimal) {
        etInput.setFilters(new InputFilter[]{new DecimalDigitsInputFilter(2)});
    }

    public static class DecimalDigitsInputFilter implements InputFilter {

        Pattern mPattern;

        public DecimalDigitsInputFilter(int digitsAfterZero) {
//            mPattern=Pattern.compile("[0-9]+((\\.[0-9]{0," + (digitsAfterZero-1) + "})?)||(\\.)?");
            mPattern = Pattern.compile("[0-9]+(\\.\\d{0," + digitsAfterZero + "})?$");
        }

        @Override
        public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
            CharSequence match = TextUtils.concat(dest.subSequence(0, dstart),
                    source.subSequence(start, end), dest.subSequence(dend, dest.length()));
            Matcher matcher = mPattern.matcher(match);
//            Matcher matcher=mPattern.matcher(dest);
            if (!matcher.matches())
                return "";
            return null;
        }

    }
}
