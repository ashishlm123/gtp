package com.emts.gtp;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.design.internal.BottomNavigationItemView;
import android.support.design.internal.BottomNavigationMenuView;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.CheckedTextView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.emts.gtp.activity.AccountListActivity;
import com.emts.gtp.activity.BaseActivity;
import com.emts.gtp.fragment.AboutUs;
import com.emts.gtp.fragment.FutureOrder;
import com.emts.gtp.fragment.FutureOrderComplete;
import com.emts.gtp.fragment.FutureOrderConfirmation;
import com.emts.gtp.fragment.LimitsFragment;
import com.emts.gtp.fragment.MyAccountFragment;
import com.emts.gtp.fragment.Notifications;
import com.emts.gtp.fragment.PriceChart;
import com.emts.gtp.fragment.SpotOrder;
import com.emts.gtp.fragment.SpotOrderComplete;
import com.emts.gtp.fragment.SpotOrderConfirmation;
import com.emts.gtp.fragment.TnCFragment;
import com.emts.gtp.fragment.Tradebook;
import com.emts.gtp.fragment.UnfulfillPo;
import com.emts.gtp.helper.PreferenceHelper;
import com.emts.gtp.helper.VolleyHelper;

import java.lang.reflect.Field;

public class MainActivity extends BaseActivity {
    DrawerLayout drawer;
    FragmentManager fm;
    Toolbar toolbar;
    PreferenceHelper prefsHelper;
    TextView toolbarTitle;
    NavigationView navigationView;
    boolean isLogin;
    CheckedTextView mPreviousMenuItem;

    CheckedTextView menuOrderDashBoard, menuLimits, menuTradeBook, menuUnfulfillPO, menuPriceChart,
            menuNotifications, menuAboutUs, menuTnC, menuLogout;
    LinearLayout layBottomNavView;
    TextView prevSelectedText;
    ImageView prevSelectedButton, profilePic;
    TextView tvSpotOrder, tvFutureOrder, tvDailyLimits, tvTradebook;
    ImageView btnSpotOrder, btnFutureOrder, btnDailyLimits, btnTradeook;
    Typeface helveticaRegular, helveticaBold;

    private static final int TAB_SPOT_ORDER = 0;
    private static final int TAB_FUTURE_ORDER = 1;
    private static final int TAB_DAILY_LIMITS = 2;
    private static final int TAB_TRADEBOOK = 3;
    private static final int TAB_OTHERS = 4;

    int selectedTabId = 0;

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        //Save the fragment's instance
        fm.putFragment(outState, "myFragmentName", fm.findFragmentById(R.id.content_frame));
        outState.putInt("selectedTabId", selectedTabId);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        prefsHelper = PreferenceHelper.getInstance(this);

        drawer = findViewById(R.id.drawer_layout);
        navigationView = findViewById(R.id.nav_view);

        toolbar = findViewById(R.id.toolbar);
        toolbarTitle = findViewById(R.id.custom_toolbar_title);

        fm = getSupportFragmentManager();

        if (prefsHelper.isLogin()) {
            isLogin = true;
        }

        ActionBarDrawerToggle drawerToggle = new ActionBarDrawerToggle(this, drawer, toolbar,
                R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(drawerToggle);
        drawerToggle.syncState();
        toolbar.setNavigationIcon(R.drawable.btn_sidemenu);

        navigationView = findViewById(R.id.nav_view);
//        navigationView.inflateMenu(R.menu.menu_main_drawer);
        setupNavigationView();

        layBottomNavView = findViewById(R.id.bottom_nav_view);

        tvSpotOrder = layBottomNavView.findViewById(R.id.tv_spot_order);
        tvFutureOrder = layBottomNavView.findViewById(R.id.tv_future_order);
        tvDailyLimits = layBottomNavView.findViewById(R.id.tv_daily_limits);
        tvTradebook = layBottomNavView.findViewById(R.id.tv_tradebook);

        btnSpotOrder = layBottomNavView.findViewById(R.id.btn_spot_order);
        btnFutureOrder = layBottomNavView.findViewById(R.id.btn_future_order);
        btnDailyLimits = layBottomNavView.findViewById(R.id.btn_daily_limits);
        btnTradeook = layBottomNavView.findViewById(R.id.btn_tradebook);

        helveticaRegular = ResourcesCompat.getFont(this, R.font.helvetica_regular);
        helveticaBold = ResourcesCompat.getFont(this, R.font.helvetica_bold);

        if (getIntent() != null) {
            if (getIntent().getBooleanExtra("isNotification", false)) {
                fm.beginTransaction().replace(R.id.content_frame, new Notifications(),
                        Notifications.class.getSimpleName()).commit();
                return;
            }
        }
        if (savedInstanceState != null) {
            //Restore the fragment's instance
            Fragment fragment = fm.getFragment(savedInstanceState, "myFragmentName");

            fm.beginTransaction().replace(R.id.content_frame, fragment, fragment.getClass().getSimpleName()).commit();
            selectBottomTab(savedInstanceState.getInt("selectedTabId", 0));
        } else {
            fm.beginTransaction().replace(R.id.content_frame, new SpotOrder(), SpotOrder.class.getSimpleName()).commit();
            selectBottomTab(TAB_SPOT_ORDER);
        }
        prefsHelper.edit().putBoolean("isLoggedIn", true).apply();
    }

    private void setupNavigationView() {
        LinearLayout views = navigationView.findViewById(R.id.nav_header_main);
        TextView tvUserName = views.findViewById(R.id.tv_username);
        tvUserName.setText(prefsHelper.getUsername());
        views.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openMyAccountFrag();
            }
        });
        profilePic = views.findViewById(R.id.profile_icon);

        menuOrderDashBoard = navigationView.findViewById(R.id.nav_order_dashboard);
        menuOrderDashBoard.setText("Order Dashboard");
        menuOrderDashBoard.setCompoundDrawablesWithIntrinsicBounds(R.drawable.icon_order_dashboard, 0, 0, 0);
        menuOrderDashBoard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onNavigationItemSelected(view);
            }
        });
        menuLimits = navigationView.findViewById(R.id.nav_limits);
        menuLimits.setText("Limits");
        menuLimits.setCompoundDrawablesWithIntrinsicBounds(R.drawable.icon_limits, 0, 0, 0);
        menuLimits.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onNavigationItemSelected(view);
            }
        });
        menuTradeBook = navigationView.findViewById(R.id.nav_tradebook);
        menuTradeBook.setText("Tradebook");
        menuTradeBook.setCompoundDrawablesWithIntrinsicBounds(R.drawable.icon_tradebook, 0, 0, 0);
        menuTradeBook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onNavigationItemSelected(view);
            }
        });
        menuUnfulfillPO = navigationView.findViewById(R.id.nav_unfulfilpro);
        menuUnfulfillPO.setText("Unfulfill PO");
        menuUnfulfillPO.setCompoundDrawablesWithIntrinsicBounds(R.drawable.icon_unfulfill_pro, 0, 0, 0);
        menuUnfulfillPO.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onNavigationItemSelected(view);
            }
        });
        menuPriceChart = navigationView.findViewById(R.id.nav_price_chart);
        menuPriceChart.setText("Price Chart");
        menuPriceChart.setCompoundDrawablesWithIntrinsicBounds(R.drawable.icon_price_chart, 0, 0, 0);
        menuPriceChart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onNavigationItemSelected(view);
            }
        });
        menuNotifications = navigationView.findViewById(R.id.nav_notification);
        menuNotifications.setText("Notifications");
        menuNotifications.setCompoundDrawablesWithIntrinsicBounds(R.drawable.icon_notification, 0, 0, 0);
        menuNotifications.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onNavigationItemSelected(view);
            }
        });
        menuAboutUs = navigationView.findViewById(R.id.nav_about_us);
        menuAboutUs.setText("About Us");
        menuAboutUs.setCompoundDrawablesWithIntrinsicBounds(R.drawable.icon_about_us, 0, 0, 0);
        menuAboutUs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onNavigationItemSelected(view);
            }
        });
        menuTnC = navigationView.findViewById(R.id.nav_tnc);
        menuTnC.setText("Terms and Conditions");
        menuTnC.setCompoundDrawablesWithIntrinsicBounds(R.drawable.icon_terms_and_conditions, 0, 0, 0);
        menuTnC.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onNavigationItemSelected(view);
            }
        });
        menuLogout = navigationView.findViewById(R.id.nav_logout);
        menuLogout.setText("Logout");
        menuLogout.setCompoundDrawablesWithIntrinsicBounds(R.drawable.icon_logout, 0, 0, 0);
        menuLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onNavigationItemSelected(view);
            }
        });
    }

    private void openMyAccountFrag() {
        fm.beginTransaction().replace(R.id.content_frame, new MyAccountFragment(), MyAccountFragment.class.getSimpleName()).addToBackStack(null).commit();
        drawer.closeDrawer(GravityCompat.START);
        setToolbarTitle(false, "");
        selectBottomTab(TAB_OTHERS);
    }

    @Override
    protected void onResume() {
        super.onResume();
        String pp = prefsHelper.getString(PreferenceHelper.PROFILE_PIC, "");
        if (!pp.equalsIgnoreCase("null") && !TextUtils.isEmpty(pp)) {
//            Glide.with(this).load(pp).asBitmap().centerCrop().into(new BitmapImageViewTarget(profilePic) {
//                @Override
//                protected void setResource(Bitmap resource) {
//                    RoundedBitmapDrawable circularBitmapDrawable =
//                            RoundedBitmapDrawableFactory.create(getResources(), resource);
//                    circularBitmapDrawable.setCircular(true);
//                    profilePic.setImageDrawable(circularBitmapDrawable);
//                }
//            });
//            Glide.with(this).load(pp).apply(RequestOptions().circleCrop()).into(profilePic);
            Glide.with(this).load(pp).apply(RequestOptions.circleCropTransform()).into(profilePic);
        } else {
            profilePic.setPadding(12, 12, 12, 12);
            profilePic.setImageResource(R.drawable.icon_user_icon_replacer);
        }
    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
            return;
        }
        if (fm.getBackStackEntryCount() > 0) {
            fm.popBackStack();
            setToolbarTitle(true, "");
        } else {
            VolleyHelper.getInstance(this).cancelAllRequests();
//            super.onBackPressed();
            prefsHelper.edit().putBoolean("isLoggedIn", false).apply();
            finish();
        }
    }

    @Override
    protected void onDestroy() {
        stopService(new Intent(getApplicationContext(), LivePriceSocketService.class));
        prefsHelper.edit().putBoolean("isLoggedIn", false).apply();
        super.onDestroy();
    }

    @Override
    protected void onStop() {
        stopService(new Intent(getApplicationContext(), LivePriceSocketService.class));
        super.onStop();
    }

    public void onNavigationItemSelected(View view) {
        Fragment fragment = null;
        Bundle bundle = new Bundle();

        if (mPreviousMenuItem != null) {
            mPreviousMenuItem.setChecked(false);
        }
        ((CheckedTextView) view).setChecked(true);
        mPreviousMenuItem = (CheckedTextView) view;

        switch (view.getId()) {
            case R.id.nav_order_dashboard:
                fragment = new SpotOrder();
                menuOrderDashBoard.setTypeface(helveticaBold);
                menuLimits.setTypeface(helveticaBold);
                menuOrderDashBoard.setTypeface(helveticaBold);
                menuOrderDashBoard.setTypeface(helveticaBold);
                menuOrderDashBoard.setTypeface(helveticaBold);
                menuOrderDashBoard.setTypeface(helveticaBold);
                menuOrderDashBoard.setTypeface(helveticaBold);
                menuOrderDashBoard.setTypeface(helveticaBold);

                selectBottomTab(TAB_SPOT_ORDER);
                break;
            case R.id.nav_limits:
                fragment = new LimitsFragment();
                selectBottomTab(TAB_DAILY_LIMITS);
                break;
            case R.id.nav_tradebook:
                fragment = new Tradebook();
                selectBottomTab(TAB_TRADEBOOK);
                break;
            case R.id.nav_unfulfilpro:
                fragment = new UnfulfillPo();
                selectBottomTab(TAB_OTHERS);
                break;
            case R.id.nav_price_chart:
                fragment = new PriceChart();
                selectBottomTab(TAB_OTHERS);
                break;
            case R.id.nav_notification:
                fragment = new Notifications();
                selectBottomTab(TAB_OTHERS);
                break;
            case R.id.nav_about_us:
                fragment = new AboutUs();
                selectBottomTab(TAB_OTHERS);
                break;
            case R.id.nav_tnc:
                fragment = new TnCFragment();
                selectBottomTab(TAB_OTHERS);
                break;
            case R.id.nav_logout:
                PreferenceHelper.getInstance(MainActivity.this).edit().putBoolean(PreferenceHelper.IS_LOGIN, false).apply();
                Intent intent1 = new Intent(getApplicationContext(), AccountListActivity.class);
                intent1.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent1);
                break;
        }
        if (fragment != null) {
            fm.beginTransaction().replace(R.id.content_frame, fragment, fragment.getClass().getSimpleName()).addToBackStack(null).commit();
            drawer.closeDrawer(GravityCompat.START);
            setToolbarTitle(false, "");
        }
    }

    public void setToolbarTitle(final boolean isBackPressed, final String title) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Fragment fragment = fm.findFragmentById(R.id.content_frame);
                if (fragment instanceof SpotOrder) {
                    toolbar.setNavigationIcon(R.drawable.btn_sidemenu);
                    toolbarTitle.setText("Spot Order");
                    if (isBackPressed) selectBottomTab(TAB_SPOT_ORDER);
                } else if (fragment instanceof LimitsFragment) {
                    toolbarTitle.setText("Limits");
                    if (isBackPressed) selectBottomTab(TAB_DAILY_LIMITS);
                } else if (fragment instanceof Tradebook) {
                    toolbar.setNavigationIcon(R.drawable.btn_sidemenu);
                    toolbarTitle.setText("Tradebook");
                    if (isBackPressed) selectBottomTab(TAB_TRADEBOOK);
                } else if (fragment instanceof FutureOrder) {
                    toolbar.setNavigationIcon(R.drawable.btn_sidemenu);
                    toolbarTitle.setText("Future Order");
                    if (isBackPressed) selectBottomTab(TAB_FUTURE_ORDER);
                } else if (fragment instanceof UnfulfillPo) {
                    toolbar.setNavigationIcon(R.drawable.btn_sidemenu);
                    toolbarTitle.setText("Unfulfill PO");
                    if (isBackPressed) selectBottomTab(TAB_OTHERS);
                } else if (fragment instanceof PriceChart) {
                    toolbar.setNavigationIcon(R.drawable.btn_sidemenu);
                    toolbarTitle.setText("Price Chart");
                    if (isBackPressed) selectBottomTab(TAB_OTHERS);
                } else if (fragment instanceof Notifications) {
                    toolbar.setNavigationIcon(R.drawable.btn_sidemenu);
                    toolbarTitle.setText("Notifications");
                    if (isBackPressed) selectBottomTab(TAB_OTHERS);
                } else if (fragment instanceof AboutUs) {
                    toolbar.setNavigationIcon(R.drawable.btn_sidemenu);
                    toolbarTitle.setText("About Us");
                    if (isBackPressed) selectBottomTab(TAB_OTHERS);
                } else if (fragment instanceof TnCFragment) {
                    toolbarTitle.setText("Terms and Conditions");
                    if (isBackPressed) selectBottomTab(TAB_OTHERS);
                } else if (fragment instanceof MyAccountFragment) {
                    toolbar.setNavigationIcon(R.drawable.btn_sidemenu);
                    toolbarTitle.setText("My Account");
                    if (isBackPressed) selectBottomTab(TAB_OTHERS);
                } else if (fragment instanceof SpotOrderConfirmation) {
                    toolbar.setNavigationIcon(null);
                    toolbarTitle.setText("Confirmation");
                    if (isBackPressed) selectBottomTab(TAB_OTHERS);
                } else if (fragment instanceof SpotOrderComplete) {
                    toolbar.setNavigationIcon(null);
                    toolbarTitle.setText("Spot Order Complete");
                    if (isBackPressed) selectBottomTab(TAB_OTHERS);
                } else if (fragment instanceof FutureOrderConfirmation) {
                    toolbar.setNavigationIcon(null);
                    toolbarTitle.setText("Future Order Confirmation");
                    if (isBackPressed) selectBottomTab(TAB_OTHERS);
                } else if (fragment instanceof FutureOrderComplete) {
                    toolbar.setNavigationIcon(null);
                    toolbarTitle.setText(title);
                    if (isBackPressed) selectBottomTab(TAB_OTHERS);
                }
            }
        }, 200);
    }

    @SuppressLint("RestrictedApi")
    private void removeShiftMode(BottomNavigationView view) {
        BottomNavigationMenuView menuView = (BottomNavigationMenuView) view.getChildAt(0);
        try {
            Field shiftingMode = menuView.getClass().getDeclaredField("mShiftingMode");
            shiftingMode.setAccessible(true);
            shiftingMode.setBoolean(menuView, false);
            shiftingMode.setAccessible(false);
            for (int i = 0; i < menuView.getChildCount(); i++) {
                BottomNavigationItemView item = (BottomNavigationItemView) menuView.getChildAt(i);
                item.setShiftingMode(false);
                // set once again checked value, so view will be updated
                item.setChecked(item.getItemData().isChecked());
            }
        } catch (NoSuchFieldException e) {
            Log.e("ERROR NO SUCH FIELD", "Unable to get shift mode field");
        } catch (IllegalAccessException e) {
            Log.e("ERROR ILLEGAL ALG", "Unable to change value of shift mode");
        }
    }

    public void openFragment(Fragment fragment, boolean addToBackStack) {
        assert getSupportFragmentManager() != null;
        fm = getSupportFragmentManager();
        if (addToBackStack) {
            fm.beginTransaction().replace(R.id.content_frame, fragment, fragment.getClass().getSimpleName()).addToBackStack(null).commit();
        } else {
            fm.beginTransaction().replace(R.id.content_frame, fragment, fragment.getClass().getSimpleName()).commit();
        }
        setToolbarTitle(false, "");
    }

    private void selectNavMenu(View view) {
        if (mPreviousMenuItem != null) {
            mPreviousMenuItem.setChecked(false);
        }
        if (view == null)
            return;
        ((CheckedTextView) view).setChecked(true);
        mPreviousMenuItem = (CheckedTextView) view;
    }

    private void selectBottomTab(int tabId) {
        selectedTabId = tabId;
        switch (tabId) {
            case TAB_SPOT_ORDER:
                if (prevSelectedText != null)
                    prevSelectedText.setTextColor(Color.BLACK);
                if (prevSelectedButton != null)
                    prevSelectedButton.setColorFilter(R.color.black);

                tvSpotOrder.setTextColor(getResources().getColor(R.color.colorPrimary));
                btnSpotOrder.setColorFilter(getResources().getColor(R.color.colorPrimary));
                prevSelectedText = tvSpotOrder;
                prevSelectedButton = btnSpotOrder;
                break;

            case TAB_FUTURE_ORDER:
                if (prevSelectedText != null)
                    prevSelectedText.setTextColor(Color.BLACK);
                if (prevSelectedButton != null)
                    prevSelectedButton.setColorFilter(Color.BLACK);
                tvFutureOrder.setTextColor(getResources().getColor(R.color.colorPrimary));
                btnFutureOrder.setColorFilter(getResources().getColor(R.color.colorPrimary));
                prevSelectedText = tvFutureOrder;
                prevSelectedButton = btnFutureOrder;
                break;

            case TAB_DAILY_LIMITS:
                if (prevSelectedText != null)
                    prevSelectedText.setTextColor(Color.BLACK);
                if (prevSelectedButton != null)
                    prevSelectedButton.setColorFilter(Color.BLACK);
                tvDailyLimits.setTextColor(getResources().getColor(R.color.colorPrimary));
                btnDailyLimits.setColorFilter(getResources().getColor(R.color.colorPrimary));
                prevSelectedText = tvDailyLimits;
                prevSelectedButton = btnDailyLimits;
                break;

            case TAB_TRADEBOOK:
                if (prevSelectedText != null)
                    prevSelectedText.setTextColor(Color.BLACK);
                if (prevSelectedButton != null)
                    prevSelectedButton.setColorFilter(Color.BLACK);
                tvTradebook.setTextColor(getResources().getColor(R.color.colorPrimary));
                btnTradeook.setColorFilter(getResources().getColor(R.color.colorPrimary));
                prevSelectedText = tvTradebook;
                prevSelectedButton = btnTradeook;
                break;

            case TAB_OTHERS:
                tvSpotOrder.setTextColor(getResources().getColor(R.color.black));
                tvDailyLimits.setTextColor(getResources().getColor(R.color.black));
                tvFutureOrder.setTextColor(getResources().getColor(R.color.black));
                tvTradebook.setTextColor(getResources().getColor(R.color.black));

                btnSpotOrder.setColorFilter(getResources().getColor(R.color.black));
                btnFutureOrder.setColorFilter(getResources().getColor(R.color.black));
                btnDailyLimits.setColorFilter(getResources().getColor(R.color.black));
                btnTradeook.setColorFilter(getResources().getColor(R.color.black));
                break;
        }
    }

    public void bottomNavItemClick(View view) {
        Fragment fragment = null;
        Intent intent = null;

        switch (view.getId()) {
            case R.id.nav_order_dashboard:
                fragment = new SpotOrder();
                selectBottomTab(TAB_SPOT_ORDER);
                selectNavMenu(menuOrderDashBoard);
                break;

            case R.id.nav_future_order:
                fragment = new FutureOrder();
                selectBottomTab(TAB_FUTURE_ORDER);
                selectNavMenu(null);
                break;

            case R.id.nav_daily_limits:
                fragment = new LimitsFragment();
                selectBottomTab(TAB_DAILY_LIMITS);
                selectNavMenu(menuLimits);
                break;

            case R.id.nav_tradebook:
                fragment = new Tradebook();
                selectBottomTab(TAB_TRADEBOOK);
                selectNavMenu(menuTradeBook);
                break;
        }

        if (fragment != null) {
            fm.beginTransaction().replace(R.id.content_frame, fragment, fragment.getClass().getSimpleName())
                    .addToBackStack(null)
                    .commit();
            setToolbarTitle(false, "");
        }
    }


}
