package com.emts.gtp;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.emts.gtp.helper.PreferenceHelper;

public class BootCompleteReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        if (action == null) return;
        if (action.equals(Intent.ACTION_BOOT_COMPLETED)) {
            PreferenceHelper preferenceHelper = PreferenceHelper.getInstance(context);
            preferenceHelper.edit().putString(PreferenceHelper.SESSION_PIN_CODE, "").apply();
        }
    }
}