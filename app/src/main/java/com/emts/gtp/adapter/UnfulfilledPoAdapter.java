package com.emts.gtp.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.emts.gtp.R;
import com.emts.gtp.activity.UnfulfilledPODetailsActivity;
import com.emts.gtp.model.UnfulfilledPOModel;

import java.util.ArrayList;

public class UnfulfilledPoAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context context;
    private ArrayList<UnfulfilledPOModel> unfulfilledPOLists;

    public UnfulfilledPoAdapter(Context context, ArrayList<UnfulfilledPOModel> unfulfilledPOLists) {
        this.context = context;
        this.unfulfilledPOLists = unfulfilledPOLists;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_unfulfillpo, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        ViewHolder viewHolder = (ViewHolder) holder;
        UnfulfilledPOModel unfulfilledPO = unfulfilledPOLists.get(position);
        viewHolder.tvPoNo.setText(unfulfilledPO.getPoNo());
        viewHolder.tvPoDate.setText(unfulfilledPO.getPoDate());
        viewHolder.tvVendorCode.setText(unfulfilledPO.getPoVendorCode());
        viewHolder.tvProduct.setText(unfulfilledPO.getPoProduct());
        viewHolder.tvPoQty.setText(unfulfilledPO.getPoQty());
        viewHolder.tvUnfulfillQty1.setText(unfulfilledPO.getPoUnfulfillQty_1());
        viewHolder.tvDraftGrnQty_2.setText(unfulfilledPO.getDraftGrnQty_2());
        viewHolder.tvBalance_1_2.setText(unfulfilledPO.getBalance_1_2());
        viewHolder.tvGtpBookingNo.setText(unfulfilledPO.getGtpBookingNo());
    }

    @Override
    public int getItemCount() {
        return unfulfilledPOLists.size();
    }

    private class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvPoNo, tvPoDate, tvVendorCode, tvProduct, tvPoQty,
                tvUnfulfillQty1, tvDraftGrnQty_2, tvBalance_1_2, tvGtpBookingNo;

        ViewHolder(View itemView) {
            super(itemView);

            tvPoNo = itemView.findViewById(R.id.tv_po_num);
            tvPoDate = itemView.findViewById(R.id.tv_po_date);
            tvVendorCode = itemView.findViewById(R.id.tv_vendor_code);
            tvProduct = itemView.findViewById(R.id.tv_product);
            tvPoQty = itemView.findViewById(R.id.tv_po_qty);
            tvUnfulfillQty1 = itemView.findViewById(R.id.tv_unfulfill_qty1);
            tvDraftGrnQty_2 = itemView.findViewById(R.id.tv_draft_grn_qty);
            tvBalance_1_2 = itemView.findViewById(R.id.tv_one_two_balance);
//            tvPoUnitPrice = itemView.findViewById(R.id.tv_po_unit_price);
//            tvPoAmt = itemView.findViewById(R.id.tv_po_amt);
//            tvPoGst = itemView.findViewById(R.id.tv_po_gst);
//            tvPoTotal = itemView.findViewById(R.id.tv_po_total);
//            tvGrnRef = itemView.findViewById(R.id.tv_grn_ref);
            tvGtpBookingNo = itemView.findViewById(R.id.tv_gtp_booking_no);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(context, UnfulfilledPODetailsActivity.class);
                    intent.putExtra("unfulfilledPo", unfulfilledPOLists.get(getLayoutPosition()));
                    context.startActivity(intent);
                }
            });
        }
    }
}
