package com.emts.gtp.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.emts.gtp.R;
import com.emts.gtp.activity.OrderDetailsActivity;
import com.emts.gtp.model.OrderModel;

import java.util.ArrayList;

public class SpotOrderAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context context;
    private ArrayList<OrderModel> spotOrderLists;

    public SpotOrderAdapter(Context context, ArrayList<OrderModel> spotOrderLists) {
        this.context = context;
        this.spotOrderLists = spotOrderLists;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_order, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        ViewHolder viewHolder = (ViewHolder) holder;
        OrderModel orderModel = spotOrderLists.get(position);

        viewHolder.tvDate.setText(orderModel.getOrderDate());
        viewHolder.tvOrderId.setText(orderModel.getOrderId());

        if (orderModel.getGrossTotal().equals("")) {
            viewHolder.tvOrderNo.setText("-");
        } else {
            viewHolder.tvOrderNo.setText("RM" + " " + String.format("%,.2f", Double.parseDouble(orderModel.getGrossTotal())));
        }
        viewHolder.tvOrderPrice.setText(orderModel.getPrice());
        viewHolder.tvOrderStatus.setText(orderModel.getOrderStatus());
    }

    @Override
    public int getItemCount() {
        return spotOrderLists.size();
    }

    private class ViewHolder extends RecyclerView.ViewHolder {
        ImageView btnView;
        TextView tvDate, tvOrderId, tvOrderNo, tvOrderPrice, tvOrderStatus;

        ViewHolder(View itemView) {
            super(itemView);
            btnView = itemView.findViewById(R.id.btn_view);

            tvDate = itemView.findViewById(R.id.tv_date);
            tvOrderId = itemView.findViewById(R.id.tv_id);
            tvOrderNo = itemView.findViewById(R.id.tv_order_no);
            tvOrderPrice = itemView.findViewById(R.id.tv_price);
            tvOrderStatus = itemView.findViewById(R.id.tv_status);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(context, OrderDetailsActivity.class);
                    intent.putExtra("isFutureOrder", false);
                    intent.putExtra("spotOrder", spotOrderLists.get(getLayoutPosition()));

                    context.startActivity(intent);
                }
            });
        }
    }
}
