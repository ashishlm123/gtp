package com.emts.gtp.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;

import com.emts.gtp.R;
import com.emts.gtp.model.Product;

import java.util.ArrayList;

public class ProductAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context context;
    private ArrayList<Product> productLists;

    public ProductAdapter(Context context, ArrayList<Product> productLists) {
        this.context = context;
        this.productLists = productLists;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_product_lists, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        ViewHolder viewHolder = (ViewHolder) holder;
        Product product = productLists.get(position);
        if(product.isSelected()){
            viewHolder.cbProductName.setChecked(true);
        }else{
            viewHolder.cbProductName.setChecked(false);
        }
//        viewHolder.cbProductName.setText(product.getpName());
        viewHolder.cbProductName.setText(product.getpCode());
    }

    @Override
    public int getItemCount() {
        return productLists.size();
    }

    private class ViewHolder extends RecyclerView.ViewHolder {
        CheckBox cbProductName;

        ViewHolder(View itemView) {
            super(itemView);

            cbProductName = itemView.findViewById(R.id.product_name);
            cbProductName.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    productLists.get(getLayoutPosition()).setSelected(b);
                }
            });
        }
    }
}
