package com.emts.gtp.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.emts.gtp.R;
import com.emts.gtp.activity.NotificationDetailsActivity;
import com.emts.gtp.model.NotificationModel;

import java.util.ArrayList;

public class NotificationAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context context;
    private ArrayList<NotificationModel> notificationLists;

    public NotificationAdapter(Context context, ArrayList<NotificationModel> notificationLists) {
        this.context = context;
        this.notificationLists = notificationLists;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_notification, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        ViewHolder viewHolder = (ViewHolder) holder;
        NotificationModel notificationModel = notificationLists.get(position);
        viewHolder.notiTitle.setText(notificationModel.getNotiTitle());
        viewHolder.notiBody.setText(Html.fromHtml(notificationModel.getNotiBody()));
        viewHolder.notiDate.setText(notificationModel.getNotiDate());
    }

    @Override
    public int getItemCount() {
        return notificationLists.size();
    }

    private class ViewHolder extends RecyclerView.ViewHolder {
        TextView notiTitle, notiBody, notiDate;

        ViewHolder(View itemView) {
            super(itemView);
            notiTitle = itemView.findViewById(R.id.tv_noti_title);
            notiBody = itemView.findViewById(R.id.tv_noti_body);
            notiDate = itemView.findViewById(R.id.noti_date);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(context, NotificationDetailsActivity.class);
                    intent.putExtra("notification", notificationLists.get(getLayoutPosition()));
                    context.startActivity(intent);
                }
            });
        }
    }
}
