package com.emts.gtp.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.emts.gtp.Database;
import com.emts.gtp.R;
import com.emts.gtp.activity.SignInActivity;
import com.emts.gtp.helper.AlertUtils;
import com.emts.gtp.model.Account;

import java.util.ArrayList;

public class AccountAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context context;
    private ArrayList<Account> accountList;
    private Database dbHelper;

    private boolean isSignIn = false;

    public void setSignIn(boolean signIn) {
        isSignIn = signIn;
    }

    public AccountAdapter(Context context, ArrayList<Account> accountList) {
        this.context = context;
        this.accountList = accountList;
        dbHelper = Database.getInstance(context);
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (isSignIn) {
            return new ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_account_view, parent, false));
        } else {
            return new ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_manage_account, parent, false));
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        ViewHolder viewHolder = (ViewHolder) holder;
        Account account = accountList.get(position);

        viewHolder.tvAccountName.setText(account.getAcName());
    }

    @Override
    public int getItemCount() {
        return accountList.size();
    }

    private class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvAccountName;
        Button btnRemove;

        private ViewHolder(View itemView) {
            super(itemView);

            tvAccountName = itemView.findViewById(R.id.accountName);

            if (isSignIn) {
                tvAccountName.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(context, SignInActivity.class);
                        intent.putExtra("account", accountList.get(getLayoutPosition()));
                        context.startActivity(intent);
                    }
                });
            } else {
                btnRemove = itemView.findViewById(R.id.btn_remove);
                btnRemove.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        AlertUtils.simpleAlert(context, "", "Remove this account?", "Yes",
                                "No", new AlertUtils.OnAlertButtonClickListener() {
                                    @Override
                                    public void onAlertButtonClick(boolean isPositiveButton) {
                                        if (isPositiveButton) {
                                            if (dbHelper.deleteAccount(accountList.get(getLayoutPosition()).getAcName())) {
                                                accountList.remove(getLayoutPosition());
                                                notifyItemRemoved(getLayoutPosition());
                                            }
                                        }
                                    }
                                });
                    }
                });
            }
        }
    }
}
