package com.emts.gtp;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.hardware.fingerprint.FingerprintManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;
import android.widget.TextClock;
import android.widget.TextView;

import com.emts.gtp.activity.SplashActivity;
import com.emts.gtp.helper.Logger;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONObject;

public class LockScreenActivity extends AppCompatActivity {
    TextView tvBuy, tvSell;
    //    ImageView imgBuy, imgSell;
    double latestBuyPrice, latestSellPrice, prevBuyPrice, prevSellPrice;
    //    Bitmap iconStable, iconIncreasing, iconDecreasing;
    Animation blinkAnimBuy, blinkAnimSell;

    LinearLayout notiLayout;

    FingerPrintHelper fingerPrintHelper;

    int fingerPrintRetryCount = 0;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

//        boolean isLockIntended = getIntent().getBooleanExtra("isLockIntended", false);
//        if (!isLockIntended) {
//        }
        if ((getIntent().getFlags() & Intent.FLAG_ACTIVITY_LAUNCHED_FROM_HISTORY) != 0) {
            Intent intent = new Intent(getApplicationContext(), SplashActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP
                    | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            this.finish();
        }

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON |
                WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD |
                WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED |
                WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);

        //keep screen on
        getWindow().addFlags(android.view.WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        setContentView(R.layout.notification_live_feed);

        notiLayout = findViewById(R.id.noti_lock);

        tvBuy = findViewById(R.id.tv_noti_ace_buy);
        tvSell = findViewById(R.id.tv_noti_ace_sell);
//        imgBuy = findViewById(R.id.img_noti_buy);
//        imgSell = findViewById(R.id.img_noti_sell);

        findViewById(R.id.touchToDismiss).setOnTouchListener(new View.OnTouchListener() {
            @SuppressLint("ClickableViewAccessibility")
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    finish();
//                    android.os.Process.killProcess(android.os.Process.myPid());
                }
                return false;
            }
        });

        findViewById(R.id.touchToDismiss1).setOnTouchListener(new View.OnTouchListener() {
            @SuppressLint("ClickableViewAccessibility")
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    finish();
//                    android.os.Process.killProcess(android.os.Process.myPid());
                }
                return false;
            }
        });

        //ready the icons and color
        Resources res = getResources();

//        iconStable = BitmapFactory.decodeResource(res, R.drawable.icon_stable);
//        iconIncreasing = BitmapFactory.decodeResource(res, R.drawable.icon_ace_increse);
//        iconDecreasing = BitmapFactory.decodeResource(res, R.drawable.icon_ace_decrese);
        blinkAnimBuy = AnimationUtils.loadAnimation(this, R.anim.blink);
        blinkAnimSell = AnimationUtils.loadAnimation(this, R.anim.blink);

        blinkAnimBuy.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                tvBuy.setTextColor(Color.BLACK);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

        blinkAnimSell.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                tvSell.setTextColor(Color.BLACK);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

        TextClock textClock = findViewById(R.id.clock);
        textClock.setFormat12Hour(null);
        textClock.setFormat24Hour("hh:mm");

        TextClock textClock1 = findViewById(R.id.date);
        textClock1.setFormat12Hour(null);
        textClock1.setFormat24Hour("EEEE dd");


        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            // only for gingerbread and newer versions
            fingerPrintHelper = FingerPrintHelper.getInstance(this);
            fingerPrintHelper.setFingerPrintCallback(fingerPrintAuthCallback);
        }

    }

    @Override
    protected void onDestroy() {
        getWindow().clearFlags(android.view.WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        stopService(new Intent(getApplicationContext(), LivePriceSocketService.class));
        super.onDestroy();
    }

    @Override
    protected void onStart() {
        super.onStart();

        EventBus.getDefault().register(this);
        startService(new Intent(getApplicationContext(), LivePriceSocketService.class));
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
        stopFingerPrintAuth();
    }

    public void startFingerPrintAuth() {
        if (fingerPrintHelper == null) return;
        if (fingerPrintHelper.checkFingerPrintAvailability(LockScreenActivity.this)) {
            //start fingerprint auth
            fingerPrintHelper.startAuth();
        }
        Logger.e("LockScreenActivity start FingerPrintAuth", " ***** FINGER PRINT LISTENING *******");
    }


    public void stopFingerPrintAuth() {
        if (fingerPrintHelper == null) return;
        fingerPrintHelper.stopAuth();
        Logger.e("LockScreenActivity stop FingerPrintAuth", " ***** FINGER PRINT STOP *******");
    }

    @Override
    protected void onResume() {
        super.onResume();
        startFingerPrintAuth();
    }

    FingerPrintHelper.FingerPrintAuthCallback fingerPrintAuthCallback = new FingerPrintHelper.FingerPrintAuthCallback() {
        @Override
        public void onNoFingerPrintHardwareFound() {
            Logger.e("FingerPrintHelper", "onNoFingerPrintHardwareFound");
        }

        @Override
        public void onNoFingerPrintRegistered() {
            Logger.e("FingerPrintHelper", "onNoFingerPrintRegistered");
        }

        @Override
        public void onBelowMarshmallow() {
            Logger.e("FingerPrintHelper", "onBelowMarshmallow");
        }

        @Override
        public void onAuthSuccess(FingerprintManager.CryptoObject cryptoObject) {
            Logger.e("FingerPrintHelper", "onAuthSuccess : " + cryptoObject);

            LockScreenActivity.this.finish();
        }

        @Override
        public void onAuthFailed(int errorCode, String errorMessage) {
            Logger.e("FingerPrintHelper", "Error Code: " + errorCode + " Error Message: " + errorMessage);
            switch (errorCode) {    //Parse the error code for recoverable/non recoverable error.
                case FingerPrintHelper.AuthErrorCodes.CANNOT_RECOGNIZE_ERROR:
                    //Cannot recognize the fingerprint scanned.
                    break;
                case FingerPrintHelper.AuthErrorCodes.NON_RECOVERABLE_ERROR:
                    //This is not recoverable error. Try other options for user authentication. like pin, password.
                    // try to connect again
                    if (fingerPrintRetryCount < 3) {
                        stopFingerPrintAuth();
                        startFingerPrintAuth();
                        fingerPrintRetryCount++;
                    }
                    break;
                case FingerPrintHelper.AuthErrorCodes.RECOVERABLE_ERROR:
                    //Any recoverable error. Display message to the user.
                    break;
            }

        }
    };

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(LivePriceSocketService.TradeOpenMesObj event) {
        try {
            if (!event.msg.equals("true")) {
//                finish();
                android.os.Process.killProcess(android.os.Process.myPid());
            }
        } catch (Exception e) {
            Logger.e("listenToTradeOpen onUI thread ex", e.getMessage() + " ");
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(LivePriceSocketService.IntIXMessageObj event) {
        try {
            final JSONObject eachChat = new JSONObject(event.msg);
            Double liveBuyPrice = Double.parseDouble(eachChat.getString("gp_livebuyprice_gm"));
            Double liveSellPrice = Double.parseDouble(eachChat.getString("gp_livesellprice_gm"));

            tvBuy.setText("RM " + String.format("%.03f", liveBuyPrice));
            tvSell.setText("RM " + String.format("%.03f", liveSellPrice));
            prevBuyPrice = latestBuyPrice;
            prevSellPrice = latestSellPrice;
            latestBuyPrice = liveBuyPrice;
            latestSellPrice = liveSellPrice;

            if (prevSellPrice == latestSellPrice) {
//                imgSell.setImageBitmap(iconStable);
//                imgSell.setColorFilter(getResources().getColor(R.color.btnDisable));
                setAnimation(tvSell, 0);
            } else if (prevSellPrice < latestSellPrice) {
//                imgSell.setColorFilter(null);
//                imgSell.setImageBitmap(iconIncreasing);
                setAnimation(tvSell, 1);
            } else {
//                imgSell.setColorFilter(null);
//                imgSell.setImageBitmap(iconDecreasing);
                setAnimation(tvSell, -1);
            }
            if (prevBuyPrice == latestBuyPrice) {
//                imgBuy.setColorFilter(getResources().getColor(R.color.btnDisable));
//                imgBuy.setImageBitmap(iconStable);
                setAnimation(tvBuy, 0);
            } else if (prevBuyPrice < latestBuyPrice) {
//                imgBuy.setColorFilter(null);
//                imgBuy.setImageBitmap(iconIncreasing);
                setAnimation(tvBuy, 1);
            } else {
//                imgBuy.setColorFilter(null);
//                imgBuy.setImageBitmap(iconDecreasing);
                setAnimation(tvBuy, -1);
            }

            if (notiLayout.getVisibility() == View.GONE) {
                notiLayout.setVisibility(View.VISIBLE);
            }

        } catch (Exception e) {
            Logger.e("listenToIntIX onUI thread ex", e.getMessage() + " ");
        }
    }

    public void setAnimation(final TextView tv, int change) {
        if (change == 0) {
            //stable
            tv.clearAnimation();
            tv.setTextColor(Color.BLACK);
        } else if (change == -1) {
            //decrease
            tv.clearAnimation();
            tv.setTextColor(ResourcesCompat.getColor(getResources(), R.color.priceDecline, null));
            if (tv.getId() == tvBuy.getId()) {
                tv.startAnimation(blinkAnimBuy);
            } else {
                tv.startAnimation(blinkAnimSell);
            }
        } else if (change == 1) {
            //increase
            tv.clearAnimation();
            tv.setTextColor(ResourcesCompat.getColor(getResources(), R.color.priceIncrease, null));
            if (tv.getId() == tvBuy.getId()) {
                tv.startAnimation(blinkAnimBuy);
            } else {
                tv.startAnimation(blinkAnimSell);
            }
        }
    }
}
