package com.emts.gtp;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.text.TextUtils;

import com.emts.gtp.helper.Logger;
import com.emts.gtp.helper.PreferenceHelper;
import com.emts.gtp.model.Account;

import java.util.ArrayList;

public class Database extends SQLiteOpenHelper {
    private static final String DATABASE_NAME = "gtp.db";
    private static Database instance = null;
    private static final int DATABASE_VERSION = 1;

    private static final String TABLE_ACCOUNT = "gtp_account";
    private static final String ROW_ID = "_id";
    private static final String ROW_ACC_NAME = "accountName";
    private static final String ROW_ACC_PASS = "accountPassword";
    private static final String ROW_QUICK_SIGNIN_ENABLE = "quickSignInEnable";

    private Database(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }


    public static Database getInstance(Context context) {
        if (instance == null) {
            instance = new Database(context.getApplicationContext());
        }
        return instance;
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onCreate(db);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        //create table account
        final String CREATE_TABLE_ACCOUNT = "CREATE TABLE if not exists " + TABLE_ACCOUNT
                + " (" + ROW_ID + " integer PRIMARY KEY AUTOINCREMENT, " + ROW_ACC_NAME + " text, "
                + ROW_ACC_PASS + " text, " + ROW_QUICK_SIGNIN_ENABLE + " integer DEFAULT 0)";
        db.execSQL(CREATE_TABLE_ACCOUNT);
    }

    public boolean addAccount(String accountName) {
        ContentValues cValues = new ContentValues();
        cValues.put(ROW_ACC_NAME, accountName);
        try (SQLiteDatabase sqLiteDatabase = this.getWritableDatabase()) {
            return sqLiteDatabase.insert(TABLE_ACCOUNT, null, cValues) != -1;
        } catch (Exception e) {
            Logger.e("db addAccount ex", e.getMessage() + " ");
        } finally {
            cValues.clear();
            SQLiteDatabase.releaseMemory();
        }
        return false;
    }

    public boolean deleteAccount(String name) {
        SQLiteDatabase db = getWritableDatabase();
        return db.delete(TABLE_ACCOUNT, ROW_ACC_NAME + "='" + name + "'", null) > 0;
    }

    public ArrayList<Account> getAllAccounts() {
        ArrayList<Account> allAccounts = new ArrayList<>();
        SQLiteDatabase db = getReadableDatabase();
        String queryGet = "select " + ROW_ACC_NAME + ", " + ROW_QUICK_SIGNIN_ENABLE + " from " + TABLE_ACCOUNT;

        try (Cursor c = db.rawQuery(queryGet, null)) {
            if (c.moveToFirst()) {
                for (int i = 0; i < c.getCount(); i++) {
                    Account acc = new Account();
                    acc.setAcName(c.getString(c.getColumnIndex(ROW_ACC_NAME)));
                    acc.setQuickSignInEnable(c.getInt(c.getColumnIndex(ROW_QUICK_SIGNIN_ENABLE)) == 1);
                    allAccounts.add(acc);
                    c.moveToNext();
                }
            }
        } catch (Exception e) {
            Logger.e("getAllAccounts ex", e.getMessage() + " ");
        } finally {
            SQLiteDatabase.releaseMemory();
        }
        return allAccounts;
    }

    public boolean isAccountAdded() {
        SQLiteDatabase db = getReadableDatabase();
        String query = "select " + ROW_ID + " from " + TABLE_ACCOUNT;
        Cursor c = db.rawQuery(query, null);
        boolean isAdded = c.getCount() > 0;
        c.close();
        return isAdded;
    }

    public boolean isAccountAdded(String screenName) {
        SQLiteDatabase db = getReadableDatabase();
        String query = "select " + ROW_ID + " from " + TABLE_ACCOUNT + " where " + ROW_ACC_NAME +
                " ='" + screenName + "'";
        Cursor c = db.rawQuery(query, null);
        boolean isAdded = c.getCount() > 0;
        c.close();
        return isAdded;
    }

    public boolean isScreenName(String name) {
        SQLiteDatabase db = getReadableDatabase();
        String query = "select " + ROW_ID + " from " + TABLE_ACCOUNT + " where " + ROW_ACC_NAME + "='" + name + "'";
        Cursor c = db.rawQuery(query, null);
        int cursorLength = c.getCount();
        c.close();
        return cursorLength > 0;
    }

    public boolean updatePassword(String screenName, String password) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(ROW_ACC_PASS, password);

        return db.update(TABLE_ACCOUNT, contentValues, ROW_ACC_NAME + " = ?", new String[]{screenName}) > 0;
    }

    public boolean setQuickSignInFor(String screenName, boolean isQuickSignIn) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        if (isQuickSignIn) {
            contentValues.put(ROW_QUICK_SIGNIN_ENABLE, 1);
        } else {
            contentValues.put(ROW_QUICK_SIGNIN_ENABLE, 0);
        }

        return db.update(TABLE_ACCOUNT, contentValues, ROW_ACC_NAME + " = ?", new String[]{screenName}) > 0;
    }

    public boolean isQuickSignInEnable(String screenName) {
        SQLiteDatabase db = getReadableDatabase();

        try (Cursor c = db.query(TABLE_ACCOUNT, new String[]{ROW_QUICK_SIGNIN_ENABLE}, ROW_ACC_NAME + " = ?",
                new String[]{screenName}, null, null, null)) {
            if (c != null) {
                if (c.getCount() > 0 && c.moveToFirst()) {
                    return c.getInt(c.getColumnIndex(ROW_QUICK_SIGNIN_ENABLE)) == 1;
                }
            }
        }
        return false;
    }

    public String getPassword(Context context, String screenName) {
        if (TextUtils.isEmpty(PreferenceHelper.getInstance(context).getString(PreferenceHelper.USERNAME, ""))) {
            return PreferenceHelper.getInstance(context).getDefaultPassword();
        }

        SQLiteDatabase db = getReadableDatabase();

        try (Cursor c = db.query(TABLE_ACCOUNT, new String[]{ROW_ACC_PASS}, ROW_ACC_NAME + " = ?",
                new String[]{screenName}, null, null, null)) {
            if (c != null) {
                if (c.getCount() > 0 && c.moveToFirst()) {
                    return c.getString(c.getColumnIndex(ROW_ACC_PASS));
                }
            }
        }

        return PreferenceHelper.getInstance(context).getDefaultPassword();
    }
}
