package com.emts.gtp.model;

import java.io.Serializable;

public class NotificationModel implements Serializable {
    private String notiID;
    private String notiTitle;
    private String notiBody;
    private String notiDate;

    public String getNotiID() {
        return notiID;
    }

    public void setNotiID(String notiID) {
        this.notiID = notiID;
    }

    public String getNotiTitle() {
        return notiTitle;
    }

    public void setNotiTitle(String notiTitle) {
        this.notiTitle = notiTitle;
    }

    public String getNotiBody() {
        return notiBody;
    }

    public void setNotiBody(String notiBody) {
        this.notiBody = notiBody;
    }

    public String getNotiDate() {
        return notiDate;
    }

    public void setNotiDate(String notiDate) {
        this.notiDate = notiDate;
    }
}
