package com.emts.gtp.model;

import java.io.Serializable;

public class Account implements Serializable {
    private String acName;
    private boolean isQuickSignInEnable;

    public String getAcName() {
        return acName;
    }

    public void setAcName(String acName) {
        this.acName = acName;
    }

    public boolean isQuickSignInEnable() {
        return isQuickSignInEnable;
    }

    public void setQuickSignInEnable(boolean quickSignInEnable) {
        isQuickSignInEnable = quickSignInEnable;
    }
}
