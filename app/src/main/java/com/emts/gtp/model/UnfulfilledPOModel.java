package com.emts.gtp.model;

import java.io.Serializable;

public class UnfulfilledPOModel implements Serializable {
    private String poNo;
    private String poDate;
    private String poVendorCode;
    private String poProduct;
    private String poDescription;
    private String poQty;
    private String poUnfulfillQty_1;
    private String draftGrnQty_2;
    private String balance_1_2;
    private String poUnitPrice;
    private String poAmount;
    private String poGst;
    private String poTotal;
    private String grnRef;
    private String gtpBookingNo;
    private boolean isJList;

    public String getPoNo() {
        return poNo;
    }

    public void setPoNo(String poNo) {
        this.poNo = poNo;
    }

    public String getPoVendorCode() {
        return poVendorCode;
    }

    public void setPoVendorCode(String poVendorCode) {
        this.poVendorCode = poVendorCode;
    }

    public String getPoDate() {
        return poDate;
    }

    public void setPoDate(String poDate) {
        this.poDate = poDate;
    }

    public String getPoProduct() {
        return poProduct;
    }

    public void setPoProduct(String poProduct) {
        this.poProduct = poProduct;
    }

    public String getPoDescription() {
        return poDescription;
    }

    public void setPoDescription(String poDescription) {
        this.poDescription = poDescription;
    }

    public String getPoQty() {
        return poQty;
    }

    public void setPoQty(String poQty) {
        this.poQty = poQty;
    }

    public String getPoUnfulfillQty_1() {
        return poUnfulfillQty_1;
    }

    public void setPoUnfulfillQty_1(String poUnfulfillQty_1) {
        this.poUnfulfillQty_1 = poUnfulfillQty_1;
    }

    public String getDraftGrnQty_2() {
        return draftGrnQty_2;
    }

    public void setDraftGrnQty_2(String draftGrnQty_2) {
        this.draftGrnQty_2 = draftGrnQty_2;
    }

    public String getBalance_1_2() {
        return balance_1_2;
    }

    public void setBalance_1_2(String balance_1_2) {
        this.balance_1_2 = balance_1_2;
    }

    public String getPoUnitPrice() {
        return poUnitPrice;
    }

    public void setPoUnitPrice(String poUnitPrice) {
        this.poUnitPrice = poUnitPrice;
    }

    public String getPoAmount() {
        return poAmount;
    }

    public void setPoAmount(String poAmount) {
        this.poAmount = poAmount;
    }

    public String getPoGst() {
        return poGst;
    }

    public void setPoGst(String poGst) {
        this.poGst = poGst;
    }

    public String getPoTotal() {
        return poTotal;
    }

    public void setPoTotal(String poTotal) {
        this.poTotal = poTotal;
    }

    public String getGrnRef() {
        return grnRef;
    }

    public void setGrnRef(String grnRef) {
        this.grnRef = grnRef;
    }

    public String getGtpBookingNo() {
        return gtpBookingNo;
    }

    public void setGtpBookingNo(String gtpBookingNo) {
        this.gtpBookingNo = gtpBookingNo;
    }

    public boolean isJList() {
        return isJList;
    }

    public void setJList(boolean JList) {
        isJList = JList;
    }
}
