package com.emts.gtp.model;

import java.io.Serializable;

public class Product implements Serializable {
    public static String P_BY_AMOUNT = "amount";
    public static String P_BY_WEIGHT = "weight";
    private String pName;
    private String pId;
    private String premiumFee;
    private String refineFee;
    private boolean isSelected;
    private String pCode;

    public String getpName() {
        return pName;
    }

    public void setpName(String pName) {
        this.pName = pName;
    }

    public String getpId() {
        return pId;
    }

    public void setpId(String pId) {
        this.pId = pId;
    }

    @Override
    public String toString() {
        return this.pName;
    }

    public String getPremiumFee() {
        return premiumFee;
    }

    public void setPremiumFee(String premiumFee) {
        this.premiumFee = premiumFee;
    }

    public String getRefineFee() {
        return refineFee;
    }

    public void setRefineFee(String refineFee) {
        this.refineFee = refineFee;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public String getpCode() {
        return pCode;
    }

    public void setpCode(String pCode) {
        this.pCode = pCode;
    }
}
