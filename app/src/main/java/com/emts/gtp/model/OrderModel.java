package com.emts.gtp.model;

import java.io.Serializable;

public class OrderModel implements Serializable {
    private String orderDate;
    private String orderId;
    private String orderNo;
    private String price;
    private String orderStatus;
    private String bookBy;
    private String xauWeight;
    private String productType;
    private String aceBuySell;
    private String grossTotal;
    private String customerOrderType;

    public OrderModel() {
    }

    public String getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(String orderDate) {
        this.orderDate = orderDate;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    public String getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(String orderStatus) {
        this.orderStatus = orderStatus;
    }

    public String getBookBy() {
        return bookBy;
    }

    public void setBookBy(String bookBy) {
        this.bookBy = bookBy;
    }

    public String getXauWeight() {
        return xauWeight;
    }

    public void setXauWeight(String xauWeight) {
        this.xauWeight = xauWeight;
    }

    public String getProductType() {
        return productType;
    }

    public void setProductType(String productType) {
        this.productType = productType;
    }

    public String getAceBuySell() {
        return aceBuySell;
    }

    public void setAceBuySell(String aceBuySell) {
        this.aceBuySell = aceBuySell;
    }

    public String getGrossTotal() {
        return grossTotal;
    }

    public void setGrossTotal(String grossTotal) {
        this.grossTotal = grossTotal;
    }

    public String getCustomerOrderType() {
        return customerOrderType;
    }

    public void setCustomerOrderType(String customerOrderType) {
        this.customerOrderType = customerOrderType;
    }
}
