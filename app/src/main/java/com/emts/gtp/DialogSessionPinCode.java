package com.emts.gtp;


import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.emts.gtp.helper.AlertUtils;
import com.emts.gtp.helper.PreferenceHelper;

import java.util.Objects;

public class DialogSessionPinCode extends DialogFragment {
    public static final String TAG = "adasd";
    static DialogSessionPinCode dialogSessionPinCode;
    private OnDialogSessionPinCodeInterface dialogInterface;


    public static DialogSessionPinCode getInstance() {
        if (dialogSessionPinCode == null) {
            dialogSessionPinCode = new DialogSessionPinCode();
        }
        return dialogSessionPinCode;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Override
    public void onDestroyView() {
        if (getDialog() != null && getRetainInstance())
            getDialog().setOnDismissListener(null);
        super.onDestroyView();
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(Objects.requireNonNull(getActivity()));
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.layout_session_pin_code, null, false);
        final EditText etSessionPinCode = view.findViewById(R.id.et_session_pin_code);
        Button btnProceed = view.findViewById(R.id.btn_proceed);
        Button btnCancel = view.findViewById(R.id.btn_cancel);
        setCancelable(false);

        builder.setView(view);
        final Dialog dialog = builder.create();
        btnProceed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PreferenceHelper helper = PreferenceHelper.getInstance(getActivity());
                if (etSessionPinCode.getText().length() < 6) {
                    AlertUtils.showToast(getActivity(), "Please enter 6 digit session pin code");
                    return;
                }
                if (helper.getSessionPinCode().equals(etSessionPinCode.getText().toString().trim())) {
                    dialog.dismiss();
                    if (dialogInterface != null) {
                        dialogInterface.onDialogButtonClicked(true);
                    }
                    AlertUtils.hideInputMethod(getActivity(), view);
                } else {
                    AlertUtils.showToast(getActivity(), "Session Pin Code Incorrect");
                }
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                if (dialogInterface != null) {
                    dialogInterface.onDialogButtonClicked(false);
                    AlertUtils.hideInputMethod(getActivity(), view);
                }
            }
        });


        return dialog;
    }

    public void setDialogInterface(OnDialogSessionPinCodeInterface dialogInterface) {
        this.dialogInterface = dialogInterface;
    }


    public interface OnDialogSessionPinCodeInterface {
        void onDialogButtonClicked(boolean isPositive);
    }
}
